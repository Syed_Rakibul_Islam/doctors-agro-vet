<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Admin Panel
Route::prefix('admin')->group(function () {
	// Authentication Routes...
	$this->get('login', 'Admin\Auth\AdminLoginController@showAdminLoginForm')->name('admin.login');
	$this->post('login', 'Admin\Auth\AdminLoginController@login');
	$this->post('logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');

	// Registration Routes...
//	$this->get('register', 'Admin\Auth\AdminRegisterController@showAdminRegistrationForm')->name('admin.register');
//	$this->post('register', 'Admin\Auth\AdminRegisterController@register');

	// Password Reset Routes...
	$this->get('password/reset', 'Admin\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	$this->post('password/email', 'Admin\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	$this->get('password/reset/{token}', 'Admin\Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
	$this->post('password/reset', 'Admin\Auth\AdminResetPasswordController@reset');

	Route::group(['middleware' => ['isAdmin']], function () {

		/*User Module*/
		Route::resource('user', 'Admin\AdminUserController');

		Route::get('dashboard', 'Admin\DashboardController@index');
		Route::get('/', 'Admin\DashboardController@index');

		/*Profile module*/
		Route::get('/profile', 'Admin\AdminProfileController@index');
		Route::get('/profile/edit', 'Admin\AdminProfileController@profileEdit');
		Route::post('/profile/edit', 'Admin\AdminProfileController@profileSave');
		Route::get('/profile/change-password', 'Admin\AdminProfileController@changePassword');
		Route::post('/profile/change-password', 'Admin\AdminProfileController@savePassword');

		/*News Module*/
		Route::resource('news/comment', 'Admin\AdminCommentController');
		Route::resource('news/category', 'Admin\AdminPostCategoryController');
		Route::get('/news/{id}/comment/{comment_id}', 'Admin\AdminPostController@comment')->where(['id' => '[0-9]+', '{comment_id}' => '[0-9]+']);
		Route::patch('/news/{id}/comment/{comment_id}', 'Admin\AdminPostController@approve')->where(['id' => '[0-9]+', '{comment_id}' => '[0-9]+']);
		Route::delete('/news/{id}/comment/{comment_id}', 'Admin\AdminPostController@delete')->where(['id' => '[0-9]+', '{comment_id}' => '[0-9]+']);
		Route::get('/news/{id}/comment/', 'Admin\AdminPostController@comments')->where('id', '[0-9]+');
		Route::resource('news', 'Admin\AdminPostController');


		/*Product Module*/
		Route::resource('product/category', 'Admin\AdminProductCategoryController');
		Route::resource('product', 'Admin\AdminProductController');

		/*Client Module*/
//		Route::resource('client', 'Admin\AdminClientController');

		/*Partner Module*/
		Route::resource('partner', 'Admin\AdminPartnerController');

		/*Slide Module*/
		Route::resource('slide', 'Admin\AdminSlideController');

		/*Slide Module*/
		Route::get('best-product', 'Admin\AdminBestProductController@index');
		Route::get('best-product/{id}', 'Admin\AdminBestProductController@show')->where('id', '[0-9]+');
		Route::get('best-product/{id}', 'Admin\AdminBestProductController@show')->where('id', '[0-9]+');
		Route::get('best-product/{id}/edit', 'Admin\AdminBestProductController@edit')->where('id', '[0-9]+');
		Route::patch('best-product/{id}', 'Admin\AdminBestProductController@update')->where('id', '[0-9]+');


		Route::group(['prefix' => 'ajax', 'middleware' => 'ajax'], function () {
			Route::get('/check-post-name', 'Admin\adminAjaxController@checkPostName');
			Route::get('/check-edit-post-name', 'Admin\adminAjaxController@checkEditPostName');
			Route::get('/check-username', 'Admin\adminAjaxController@checkUsername');
			Route::get('/check-user-email', 'Admin\adminAjaxController@checkUserEmail');
			Route::get('/check-product-name', 'Admin\adminAjaxController@checkProductName');
			Route::get('/check-edit-product-name', 'Admin\adminAjaxController@checkEditProductName');
		});
	});

	/*Retrieve Data */
	Route::group(['prefix' => 'retrieve', 'middleware' => 'isSuper'], function () {
		/*Product module*/
		Route::get('/product', 'Admin\AdminRetrieveController@product');
		Route::get('/product/{id}', 'Admin\AdminRetrieveController@productView')->where('id', '[0-9]+');
		Route::put('/product/{id}', 'Admin\AdminRetrieveController@productRestore')->where('id', '[0-9]+');
		Route::delete('/product/{id}', 'Admin\AdminRetrieveController@productDelete')->where('id', '[0-9]+');

		Route::get('/product/category', 'Admin\AdminRetrieveController@productCategory');
		Route::get('/product/category/{id}', 'Admin\AdminRetrieveController@productCategoryView')->where('id', '[0-9]+');
		Route::put('/product/category/{id}', 'Admin\AdminRetrieveController@productCategoryRestore')->where('id', '[0-9]+');
		Route::delete('/product/category/{id}', 'Admin\AdminRetrieveController@productCategoryDelete')->where('id', '[0-9]+');


		/*news module*/
		Route::get('/news', 'Admin\AdminRetrieveController@post');
		Route::get('/news/{id}', 'Admin\AdminRetrieveController@postView')->where('id', '[0-9]+');
		Route::put('/news/{id}', 'Admin\AdminRetrieveController@postRestore')->where('id', '[0-9]+');
		Route::delete('/news/{id}', 'Admin\AdminRetrieveController@postDelete')->where('id', '[0-9]+');

		Route::get('/news/category', 'Admin\AdminRetrieveController@postCategory');
		Route::get('/news/category/{id}', 'Admin\AdminRetrieveController@postCategoryView')->where('id', '[0-9]+');
		Route::put('/news/category/{id}', 'Admin\AdminRetrieveController@postCategoryRestore')->where('id', '[0-9]+');
		Route::delete('/news/category/{id}', 'Admin\AdminRetrieveController@postCategoryDelete')->where('id', '[0-9]+');

		Route::get('/news/comment', 'Admin\AdminRetrieveController@postComment');
		Route::get('/news/comment/{id}', 'Admin\AdminRetrieveController@postCommentView')->where('id', '[0-9]+');
		Route::put('/news/comment/{id}', 'Admin\AdminRetrieveController@postCommentRestore')->where('id', '[0-9]+');
		Route::delete('/news/comment/{id}', 'Admin\AdminRetrieveController@postCommentDelete')->where('id', '[0-9]+');


		/*Partner module*/
		Route::get('/partner', 'Admin\AdminRetrieveController@partner');
		Route::get('/partner/{id}', 'Admin\AdminRetrieveController@partnerView')->where('id', '[0-9]+');
		Route::put('/partner/{id}', 'Admin\AdminRetrieveController@partnerRestore')->where('id', '[0-9]+');
		Route::delete('/partner/{id}', 'Admin\AdminRetrieveController@partnerDelete')->where('id', '[0-9]+');


		/*User module*/
		Route::get('/user', 'Admin\AdminRetrieveController@user');
		Route::get('/user/{id}', 'Admin\AdminRetrieveController@userView')->where('id', '[0-9]+');
		Route::put('/user/{id}', 'Admin\AdminRetrieveController@userRestore')->where('id', '[0-9]+');
		Route::delete('/user/{id}', 'Admin\AdminRetrieveController@userDelete')->where('id', '[0-9]+');


		/*Slide module*/
		Route::get('/slide', 'Admin\AdminRetrieveController@slide');
		Route::get('/slide/{id}', 'Admin\AdminRetrieveController@slideView')->where('id', '[0-9]+');
		Route::put('/slide/{id}', 'Admin\AdminRetrieveController@slideRestore')->where('id', '[0-9]+');
		Route::delete('/slide/{id}', 'Admin\AdminRetrieveController@slideDelete')->where('id', '[0-9]+');

	});
});





/*
 * User Panel
 */
$locale = \Illuminate\Support\Facades\Request::segment(1);
$allowed_locales = ['en', 'bn'];

if (! in_array($locale, $allowed_locales)) $locale = null;
Route::group(['prefix' => $locale, 'middleware' => ['locale']], function() {
	Auth::routes();
	//User panel
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/', 'HomeController@index');
	Route::get('/company', 'HomeController@company')->name('company');

	/*Profile module*/
	Route::group(['middleware' => ['auth']], function () {
		Route::get('/profile', 'ProfileController@index')->name('profile');
		Route::get('/profile/edit', 'ProfileController@profileEdit');
		Route::post('/profile/edit', 'ProfileController@profileSave');
		Route::get('/profile/change-password', 'ProfileController@changePassword')->name('change_password');
		Route::post('/profile/change-password', 'ProfileController@savePassword');
	});

	/*News module*/
	Route::get('/news', 'NewsController@index')->name('news');
	Route::get('/news/category', 'NewsController@category');
	Route::get('/news/date', 'NewsController@date');
	Route::get('/news/category/{category}', 'NewsController@categoryArchive')->where('category', '[a-zA-Z-]+');
	Route::get('/news/date/{year?}/{month?}/{day?}', 'NewsController@dateArchive')->where(['year' => '[0-9]{4}', 'month' => '[0-9]{2}', 'day' => '[0-9]{2}']);
	Route::get('/news/{name}', 'NewsController@single')->where('name', '[a-zA-Z-]+');

	/*Product module*/
	Route::get('/products/category', 'ProductController@category');
	Route::get('/products/category/{category}', 'ProductController@categoryArchive');
	Route::get('/products', 'ProductController@index')->name('product');
	Route::get('/products/{name}', 'ProductController@single')->where('name', '[0-9a-z-]+');

	/*Partner module*/
	Route::get('/partners', 'PartnerController@index')->name('partner');
	Route::get('/partners/{id}', 'PartnerController@single')->where('id', '[0-9]+');

	/*Client module*/
//	Route::get('/clients', 'ClientController@clients')->name('client');
//	Route::get('/clients/{district}', 'ClientController@clientsDistrict')->where('district', '[a-z]+');

	/* Contact Module */
	Route::get('/contact', 'ContactController@index')->name('contact');

});

// Ajax prefix
Route::group(['prefix' => 'ajax', 'middleware' => 'ajax'], function () {
	Route::get('/post-like', 'NewsController@postLike');
	Route::post('/post-comment', 'NewsController@postComment');
	Route::post('/client-data', 'ClientController@clientData');
	Route::get('/map-sidebar', 'ClientController@mapSidebar');
	Route::get('/client-modal', 'ClientController@clientModal');
	Route::post('/client-search', 'ClientController@clientSearch');
	Route::post('/district-client-data', 'ClientController@districtClientData');
	Route::post('/contact-data', 'ContactController@showContactData');
	Route::post('/contact', 'ContactController@saveContact');
});

Route::get('/test', function(){
	$disCount = \Illuminate\Support\Facades\DB::table('districts')->count();
	dd($disCount == 65);
});
