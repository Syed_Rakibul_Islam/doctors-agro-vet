//var mapStyles = [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":20},{"color":"#ececec"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"on"},{"color":"#f0f0ef"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#f0f0ef"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#d4d4d4"}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"on"},{"color":"#ececec"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"lightness":21},{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#d4d4d4"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#303030"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"geometry.stroke","stylers":[{"lightness":"-61"},{"gamma":"0.00"},{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#dadada"},{"lightness":17}]}];
//var mapStyles = [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}];
//var mapStyles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#dde6e8"},{"visibility":"on"}]}];

var lastClickedMarker;
var lastLeftClickMarker;
var searchClicked;
var mapAutoZoom;
var map;
var clientName;
var districtID;

// Hero Map on Home ----------------------------------------------------------------------------------------------------

function heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom){
    if( document.getElementById(element) != null ){

        //check e=image exist
        function UrlExists(url)
        {
            var http = new XMLHttpRequest();
            http.open('HEAD', url, false);
            http.send();
            return http.status!=404;
        }

        // Create google map first -------------------------------------------------------------------------------------

        if( !mapDefaultZoom ){
            mapDefaultZoom = 14;
        }

        if( !optimizedDatabaseLoading ){
            var optimizedDatabaseLoading = 0;
        }

        var mapStyles = [{"elementType":"geometry","stylers":[{"hue":"#ff4400"},{"saturation":-68},{"lightness":-4},{"gamma":0.72}]},{"featureType":"road","elementType":"labels.icon"},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"hue":"#0077ff"},{"gamma":3.1}]},{"featureType":"water","stylers":[{"hue":"#00ccff"},{"gamma":0.44},{"saturation":-33}]},{"featureType":"poi.park","stylers":[{"hue":"#44ff00"},{"saturation":-23}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"hue":"#007fff"},{"gamma":0.77},{"saturation":65},{"lightness":99}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"gamma":0.11},{"weight":5.6},{"saturation":99},{"hue":"#0091ff"},{"lightness":-86}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"lightness":-48},{"hue":"#ff5e00"},{"gamma":1.2},{"saturation":-23}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"saturation":-64},{"hue":"#ff9100"},{"lightness":16},{"gamma":0.47},{"weight":2.7}]}];

        map = new google.maps.Map(document.getElementById(element), {
            zoom: mapDefaultZoom,
            scrollwheel: true,
            center: new google.maps.LatLng(_latitude, _longitude),
            mapTypeId: "roadmap",
            styles: mapStyles
        });

        // Load necessary data for markers using PHP (from database) after map is loaded and ready ---------------------

        var allMarkers;

        //  When optimization is enabled, map will load the data in Map Bounds every time when it's moved. Otherwise will load data at once

        if( optimizedDatabaseLoading == 1 ){
            google.maps.event.addListener(map, 'idle', function(){
                if( searchClicked != 1 ){
                    var ajaxData = {
                        optimized_loading: 1,
                        north_east_lat: map.getBounds().getNorthEast().lat(),
                        north_east_lng: map.getBounds().getNorthEast().lng(),
                        south_west_lat: map.getBounds().getSouthWest().lat(),
                        south_west_lng: map.getBounds().getSouthWest().lng()
                    };
                    if( markerCluster != undefined ){
                        markerCluster.clearMarkers();
                    }
                    loadData(baseURL + '/ajax/client-data', ajaxData);
                }
            });
        }
        else {
            google.maps.event.addListenerOnce(map, 'idle', function(){
                loadData(baseURL + '/ajax/client-data');
            });
        }

        if( showMarkerLabels == true ){
            $(".hero-section .map").addClass("show-marker-labels");
        }

        // Create and place markers function ---------------------------------------------------------------------------

        var i;
        var a;
        var newMarkers = [];
        var resultsArray = [];
        var visibleMarkersId = [];
        var visibleMarkersOnMap = [];
        var markerCluster;

        function placeMarkers(markers){

            newMarkers = [];

            for (i = 0; i < markers.length; i++) {

                var marker;
                var markerContent = document.createElement('div');
                var thumbnailImage;

                if( markers[i]["image"] != undefined ){
                    thumbnailImage = assetURL + "images/upload/client/" + markers[i]["image"];
                }
                else {
                    thumbnailImage = assetURL + "images/client.png";
                }

                markerContent.innerHTML =
                    '<div class="marker" data-id="'+ markers[i]["id"] +'">' +
                    '<div class="title">'+ markers[i]["name"] +'</div>' +
                    '<div class="marker-wrapper">' +
                    '<div class="pin">' +
                    '<div class="image" style="background-image: url('+ thumbnailImage +');"></div>' +
                    '</div>' +
                    '<div class="pulse"></div>' +
                    '</div>';

                if ( markers[i]["latitude"] && markers[i]["longitude"] ) {
                    renderRichMarker(i,"latitudeLongitude");
                }

                // No coordinates

                else {
                    console.log( "No location coordinates");
                }
            }

            // Create marker using RichMarker plugin -------------------------------------------------------------------

            function renderRichMarker(i,method){
                if( method == "latitudeLongitude" ){
                    marker = new RichMarker({
                        position: new google.maps.LatLng( markers[i]["latitude"], markers[i]["longitude"] ),
                        map: map,
                        draggable: false,
                        content: markerContent,
                        flat: true
                    });
                    google.maps.event.addListener(marker, 'click', (function() {
                        return function() {
                            if( markerTarget == "modal" ){

                                clientModal($(this.content.firstChild).attr("data-id"), "client-modal");

                            }
                        }
                    })(marker, i));
                    newMarkers.push(marker);
                }

                if ( mapAutoZoom == 1 ){
                    var bounds  = new google.maps.LatLngBounds();
                    for (var i = 0; i < newMarkers.length; i++ ) {
                        bounds.extend(newMarkers[i].getPosition());
                    }
                    map.fitBounds(bounds);
                }

            }

            // Highlight result in sidebar on marker hover -------------------------------------------------------------

            $('body').on("mouseover", '.marker', function(){
                var id = $(this).attr("data-id");
                $(".results-wrapper #results-content .map-sidebar-content[data-id="+ id +"]" ).addClass("hover-state");
            }).on("mouseleave", '.marker', function(){
                var id = $(this).attr("data-id");
                $(".results-wrapper #results-content .map-sidebar-content[data-id="+ id +"]" ).removeClass("hover-state");
            });

            $('body').on("click", '.marker', function(){
                var id = $(this).attr("data-id");
                $(lastClickedMarker).removeClass("active");
                $(lastLeftClickMarker).removeClass("active");
                $(this).addClass("active");
                lastClickedMarker = $(this);
                $(".results-wrapper #results-content .map-sidebar-content[data-id="+ id +"]" ).addClass("active");
                lastLeftClickMarker = $(".results-wrapper #results-content .map-sidebar-content[data-id="+ id +"]" );
            });

            // Marker clusters -----------------------------------------------------------------------------------------

            var clusterStyles = [
                {
                    url: assetURL + "images/cluster.png",
                    height: 36,
                    width: 36
                }
            ];

            markerCluster = new MarkerClusterer(map, newMarkers, { styles: clusterStyles, maxZoom: 16, ignoreHidden: true });

            // Show results in sidebar after map is moved --------------------------------------------------------------



            // Results in the sidebar ----------------------------------------------------------------------------------

            function renderResults(client_name, district_id){
                resultsArray = [];
                visibleMarkersId = [];
                visibleMarkersOnMap = [];

                for (var i = 0; i < newMarkers.length; i++) {
                    if ( map.getBounds().contains(newMarkers[i].getPosition()) ){
                        visibleMarkersOnMap.push(newMarkers[i]);
                        visibleMarkersId.push( $(newMarkers[i].content.firstChild).attr("data-id") );
                        newMarkers[i].setVisible(true);
                    }
                    else {
                        newMarkers[i].setVisible(false);
                    }
                }
                markerCluster.repaint();

                // Ajax load data for sidebar results after markers are placed


                sidebarUrl = baseURL + "/ajax/map-sidebar";

                $.ajax({
                    url: sidebarUrl,
                    method: "GET",
                    data: { markers: visibleMarkersId, client_name: client_name, client_district: district_id },
                    success: function(results){
                        resultsArray.push(results); // push the results from php into array
                        $(".results-wrapper #results-content").html(results); // render the new php data into html element
                        $(".results-wrapper .section-title h2 .results-number").html(visibleMarkersId.length); // show the number of results
                        //ratingPassive(".results-wrapper .results"); // render rating stars

                        // Hover on the result in sidebar will highlight the marker

                        $("#results-content").on("mouseover", ".map-sidebar-content", function(){
                            $(".map .marker[data-id="+ $(this).attr("data-id") +"]").addClass("hover-state");
                        }).on("mouseleave", ".map-sidebar-content", function(){
                            $(".map .marker[data-id="+ $(this).attr("data-id") +"]").removeClass("hover-state");
                        });

                        //Scroller
                        $("#results-content").mCustomScrollbar({
                            axis:"y", // vertical and horizontal scrollbar
                            setHeight: (scrollerBarSize - 91) + "px"
                        });

                        $('#content-result-loading').hide();
                        $("#results-content").show();


                        // Show detailed information in sidebar

                        $("#results-content").on("click", '.map-sidebar-content', function(e){
                            e.preventDefault();

                            clientModal($(this).attr("data-id"), "client-modal");


                            $(lastClickedMarker).removeClass("active");
                            $(lastLeftClickMarker).removeClass("active");

                            $(".map .marker[data-id="+ $(this).attr("data-id") +"]").addClass("active");
                            lastClickedMarker = $(".map .marker[data-id="+ $(this).attr("data-id") +"]");
                            $(this).addClass("active");
                            lastLeftClickMarker = $(this);
                        });

                    },
                    error : function (e) {
                        console.log(e);
                    }
                });

            }
            renderResults(clientName, districtID);
        }

        // Serach button
        $("#serach-button").on("click", function(e){
            e.preventDefault();
            var url = baseURL + '/ajax/client-search';
            searchClicked = 1;
            if( $(this).attr("data-ajax-auto-zoom") == 1 ){
                mapAutoZoom = 1;
            }
            var ajaxData = $('#map-search').serialize();

            $('#content-result-loading').show();
            $("#results-content").html('');

            clientName = $('#map-search').find('input[name=client_name]').val();
            districtID = $('#map-search').find('select[name=client_district]').val();
            markerCluster.clearMarkers();
            loadData(url, ajaxData);

        });


        function loadData(url, ajaxData){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                dataType: "json",
                method: "POST",
                data: ajaxData,
                cache: false,
                success: function(results){
                    for( var i=0; i <newMarkers.length; i++ ){
                        newMarkers[i].setMap(null);
                    }
                    allMarkers = results;
                    placeMarkers(results);
                },
                error : function (e) {
                    console.log(e);
                }
            });
        }
        function clientModal(client_id, url) {
            var fullUrl = baseURL + '/ajax/' + url;
            $.ajax({
                url: fullUrl,
                method: "GET",
                data: {id: client_id},
                success: function(result){
                    $("#modal-client").find(".service-box").html(result);
                    $('#modal-client').modal('show');

                },
                error : function (e) {
                    console.log(e);
                }
            });
        }
        function searchRender(searchData){
            sidebarUrl = baseURL + "/ajax/map-sidebar";
            $.ajax({
                url: sidebarUrl,
                method: "GET",
                data: searchData,
                success: function(results){
                    resultsArray.push(results); // push the results from php into array
                    $(".results-wrapper #results-content").html(results); // render the new php data into html element
                    $(".results-wrapper .section-title h2 .results-number").html(visibleMarkersId.length); // show the number of results
                    //ratingPassive(".results-wrapper .results"); // render rating stars

                    // Hover on the result in sidebar will highlight the marker

                    $("#results-content").on("mouseover", ".map-sidebar-content", function(){
                        $(".map .marker[data-id="+ $(this).attr("data-id") +"]").addClass("hover-state");
                    }).on("mouseleave", ".map-sidebar-content", function(){
                        $(".map .marker[data-id="+ $(this).attr("data-id") +"]").removeClass("hover-state");
                    });

                    //Scroller
                    $("#results-content").mCustomScrollbar({
                        axis:"y", // vertical and horizontal scrollbar
                        setHeight: (scrollerBarSize - 91) + "px"
                    });

                    $('#content-result-loading').hide();
                    $("#results-content").show();


                    // Show detailed information in sidebar

                    $("#results-content").on("click", '.map-sidebar-content', function(e){
                        e.preventDefault();

                        clientModal($(this).attr("data-id"), "client-modal");


                        $(lastClickedMarker).removeClass("active");
                        $(lastLeftClickMarker).removeClass("active");

                        $(".map .marker[data-id="+ $(this).attr("data-id") +"]").addClass("active");
                        lastClickedMarker = $(".map .marker[data-id="+ $(this).attr("data-id") +"]");
                        $(this).addClass("active");
                        lastLeftClickMarker = $(this);
                    });

                },
                error : function (e) {
                    console.log(e);
                }
            });
        }

        $('#modal-client').on('hidden.bs.modal', function () {
            $(lastClickedMarker).removeClass("active");
            $(lastLeftClickMarker).removeClass("active");;
        });

        // Autocomplete

        autoComplete(map);

    }
    else {
        console.log("No map element");
    }

}

function reloadMap(){
    google.maps.event.trigger(map, 'resize');
}




//Autocomplete ---------------------------------------------------------------------------------------------------------

function autoComplete(map, marker){
    if( $("#address-autocomplete").length ){
        if( !map ){
            map = new google.maps.Map(document.getElementById("address-autocomplete"));
        }
        var input = document.getElementById('address-autocomplete');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            if( marker ){
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                $('#latitude').val( marker.getPosition().lat() );
                $('#longitude').val( marker.getPosition().lng() );
            }
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });

        function success(position) {
            map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            //initSubmitMap(position.coords.latitude, position.coords.longitude);
            $('#latitude').val( position.coords.latitude );
            $('#longitude').val( position.coords.longitude );
        }
    }
}