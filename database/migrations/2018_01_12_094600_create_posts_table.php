<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name', 100)->unique();
	        $table->string('title', 100)->nullable();
	        $table->longText('description')->nullable();
	        $table->string('image', 255)->nullable();
	        $table->string('video_link', 255)->nullable();
	        $table->string('status', 10)->nullable();
	        $table->date('post_date')->nullable();

	        $table->string('title_b', 100)->nullable();
	        $table->longText('description_b')->nullable();

	        $table->integer('created_by')->unsigned();
	        $table->foreign('created_by')->references('id')->on('users');

            $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
