<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('name', 100);
	        $table->string('email', 100)->nullable();
	        $table->string('contact', 100)->nullable();
	        $table->string('image', 255)->nullable();
	        $table->string('location', 100)->nullable();
	        $table->string('latitude', 50)->nullable();
	        $table->string('longitude', 50)->nullable();
	        $table->longText('about')->nullable();

	        $table->string('name_b', 100)->nullable();

	        $table->integer('district_id')->nullable()->unsigned();
	        $table->foreign('district_id')->references('id')->on('districts');

	        $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
