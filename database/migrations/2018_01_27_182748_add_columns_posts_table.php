<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('posts', function($table)
	    {
		    $table->integer('total_like')->after('description_b')->nullable();
		    $table->integer('total_comment')->after('description_b')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('posts', function(Blueprint $table) {
		    $table->dropColumn('total_like');
		    $table->dropColumn('total_comment');
	    });
    }
}
