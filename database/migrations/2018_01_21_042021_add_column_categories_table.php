<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('categories', function($table)
	    {
		    $table->string('name_b', 100)->after('name')->nullable();
		    $table->longText('description_b')->after('description')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('categories', function(Blueprint $table) {
		    $table->dropColumn('name_b');
		    $table->dropColumn('description_b');
	    });
    }
}
