<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name', 100)->unique()->index();
	        $table->string('title', 100)->nullable();
	        $table->longText('description')->nullable();
	        $table->string('image', 255)->nullable();
	        $table->longText('price')->nullable();

	        $table->string('title_b', 100)->nullable();
	        $table->longText('description_b')->nullable();

            $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
