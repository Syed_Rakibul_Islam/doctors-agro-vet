<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('partners', function($table)
	    {
		    $table->string('url', 255)->after('address')->nullable();
		    $table->integer('country_id')->after('name_b')->nullable()->unsigned();

		    $table->foreign('country_id')->references('id')->on('countries');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('partners', function(Blueprint $table) {
		    $table->dropForeign('partners_country_id_foreign');
		    $table->dropColumn('url');
		    $table->dropColumn('country_id');
	    });
    }
}
