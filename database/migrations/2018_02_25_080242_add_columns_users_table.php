<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function($table)
	    {
		    $table->string('name_b', 100)->after('contact')->nullable();
		    $table->string('image', 255)->after('contact')->nullable();
		    $table->string('address', 255)->after('contact')->nullable();
		    $table->longText('about')->after('contact')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('users', function(Blueprint $table) {
		    $table->dropColumn('name_b');
		    $table->dropColumn('description_b');
	    });
    }
}
