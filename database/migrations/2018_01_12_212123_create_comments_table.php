<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
	        $table->longText('description')->nullable();
	        $table->string('user_name', 100)->nullable();
	        $table->string('user_email', 100)->nullable();

	        $table->tinyInteger('approve')->nullable();

	        $table->integer('post_id')->unsigned();
	        $table->foreign('post_id')->references('id')->on('posts');
	        $table->integer('user_id')->unsigned()->nullable();
	        $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
