<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('name', 100);
	        $table->string('email', 100)->nullable();
	        $table->string('contact', 100)->nullable();
	        $table->string('image', 255)->nullable();
	        $table->string('address', 100)->nullable();
	        $table->longText('about')->nullable();

	        $table->string('name_b', 100)->nullable();

	        $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
