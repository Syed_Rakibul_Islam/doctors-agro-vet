<?php

use Illuminate\Database\Seeder;
use App\User;

class MakeSuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $is_acc = User::where('username', 'super')->first();
	    if(empty($is_acc)) {
		    User::create([
			    'name' => 'Super',
			    'username' => 'super',
			    'email' => 'super@email.com',
			    'password' => bcrypt('123456'),
			    'remember_token' => bcrypt(str_random(10)),
			    'role' => 'super'
		    ]);
	    }
	    else {
		    echo 'Already have a super admin account' . PHP_EOL . PHP_EOL;
	    }
    }
}
