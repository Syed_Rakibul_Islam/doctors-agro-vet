<?php

use App\BestProduct;
use Illuminate\Database\Seeder;

class MakeBestProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $is_bestProduct1 = BestProduct::find(1);
	    if(empty($is_bestProduct1)) {
		    BestProduct::create([
			    'description' => 'Description'
		    ]);
	    }
	    else {
		    echo 'Already have best product 1' . PHP_EOL . PHP_EOL;
	    }
	    $is_bestProduct2 = BestProduct::find(2);
	    if(empty($is_bestProduct2)) {
		    BestProduct::create([
			    'description' => 'Description'
		    ]);
	    }
	    else {
		    echo 'Already have best product 2' . PHP_EOL . PHP_EOL;
	    }
	    $is_bestProduct3 = BestProduct::find(3);
	    if(empty($is_bestProduct3)) {
		    BestProduct::create([
			    'description' => 'Description'
		    ]);
	    }
	    else {
		    echo 'Already have best product 3' . PHP_EOL . PHP_EOL;
	    }
	    $is_bestProduct4 = BestProduct::find(4);
	    if(empty($is_bestProduct4)) {
		    BestProduct::create([
			    'description' => 'Description'
		    ]);
	    }
	    else {
		    echo 'Already have best product 4' . PHP_EOL . PHP_EOL;
	    }


    }
}
