<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(MakeSuperAdminSeeder::class);
	    $this->call(MakeAdminSeeder::class);
	    $this->call(MakeUserSeeder::class);
	    $this->call(MakeDistrictSeeder::class);
	    $this->call(MakeBestProductsSeeder::class);
	    $this->call(MakeCountrySeeder::class);
    }
}
