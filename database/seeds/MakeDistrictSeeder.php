<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MakeDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $disCount = DB::table('districts')->count();
	    if($disCount != 65) {
		    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		    DB::table('districts')->truncate();
		    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		    $districts = [
			    'Bagherhat',
			    'Bandarban',
			    'Barguna',
			    'Barishal',
			    'Bhola',
			    'Bogra',
			    'Brahmanbaria',
			    'Chandpur',
			    'Chapinawabganj',
			    'Chittagong',
			    'Chuadanga',
			    'Comilla',
			    'Comilla',
			    'Coxs Bazar',
			    'Dhaka',
			    'Dinajpur',
			    'Faridpur',
			    'Feni',
			    'Gaibandha',
			    'Gazipur',
			    'Gopalganj',
			    'Hobiganj',
			    'Jamalpur',
			    'Jessore',
			    'Jhalokathi',
			    'Jinaidaha',
			    'Joypurhat',
			    'Khagrachari',
			    'Khulna',
			    'Kishoreganj',
			    'Kurigram',
			    'Kustia',
			    'Lakshmipur',
			    'Lalmonirhat',
			    'Madaripur',
			    'Magura',
			    'Manikganj',
			    'Meherpur',
			    'Moulvibazar',
			    'Munshiganj',
			    'Mymensingh',
			    'Naogaon',
			    'Narail',
			    'Narayanganj',
			    'Narshingdi',
			    'Natore',
			    'Netrakona',
			    'Nilphamari',
			    'Noakhali',
			    'Pabna',
			    'Panchagarh',
			    'Patuakhali',
			    'Pirojpur',
			    'Rajbari',
			    'Rajshahi',
			    'Rangamati',
			    'Rangpur',
			    'Satkhira',
			    'Shariatpur',
			    'Sherpur',
			    'Sirajganj',
			    'Sunamganj',
			    'Sylhet',
			    'Tangail',
			    'Thakurgaon'
		    ];
		    foreach ($districts as $key => $district){
		    	$insertData[$key] = ['name' => $district];
		    }
		    DB::table('districts')->insert($insertData);
	    }
	    else {
		    echo 'Already has districts information' . PHP_EOL . PHP_EOL;;
	    }
    }
}
