<?php

use Illuminate\Database\Seeder;
use App\User;

class MakeAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $is_acc = User::where('username', 'admin')->first();
	    if(empty($is_acc)) {
		    User::create([
			    'name' => 'Admin',
			    'username' => 'admin',
			    'email' => 'admin@email.com',
			    'password' => bcrypt('123456'),
			    'remember_token' => bcrypt(str_random(10)),
			    'role' => 'admin'
		    ]);
	    }
	    else {
		    echo 'Already have an admin account' . PHP_EOL . PHP_EOL;
	    }
    }
}
