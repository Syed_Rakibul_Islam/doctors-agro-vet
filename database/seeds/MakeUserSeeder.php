<?php

use Illuminate\Database\Seeder;
use App\User;

class MakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $is_acc = User::where('username', 'user')->first();
	    if(empty($is_acc)) {
		    User::create([
			    'name' => 'User',
			    'username' => 'user',
			    'email' => 'user@email.com',
			    'password' => bcrypt('123456'),
			    'remember_token' => bcrypt(str_random(10)),
			    'role' => 'user'
		    ]);
	    }
	    else {
		    echo 'Already have a user account' . PHP_EOL . PHP_EOL;
	    }
    }
}
