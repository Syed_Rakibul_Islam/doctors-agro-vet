<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'districts';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'name'];

	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	public $timestamps = false;

	public function clients()
	{
		return $this->hasMany('App\Client');
	}
}



