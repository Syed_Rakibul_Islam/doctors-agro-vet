<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'countries';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'name', 'code'];

	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	public $timestamps = false;

	public function partners()
	{
		return $this->hasMany('App\Partner');
	}
}
