<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'user_name', 'user_email', 'approve', 'post_id', 'user_id'];

    /**
     * The table column used for soft-delete.
     *
     * @var string
     */
    protected $dates = ['deleted_at'];


    /**
     * The table methods used for relationship with others table.
     *
     * @var methods
     */
	public function post(){
		return $this->belongsTo('App\Post');
	}
    public function user(){
	    return $this->belongsTo('App\User');
    }
    
}
