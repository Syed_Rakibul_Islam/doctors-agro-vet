<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slide extends Model
{
	use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'slides';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['text1', 'text2', 'text3', 'image', 'text1_b', 'text2_b', 'text3_b'];

	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	protected $dates = ['deleted_at'];
}
