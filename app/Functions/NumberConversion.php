<?php
/**
 * Created by PhpStorm.
 * User: Romeo
 * Date: 1/21/2018
 * Time: 1:03 PM
 */

namespace App\Functions;


class NumberConversion {
	public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
	public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

	public static function bn2en($number) {
		return str_replace(self::$bn, self::$en, $number);
	}

	public static function en2bn($number) {
		return str_replace(self::$en, self::$bn, $number);
	}
}