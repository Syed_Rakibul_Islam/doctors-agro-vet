<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'title', 'description', 'image', 'price', 'title_b', 'description_b', 'status'];
	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	protected $dates = ['deleted_at'];


	/**
	 * The table methods used for relationship with others table.
	 *
	 * @var methods
	 */
	public function categories()
	{
		return $this->belongsToMany('App\Category', 'product_categories');
	}

}
