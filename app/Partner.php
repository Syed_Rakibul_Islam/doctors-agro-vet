<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
	use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'partners';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'contact', 'image', 'address', 'url', 'about', 'name_b', 'country_id'];

	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	protected $dates = ['deleted_at'];

	/**
	 * The table methods used for relationship with others table.
	 *
	 * methods
	 */
	public function country()
	{
		return $this->belongsTo('App\Country');
	}

}
