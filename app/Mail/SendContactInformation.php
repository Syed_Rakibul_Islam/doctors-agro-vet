<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContactInformation extends Mailable
{
    use Queueable, SerializesModels;

	public $name, $email, $subject, $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	public function __construct($name, $email, $subject, $message)
	{
		$this->name = $name;
		$this->email = $email;
		$this->subject = $subject;
		$this->message = $message;
	}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	    $from = env('MAIL_USERNAME');
	    $title = config('app.name') . ' | Admin | Contact';
	    return $this->view('mail.contact')
	                ->from($from, $title)
	                ->replyTo($from, $title)
	                ->subject($this->subject)
	                ->with([
		                'name' => $this->name,
		                'email' => $this->email,
		                'subject' => $this->subject,
		                'description' => $this->message
	                ]);
    }
}
