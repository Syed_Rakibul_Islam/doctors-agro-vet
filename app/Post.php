<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'title', 'description', 'image', 'video_link', 'status', 'post_date',  'title_b', 'description_b', 'created_by', 'total_like', 'total_comment'];

	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	protected $dates = ['deleted_at'];

    /**
     * The table methods used for relationship with others table.
     *
     * @var methods
     */

	public function categories()
	{
		return $this->belongsToMany('App\Category', 'post_categories');
	}
	public function author(){
		return $this->belongsTo('App\User', 'created_by');
	}
	public function comments(){
		return $this->hasMany('App\Comment');
	}
	public function hasLike(){
		return $this->hasOne('App\PostLike');
	}
	public function hasComment(){
		return $this->hasOne('App\Comment');
	}

    
}
