<?php

namespace App\Http\Controllers;

use App\Client;
use App\District;
use Illuminate\Http\Request;

class ClientController extends Controller
{
	public function clients(){
		$districts = District::select('id', 'name')->get();
		return view('client.index', compact('districts'));
	}
	public function clientData(){
		$clients = Client::all();
		echo $clients->toJson();
	}
	public function mapSidebar(Request $request){
		if(!empty($request->input('client_name')) && !empty($request->input('client_district'))){
			$clients = Client::where('name', 'like', '%' . $request->input('client_name') . '%')->where('district_id', $request->input('client_district'))->orderBy('id', 'desc')->get();
		}
		elseif (!empty($request->input('client_name')) && empty($request->input('client_district'))){
			$clients = Client::where('name', 'like', '%' . $request->input('client_name') . '%')->orderBy('id', 'desc')->get();
		}
		elseif (empty($request->input('client_name')) && !empty($request->input('client_district'))){
			$clients = Client::where('district_id', $request->input('client_district'))->orderBy('id', 'desc')->get();
		}
		else{
			$clients = Client::orderBy('id', 'desc')->get();
		}
		return view('client.sidebar', compact('clients'));
	}
	public function clientModal(Request $request){
		$client = Client::findOrFail($request->id);
		return view('client.modal', compact('client'));
	}
	public function clientSearch(Request $request){
		if(!empty($request->client_name) && !empty($request->client_district)){
			$clients = Client::where('name', 'like', '%' . $request->client_name . '%')->where('district_id', $request->client_district)->get();
		}
		elseif (!empty($request->client_name) && empty($request->client_district)){
			$clients = Client::where('name', 'like', '%' . $request->client_name . '%')->get();
		}
		elseif (empty($request->client_name) && !empty($request->client_district)){
			$clients = Client::where('district_id', $request->client_district)->get();
		}
		else{
			$clients = Client::all();
		}

		echo $clients->toJson();
	}
	public function clientsDistrict($district){
		$district = District::where('name', $district)->firstOrFail();;
		if(!empty($district)){
			return view('client.district', compact('district'));
		}
		return view('errors.404');
	}
	public function districtClientData(Request $request){
		$clients = Client::where('district_id', $request->district_id)->get();
		echo $clients->toJson();
	}
}
