<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function index(){
    	$partners = Partner::orderBy('id', 'desc')->paginate(9);
		return view('partner.index', compact('partners'));
    }
	public function single($id){
		$partner = Partner::findOrFail($id);
		return view('partner.single', compact('partner'));
	}
}
