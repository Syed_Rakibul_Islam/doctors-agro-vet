<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminProductCategoryController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$categories = Category::where('type', 'product')->orderBy('id', 'desc')->get();
		return view('admin.product.category.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return view('admin.product.category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$request->request->add(['type' => 'product']);
		$this->validate($request, [
			'name' => 'required|max:100',
			'name_b' => 'nullable|max:100',
			'description' => 'nullable|max:100000',
			'description_b' => 'nullable|max:100000'
		]);

		Category::create($request->all());

		Session::flash('flash_message', 'Category added!');

		return redirect('admin/product/category');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$category = Category::findOrFail($id);

		return view('admin.product.category.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$category = Category::findOrFail($id);

		return view('admin.product.category.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'name_b' => 'nullable|max:100',
			'description' => 'nullable|max:100000',
			'description_b' => 'nullable|max:100000'
		]);

		$category = Category::findOrFail($id);

		$category->update($request->all());

		Session::flash('flash_message', 'Category updated!');

		return redirect('admin/product/category');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$category = Category::findOrFail($id);
		$category->delete();

		Session::flash('flash_message', 'Category deleted!');

		return redirect('admin/product/category');
	}
}
