<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Comment;
use App\Partner;
use App\Post;
use App\PostLike;
use App\Product;
use App\Slide;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;

class AdminRetrieveController extends Controller
{

	// Product Module
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
    public function product(){
	    $products = Product::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
	    return view('admin.retrieve.product.index', compact('products'));
    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
    public function productView($id){
    	$product = Product::onlyTrashed()->findOrFail($id);
	    return view('admin.retrieve.product.show', compact('product'));
    }
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function productRestore($id){
		$product = Product::onlyTrashed()->findOrFail($id);
		$product->restore();
		Session::flash('flash_message', 'Product restored!');
		return redirect('admin/retrieve/product');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    public function productDelete($id){
	    $product = Product::onlyTrashed()->findOrFail($id);
	    $product->categories()->detach();

	    // Delete image
	    $path = 'images/upload/product/';
	    if (!empty($product->image) && file_exists(($path . $product->image))){
	    	unlink($path . $product->image);
	    }

	    $product->forceDelete();

	    Session::flash('flash_message', 'Product permanently deleted!');

	    return redirect('admin/retrieve/product');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function productCategory(){
		$categories = Category::onlyTrashed()->where('type', 'product')->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.product.category.index', compact('categories'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function productCategoryView($id){
		$category = Category::onlyTrashed()->where('type', 'product')->findOrFail($id);
		return view('admin.retrieve.product.category.show', compact('category'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function productCategoryRestore($id){
		$category = Category::onlyTrashed()->where('type', 'product')->findOrFail($id);
		$category->restore();
		Session::flash('flash_message', 'Product category restored!');
		return redirect('admin/retrieve/product/category');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function productCategoryDelete($id){
		$category = Category::onlyTrashed()->where('type', 'product')->findOrFail($id);
		$category->products()->detach();
		$category->forceDelete();

		Session::flash('flash_message', 'Product category permanently deleted!');

		return redirect('admin/retrieve/product/category');
	}
	// End Product module


	// News module
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function post(){
		$posts = Post::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.post.index', compact('posts'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function postView($id){
		$post = Post::onlyTrashed()->findOrFail($id);
		return view('admin.retrieve.post.show', compact('post'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postRestore($id){
		$post = Post::onlyTrashed()->findOrFail($id);
		$post->restore();
		Session::flash('flash_message', 'News restored!');
		return redirect('admin/retrieve/news');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postDelete($id){
		$post = Post::onlyTrashed()->findOrFail($id);

		DB::table('comments')->where('post_id', $id)->delete();
		DB::table('post_likes')->where('post_id', $id)->delete();
		$post->categories()->detach();

		// Delete image
		$path = 'images/upload/news/';
		if (!empty($post->image) && file_exists(($path . $post->image))){
			unlink($path . $post->image);
		}

		$post->forceDelete();

		Session::flash('flash_message', 'News comment permanently deleted!');

		return redirect('admin/retrieve/news');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function postCategory(){
		$categories = Category::onlyTrashed()->where('type', 'post')->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.post.category.index', compact('categories'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function postCategoryView($id){
		$category = Category::onlyTrashed()->where('type', 'post')->findOrFail($id);
		return view('admin.retrieve.post.category.show', compact('category'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postCategoryRestore($id){
		$category = Category::onlyTrashed()->where('type', 'post')->findOrFail($id);
		$category->restore();
		Session::flash('flash_message', 'News category restored!');
		return redirect('admin/retrieve/news/category');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postCategoryDelete($id){
		$category = Category::onlyTrashed()->where('type', 'post')->findOrFail($id);
		$category->posts()->detach();
		$category->forceDelete();

		Session::flash('flash_message', 'News category permanently deleted!');

		return redirect('admin/retrieve/news/category');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function postComment(){
		$comments = Comment::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.post.comment.index', compact('comments'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function postCommentView($id){
		$comment = Comment::onlyTrashed()->findOrFail($id);
		return view('admin.retrieve.post.comment.show', compact('comment'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postCommentRestore($id){
		$comment = Comment::onlyTrashed()->findOrFail($id);
		$comment->restore();

		//update total comment
		$post = Post::withTrashed()->find($comment->post_id);
		$totalComment = $post->total_comment + 1;
		$post->update(['total_comment' => $totalComment]);

		Session::flash('flash_message', 'News comment restored!');
		return redirect('admin/retrieve/news/comment');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postCommentDelete($id){
		$comment = Comment::onlyTrashed()->findOrFail($id);
		$comment->forceDelete();

		Session::flash('flash_message', 'News comment permanently deleted!');

		return redirect('admin/retrieve/news/comment');
	}
	//End News Module


	//Partner module
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function partner(){
		$partners = Partner::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.partner.index', compact('partners'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function partnerView($id){
		$partner = Partner::onlyTrashed()->findOrFail($id);
		return view('admin.retrieve.partner.show', compact('partner'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function partnerRestore($id){
		$partner = Partner::onlyTrashed()->findOrFail($id);
		$partner->restore();

		Session::flash('flash_message', 'Partner restored!');
		return redirect('admin/retrieve/partner');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function partnerDelete($id){
		$partner = Partner::onlyTrashed()->findOrFail($id);

		// Delete image
		$path = 'images/upload/partner/';
		if (!empty($partner->image) && file_exists(($path . $partner->image))){
			unlink($path . $partner->image);
		}

		$partner->forceDelete();

		Session::flash('flash_message', 'Partner permanently deleted!');

		return redirect('admin/retrieve/partner');
	}


	//User module
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function user(){
		$users = User::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.user.index', compact('users'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function userView($id){
		$user = User::onlyTrashed()->findOrFail($id);
		return view('admin.retrieve.user.show', compact('user'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function userRestore($id){
		$user = User::onlyTrashed()->findOrFail($id);
		$user->restore();

		Session::flash('flash_message', 'User restored!');
		return redirect('admin/retrieve/user');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function userDelete($id){
		$user = User::onlyTrashed()->findOrFail($id);

		// Delete image
		$path = 'images/upload/user/';
		if (!empty($user->image) && file_exists(($path . $user->image))){
			unlink($path . $user->image);
		}

		$user->forceDelete();

		Session::flash('flash_message', 'User permanently deleted!');

		return redirect('admin/retrieve/user');
	}


	//Slide module
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function slide(){
		$slides = Slide::onlyTrashed()->orderBy('deleted_at', 'desc')->get();
		return view('admin.retrieve.slide.index', compact('slides'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function slideView($id){
		$slide = Slide::onlyTrashed()->findOrFail($id);
		return view('admin.retrieve.slide.show', compact('slide'));
	}
	/**
	 * Restore the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function slideRestore($id){
		$slide = Slide::onlyTrashed()->findOrFail($id);
		$slide->restore();

		Session::flash('flash_message', 'Slide restored!');
		return redirect('admin/retrieve/slide');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function slideDelete($id){
		$slide = Slide::onlyTrashed()->findOrFail($id);

		// Delete image
		$path = 'images/upload/slide/';
		if (!empty($slide->image) && file_exists(($path . $slide->image))){
			unlink($path . $slide->image);
		}

		$slide->forceDelete();

		Session::flash('flash_message', 'Slide permanently deleted!');

		return redirect('admin/retrieve/slide');
	}


}
