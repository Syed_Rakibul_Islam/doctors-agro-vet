<?php

namespace App\Http\Controllers\Admin;

use App\Slide;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminSlideController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$slides = Slide::orderBy('id', 'desc')->get();

		return view('admin.slide.index', compact('slides'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return view('admin.slide.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'text1' => 'nullable|max:100',
			'text2' => 'nullable|max:100',
			'text3' => 'nullable|max:100',
			'slide_image' => 'required|image|max:22000',

			'text1_b' => 'nullable|max:100',
			'text2_b' => 'nullable|max:100',
			'text3_b' => 'nullable|max:100'
		]);

		if ($request->file('slide_image')){
			$temp = $request->file('slide_image');
			$path = 'images/upload/slide/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

		}

		$slide = Slide::create($request->except('slide_image'));

		Session::flash('flash_message', 'Slide added!');

		return redirect('admin/slide');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$slide = Slide::findOrFail($id);

		return view('admin.slide.show', compact('slide'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$slide = Slide::findOrFail($id);

		return view('admin.slide.edit', compact('slide'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'text1' => 'nullable|max:100',
			'text2' => 'nullable|max:100',
			'text3' => 'nullable|max:100',
			'slide_image' => 'nullable|image|max:22000',

			'text1_b' => 'nullable|max:100',
			'text2_b' => 'nullable|max:100',
			'text3_b' => 'nullable|max:100'
		]);

		$slide = Slide::findOrFail($id);

		if ($request->file('slide_image')){
			$temp = $request->file('slide_image');
			$path = 'images/upload/slide/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

			if (!empty($slide->image) && file_exists(($path . $slide->image))){
				unlink($path . $slide->image);
			}
		}


		$slide->update($request->except('slide_image'));

		Session::flash('flash_message', 'Slide updated!');

		return redirect('admin/slide');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$item = Slide::findOrFail($id);
		$item->delete();

		Session::flash('flash_message', 'Slide deleted!');

		return redirect('admin/slide');
	}
}
