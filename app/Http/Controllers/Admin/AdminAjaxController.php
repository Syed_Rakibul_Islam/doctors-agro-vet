<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAjaxController extends Controller
{
	public function checkPostName(Request $request){
		$name = strtolower(preg_replace('#[-]+#', '-', preg_replace("/[^A-Za-z0-9\-]/", "", preg_replace('#[ -]+#',"-", $request->name))));
		$newPostName = $name;
		$i = 1;
		while(true){
			$isPostName = Post::where('name', '=', $newPostName)->first();
			if(empty($isPostName) && $newPostName != 'category' && $newPostName != 'date'){
				$postName = $newPostName;
				break;
			}
			else {
				$newPostName = $name . '-' . $i;
				$i++;
			}
		}
		echo $postName;
	}
	public function checkEditPostName(Request $request){
		$name = strtolower(preg_replace('#[-]+#', '-', preg_replace("/[^A-Za-z0-9\-]/", "", preg_replace('#[ -]+#',"-", $request->name))));
		$newPostName = $name;
		$i = 1;
		while(true){
			$isPostName = Post::where('name', '=', $newPostName)->where('id', '<>', $request->id)->first();
			if(empty($isPostName) && $newPostName != 'category' && $newPostName != 'date'){
				$postName = $newPostName;
				break;
			}
			else {
				$newPostName = $name . '-' . $i;
				$i++;
			}
		}
		echo $postName;
	}
	public function checkUsername(Request $request){
		$isUser = User::where('username', $request->username)->first();
		if(empty($isUser)){
			return 'no';
		}
		else{
			return 'yes';
		}
	}
	public function checkUserEmail(Request $request){
		$isUser = User::where('email', $request->email)->first();
		if(empty($isUser)){
			return 'no';
		}
		else{
			return 'yes';
		}
	}
	public function checkProductName(Request $request){
		$name = strtolower(preg_replace('#[-]+#', '-', preg_replace("/[^A-Za-z0-9\-]/", "", preg_replace('#[ -]+#',"-", $request->name))));
		$newProductName = $name;
		$i = 1;
		while(true){
			$isProductName = Product::where('name', '=', $newProductName)->first();
			if(empty($isProductName) && $newProductName != 'category'){
				$productName = $newProductName;
				break;
			}
			else {
				$newProductName = $name . '-' . $i;
				$i++;
			}
		}
		echo $productName;
	}
	public function checkEditProductName(Request $request){
		$name = strtolower(preg_replace('#[-]+#', '-', preg_replace("/[^A-Za-z0-9\-]/", "", preg_replace('#[ -]+#',"-", $request->name))));
		$newProductName = $name;
		$i = 1;
		while(true){
			$isProductName = Product::where('name', '=', $newProductName)->where('id', '<>', $request->id)->first();
			if(empty($isProductName) && $newProductName != 'category'){
				$productName = $newProductName;
				break;
			}
			else {
				$newProductName = $name . '-' . $i;
				$i++;
			}
		}
		echo $productName;
	}
}
