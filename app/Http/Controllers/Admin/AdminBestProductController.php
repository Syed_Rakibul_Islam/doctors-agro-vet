<?php

namespace App\Http\Controllers\Admin;

use App\BestProduct;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminBestProductController extends Controller
{
    public function index(){
    	$bestProducts = BestProduct::all();
    	return view('admin.best-product.index', compact('bestProducts'));
    }
    public function show($id){
		$bestProduct = BestProduct::findOrFail($id);
		return view('admin.best-product.show', compact('bestProduct'));
	}
	public function edit( $id ) {
		$bestProduct = BestProduct::findOrFail($id);
		$products = Product::pluck('title', 'id');
		return view('admin.best-product.edit', compact('bestProduct', 'products'));
	}
	public function update($id, Request $request) {
		$this->validate($request, [
			'description' => 'nullable|max:10000',
			'description_b' => 'nullable|max:10000'
		]);

		$bestProduct = BestProduct::findOrFail($id);

		$bestProduct->update($request->all());

		Session::flash('flash_message', 'Best Product updated!');

		return redirect('admin/best-product');
	}
}
