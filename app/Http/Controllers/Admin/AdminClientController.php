<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\District;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminClientController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$clients = Client::orderBy('id', 'desc')->get();

		return view('admin.client.index', compact('clients'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		$districts = District::pluck('name', 'id');
		return view('admin.client.create', compact('districts'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'email' => 'nullable|max:100',
			'contact' => 'nullable|max:100',
			'client_image' => 'nullable|image|max:10000',
			'location' => 'nullable|max:100',
			'latitude' => 'nullable|max:50',
			'longitude' => 'nullable|max:50',
			'district_id' => 'nullable|integer',
			'about' => 'nullable|max:100000',

			'name_b' => 'nullable|max:100'
		]);

		if ($request->file('client_image')){
			$temp = $request->file('client_image');
			$path = 'images/upload/client/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

		}

		$client = Client::create($request->except('client_image'));

		Session::flash('flash_message', 'Client added!');

		return redirect('admin/client');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$client = Client::findOrFail($id);

		return view('admin.client.show', compact('client'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$client = Client::findOrFail($id);
		$districts = District::pluck('name', 'id');

		return view('admin.client.edit', compact('client', 'districts'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'email' => 'nullable|max:100',
			'contact' => 'nullable|max:100',
			'client_image' => 'nullable|image|max:10000',
			'location' => 'nullable|max:100',
			'latitude' => 'nullable|max:50',
			'longitude' => 'nullable|max:50',
			'district_id' => 'nullable|integer',
			'about' => 'nullable|max:100000',

			'name_b' => 'nullable|max:100'
		]);

		$client = Client::findOrFail($id);

		if ($request->file('client_image')){
			$temp = $request->file('client_image');
			$path = 'images/upload/client/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

			if(!empty($client->image)){
				$fileName = $path.$client->image;
				if(file_exists($fileName)){
					unlink($fileName);
				}
			}
		}


		$client->update($request->except('client_image'));

		Session::flash('flash_message', 'Client updated!');

		return redirect('admin/client');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$item = Client::findOrFail($id);
		$item->delete();

		Session::flash('flash_message', 'Client deleted!');

		return redirect('admin/client');
	}
}
