<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Partner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminPartnerController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$partners = Partner::orderBy('id', 'desc')->get();

		return view('admin.partner.index', compact('partners'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		$countries = Country::pluck('name', 'id');
		return view('admin.partner.create', compact('countries'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'email' => 'nullable|max:100',
			'contact' => 'nullable|max:100',
			'partner_image' => 'nullable|image|max:10000',
			'address' => 'nullable|max:100',
			'url' => 'nullable|max:100',
			'about' => 'nullable|max:100000',

			'name_b' => 'nullable|max:100'
		]);

		if ($request->file('partner_image')){
			$temp = $request->file('partner_image');
			$path = 'images/upload/partner/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

		}

		$partner = Partner::create($request->except('partner_image'));

		Session::flash('flash_message', 'Partner added!');

		return redirect('admin/partner');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$partner = Partner::findOrFail($id);

		return view('admin.partner.show', compact('partner'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$partner = Partner::findOrFail($id);
		$countries = Country::pluck('name', 'id');

		return view('admin.partner.edit', compact('partner', 'countries'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'email' => 'nullable|max:100',
			'contact' => 'nullable|max:100',
			'partner_image' => 'nullable|image|max:10000',
			'address' => 'nullable|max:100',
			'url' => 'nullable|max:100',
			'about' => 'nullable|max:100000',

			'name_b' => 'nullable|max:100'
		]);

		$partner = Partner::findOrFail($id);

		if ($request->file('partner_image')){
			$temp = $request->file('partner_image');
			$path = 'images/upload/partner/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

			if (!empty($partner->image) && file_exists(($path . $partner->image))){
				unlink($path . $partner->image);
			}
		}


		$partner->update($request->except('partner_image'));

		Session::flash('flash_message', 'Partner updated!');

		return redirect('admin/partner');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$item = Partner::findOrFail($id);
		$item->delete();

		Session::flash('flash_message', 'Partner deleted!');

		return redirect('admin/partner');
	}
}
