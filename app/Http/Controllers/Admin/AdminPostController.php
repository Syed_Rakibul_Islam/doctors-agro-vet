<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Comment;
use App\Post;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->get();

        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
    	$categories = Category::where('type', 'post')->pluck('name', 'id');
        return view('admin.post.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
	    $request->request->add(['created_by' => Auth::user()->id, 'status' => 'published']);
	    $this->validate($request, [
		    'name' => 'required|max:100',
		    'title' => 'required|max:100',
		    'description' => 'nullable|max:100000',
		    'feature_image' => 'nullable|image|max:20000',

		    'title_b' => 'nullable|max:100',
		    'description_b' => 'nullable|max:100000'
	    ]);

	    if ($request->file('feature_image')){
		    $temp = $request->file('feature_image');
		    $path = 'images/upload/news/';
		    $img_name = $temp->getClientOriginalName();
		    $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
		    $full_name = $dateTime.'__'.$img_name;
		    $adding = $temp->move($path, $full_name);
		    $request->merge(['image' => $full_name]);

	    }

        $post = Post::create($request->except('feature_image', 'category'));
	    if(!empty($request->category)){
		    $post->categories()->attach($request->category);
	    }

        Session::flash('flash_message', 'Post added!');

        return redirect('admin/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('admin.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
	    $categories = Category::where('type', 'post')->pluck('name', 'id');

        return view('admin.post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
	    $request->request->add(['created_by' => Auth::user()->id, 'status' => 'published']);
	    $this->validate($request, [
		    'name' => 'required|max:100',
		    'title' => 'required|max:100',
		    'description' => 'nullable|max:100000',
		    'feature_image' => 'nullable|image|max:20000',

		    'title_b' => 'nullable|max:100',
		    'description_b' => 'nullable|max:100000'
	    ]);

	    $post = Post::findOrFail($id);

	    if ($request->file('feature_image')){
		    $temp = $request->file('feature_image');
		    $path = 'images/upload/news/';
		    $img_name = $temp->getClientOriginalName();
		    $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
		    $full_name = $dateTime.'__'.$img_name;
		    $adding = $temp->move($path, $full_name);
		    $request->merge(['image' => $full_name]);

		    if(!empty($post->image) && file_exists(($path . $post->image))){
		    	unlink($path . $post->image);
		    }
	    }


	    $post->update($request->except('feature_image', 'category'));
	    if(!empty($request->category)){
		    $post->categories()->sync($request->category);
	    }

        Session::flash('flash_message', 'Post updated!');

        return redirect('admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $item = Post::findOrFail($id);
        $item->delete();

        Session::flash('flash_message', 'Post deleted!');

        return redirect('admin/news');
    }
	public function comments($id)
	{
		$comments = Comment::where('post_id', $id)->get();
		$postId = $id;
		return view('admin.post.comments', compact('comments', 'postId'));
	}
	public function comment($id, $comment_id)
	{
		$comment = Comment::where('post_id', $id)->where('id', $comment_id)->first();
		$postId = $id;
		return view('admin.post.comment', compact('comment', 'postId'));
	}
	public function approve($id, $comment_id, Request $request){
		$this->validate($request, [
			'status' => 'required'
		]);
		if($request->status == 'approve'){
			$comment = Comment::findOrFail($comment_id);
			$comment->update(['approve' => 1]);
		}

		Session::flash('flash_message', 'Comment Approved!');

		return redirect('admin/news/' . $id . '/comment/' . $comment_id);
	}
	public function delete($id, $comment_id){
		$item = Comment::findOrFail($comment_id);
		$item->delete();

		Session::flash('flash_message', 'Comment deleted!');

		return redirect('admin/news/' . $id . '/comment');
	}
}
