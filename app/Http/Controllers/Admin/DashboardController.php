<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Client;
use App\Partner;
use App\Post;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
		$totalProduct = Product::count('id');
		$totalClient = Client::count('id');
		$totalNews = Post::count('id');
		$totalPartner = Partner::count('id');
		$postCategories = Category::where('type', 'post')->get();
		$productCategories = Category::where('type', 'product')->get();
		return view('admin.dashboard', compact('totalProduct', 'totalClient', 'totalNews', 'totalPartner', 'postCategories', 'productCategories'));
	}
}
