<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;

class AdminProfileController extends Controller
{
    public function index()
    {
        return view('admin.profile.index');
    }
    public function profileEdit()
    {
        $user = Auth::user();
        return view('admin.profile.edit', compact('user'));
    }
    public function profileSave(Request $request){
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150',
            'contact' => 'nullable|max:255',
            'user_image' => 'nullable|image|max:10000',
            'address' => 'nullable|max:255',
            'about' => 'nullable|max:255',
        ]);
        if ($validator->fails())
        {
            return redirect('admin/profile/edit')->withErrors($validator)->withInput();
        }
        if ($request->file('user_image')){
            $temp = $request->file('user_image');
            $path = 'images/upload/user/';
            $img_name = $temp->getClientOriginalName();
            $dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
            $full_name = $dateTime.'__'.$img_name;
            $adding = $temp->move($path, $full_name);
            $request->merge(['image' => $full_name]);

            if(!empty($user->image)){
                $fileName = $path.$user->image;
                if(file_exists($fileName)){
                    unlink($fileName);
                }
            }

        }

        $user->update([
            'name' => $request->name,
            'contact' => $request->contact,
            'image' => $request->image,
            'address' => $request->address,
            'about' => $request->about,
        ]);

        Session::flash('flash_message', 'Profile Updated!');

        return redirect('admin/profile');
    }
    public function changePassword(){
        return view('admin.profile.change-password');
    }
    public function savePassword(Request $request){

        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:6|max:255',
            'password' => 'required|string|min:6||max:255|confirmed'
        ]);
        if ($validator->fails())
        {
            return redirect('admin/profile/change-password')->withErrors($validator)->withInput();
        }
        if(Hash::check($request->old_password, $user->password)){
            $user->update([
                'password' => Hash::make($request->password)
            ]);

            Session::flash('flash_message', 'Password Updated!');

            return redirect('admin/profile');
        }
        else {
            Session::flash('warning_msg', 'Old Password does not match!');
        }
        return redirect('admin/profile/change-password');
    }
}