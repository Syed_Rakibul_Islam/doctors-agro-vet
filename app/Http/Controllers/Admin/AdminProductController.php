<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminProductController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$products = Product::orderBy('id', 'desc')->get();

		return view('admin.product.index', compact('products'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		$categories = Category::where('type', 'product')->pluck('name', 'id');
		return view('admin.product.create', compact('categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'title' => 'required|max:100',
			'description' => 'nullable|max:100000',
			'feature_image' => 'nullable|image|max:10000',
			'price' => 'nullable|max:100',
			'status' => 'nullable|max:30',

			'title_b' => 'nullable|max:100',
			'description_b' => 'nullable|max:100000'
		]);

		if ($request->file('feature_image')){
			$temp = $request->file('feature_image');
			$path = 'images/upload/product/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);
		}

		$product = Product::create($request->except('feature_image', 'category'));
		if(!empty($request->category)){
			$product->categories()->attach($request->category);
		}

		Session::flash('flash_message', 'Product added!');

		return redirect('admin/product');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$product = Product::findOrFail($id);

		return view('admin.product.show', compact('product'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$product = Product::findOrFail($id);
		$categories = Category::where('type', 'product')->pluck('name', 'id');

		return view('admin.product.edit', compact('product', 'categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'title' => 'required|max:100',
			'description' => 'nullable|max:100000',
			'feature_image' => 'nullable|image|max:10000',
			'price' => 'nullable|max:100',
			'status' => 'nullable|max:30',

			'title_b' => 'nullable|max:100',
			'description_b' => 'nullable|max:100000'
		]);

		$product = Product::findOrFail($id);

		if ($request->file('feature_image')){
			$temp = $request->file('feature_image');
			$path = 'images/upload/product/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

			if(!empty($product->image) && file_exists(($path . $product->image))){
				unlink($path. $product->image);
			}
		}


		$product->update($request->except('feature_image', 'category'));
		if(!empty($request->category)){
			$product->categories()->sync($request->category);
		}

		Session::flash('flash_message', 'Product updated!');

		return redirect('admin/product');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$item = Product::findOrFail($id);
		$item->delete();

		Session::flash('flash_message', 'Product deleted!');

		return redirect('admin/product');
	}
//	public function comments($id)
//	{
//		$comments = Comment::where('product_id', $id)->get();
//		$postId = $id;
//		return view('admin.posts.comments', compact('comments', 'postId'));
//	}
//	public function comment($id, $comment_id)
//	{
//		$comment = Comment::where('post_id', $id)->where('id', $comment_id)->first();
//		$postId = $id;
//		return view('admin.posts.comment', compact('comment', 'postId'));
//	}
}
