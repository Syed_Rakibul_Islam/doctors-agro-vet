<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class AdminPostCategoryController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$categories = Category::where('type', 'post')->orderBy('id', 'desc')->get();
		return view('admin.post.category.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return view('admin.post.category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$request->request->add(['type' => 'post']);
		$this->validate($request, [
			'name' => 'required|max:100',
			'name_b' => 'nullable|max:100',
			'description' => 'nullable|max:100000',
			'description_b' => 'nullable|max:100000'
		]);

		Category::create($request->all());

		Session::flash('flash_message', 'Category added!');

		return redirect('admin/news/category');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$category = Category::findOrFail($id);

		return view('admin.post.category.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$category = Category::findOrFail($id);

		return view('admin.post.category.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'name_b' => 'nullable|max:100',
			'description' => 'nullable|max:100000',
			'description_b' => 'nullable|max:100000'
		]);

		$category = Category::findOrFail($id);

		$category->update($request->all());

		Session::flash('flash_message', 'Category updated!');

		return redirect('admin/news/category');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$category = Category::findOrFail($id);
		$category->delete();

		Session::flash('flash_message', 'Category deleted!');

		return redirect('admin/news/category');
	}
}
