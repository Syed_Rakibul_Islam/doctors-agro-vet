<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $comments = Comment::orderBy('id', 'desc')->get();

        return view('admin.post.comment.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
    	$posts = Post::pluck('name', 'id');
        return view('admin.post.comment.create', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
	    $request->request->add(['user_is' => Auth::user()->id, 'approve' => 1]);
	    $this->validate($request, [
		    'post_id' => 'required',
		    'description' => 'required|max:100000'
	    ]);
        
        Comment::create($request->all());

        Session::flash('flash_message', 'Comment added!');

        return redirect('admin/news/comment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $comment = Comment::findOrFail($id);

        return view('admin.post.comment.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
//    public function edit($id)
//    {
//        $comment = Comment::findOrFail($id);
//
//        return view('admin.posts.comments.edit', compact('comment'));
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
	    $this->validate($request, [
		    'status' => 'required'
	    ]);
	    if($request->status == 'approve'){
		    $comment = Comment::findOrFail($id);
		    $comment->update(['approve' => 1]);
	    }

        Session::flash('flash_message', 'Comment Approved!');

        return redirect('admin/news/comment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $item = Comment::findOrFail($id);

        //update total comment
        $post = Post::withTrashed()->find($item->post_id);
        $totalComment = $post->total_comment - 1;
        $post->update(['total_comment' => $totalComment]);

        $item->delete();

        Session::flash('flash_message', 'Comment deleted!');

        return redirect('admin/news/comment');
    }
}
