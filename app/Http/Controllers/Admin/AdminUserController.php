<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminUserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$users = User::where('role', '<>', 'super')->orderBy('id', 'desc')->get();
		return view('admin.user.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return view('admin.user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'username' => 'required|unique:users|max:100',
			'password' => 'required|max:100',
			'name_b' => 'nullable|max:100',
			'email' => 'required|unique:users|max:100',
			'contact' => 'nullable|max:100',
			'address' => 'nullable|max:255',
			'role' => 'nullable|max:5',
			'about' => 'nullable|max:100000'
		]);
		if ($request->file('user_image')){
			$temp = $request->file('user_image');
			$path = 'images/upload/user/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);
		}
		$request->password = bcrypt($request->password);

		$user = User::create($request->except('user_image'));


		Session::flash('flash_message', 'User added!');

		return redirect('admin/user');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);
		if($user->role != 'super'){

			return view('admin.user.show', compact('user'));
		}
		Session::flash('warning_msg', 'Can\'t view this user info!');
		return redirect('admin/user');


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$user = User::findOrFail($id);
		if(($user->role != 'admin' || Auth::user()->role == 'super' ) && $user->role != 'super'){

			return view('admin.user.edit', compact('user'));
		}
		Session::flash('warning_msg', 'Can\'t edit this user!');
		return redirect('admin/user');

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request)
	{
		$this->validate($request, [
			'name' => 'required|max:100',
			'name_b' => 'nullable|max:100',
			'contact' => 'nullable|max:100',
			'address' => 'nullable|max:255',
			'role' => 'nullable|max:5',
			'about' => 'nullable|max:100000'
		]);

		$user = User::findOrFail($id);

		if ($request->file('user_image')){
			$temp = $request->file('user_image');
			$path = 'images/upload/user/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

			if (!empty($user->image) && file_exists(($path . $user->image))){
				unlink($path . $user->image);
			}
		}

		$user->update($request->except('user_image'));

		Session::flash('flash_message', 'User updated!');

		return redirect('admin/user');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy($id)
	{
		$user = User::findOrFail($id);
		if(($user->role != 'admin' || Auth::user()->role == 'super' ) && $user->role != 'super'){
			$user->delete();
			Session::flash('flash_message', 'User deleted!');

			return redirect('admin/user');
		}
		Session::flash('warning_msg', 'Can\'t delete this user!');

		return redirect('admin/user');
	}
}
