<?php

namespace App\Http\Controllers;

use App\BestProduct;
use App\Partner;
use App\Slide;
use Illuminate\Http\Request;
use Session;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$slides = Slide::orderBy('id', 'desc')->get();
    	$bestProducts = BestProduct::all();
    	$partners = Partner::all();

        return view('home', compact('slides', 'bestProducts', 'partners'));
    }
	public function company()
	{
		return view('company');
	}

}
