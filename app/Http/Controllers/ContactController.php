<?php

namespace App\Http\Controllers;

use App\Mail\SendContactInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class ContactController extends Controller
{
	public function index()
	{
		return view('contact');
	}
	public function saveContact(Request $request){
		$validator = Validator::make($request->all(), [
			'contact_name' => 'required',
			'contact_email' => 'required',
			'contact_subject' => 'required',
			'contact_message' => 'required',
			'g-recaptcha-response' => 'required|recaptcha'
		]);

		if ( $validator->fails() ) {
			$fields = "";
			if( !isset( $request->contact_name ) || empty( $request->contact_name ) ) {
				$fields .= "Name";
			}

			if( !isset( $request->contact_email ) || empty( $request->contact_email ) ) {
				if( $fields == "" ) {
					$fields .= "Email";
				} else {
					$fields .= ", Email";
				}
			}

			if( !isset( $request->contact_subject ) || empty( $request->contact_subject ) ) {
				if( $fields == "" ) {
					$fields .= "Subject";
				} else {
					$fields .= ", Subject";
				}
			}

			if( !isset( $request->contact_message ) || empty( $request->contact_message ) ) {
				if( $fields == "" ) {
					$fields .= "Message";
				} else {
					$fields .= ", Message";
				}
			}
			$json_arr = array( "type" => "error", 'msg' => 'Please fill ' . $fields . ' fields!' );
			return json_encode( $json_arr );
		}
		else {
			Mail::to('contact@doctorsagrovetltd.com')->send(new SendContactInformation($request->contact_name, $request->contact_email, $request->contact_subject, $request->contact_message));
			$json_arr = array( "type" => "success", "msg" => 'Sent your information! Thank you for connect with us' );
			return json_encode( $json_arr );
		}
	}
	public function showContactData(){
		return json_encode(
			$data = array(
				[
					'id' => 1,
					'latitude' => "23.745586",
					'longitude' => "90.394207",
					'title' => "Head Office",
					'name' => "Doctor's Agro-vet Ltd",
					'location' => "Nurjehan Tower (5th Floor), 2, Link Road, Banglamotor, Dhaka-1200, Bangladesh",
					'phone' => "8802-9673737",
					'email' => "info@doctorsagrovetltd.com",
					'image' => asset('images/marker.png'),
				],
				[
					'id' => 2,
					'latitude' => "24.8204314",
					'longitude' => "89.1034407",
					'title' => "Naogaon Depot + Factory",
					'name' => "Md. Abdul Karim",
					'location' => "BSCIC, Naogaon",
					'phone' => "01728-657036",
					'email' => "info@doctorsagrovetltd.com",
					'image' => asset('images/factory.png'),
				],
				[
					'id' => 3,
					'latitude' => "25.6541719",
					'longitude' => "89.4634015",
					'title' => "Rangpur Depot",
					'name' => "Md. Golam Kibria Shamim",
					'location' => "New Adorshopara, Allomnagor, Rangpur",
					'phone' => "01730-072078",
					'email' => "info@doctorsagrovetltd.com",
					'image' => asset('images/factory.png'),
				],
				[
					'id' => 4,
					'latitude' => "24.611477",
					'longitude' => "90.031629",
					'title' => "Modhupur Depot",
					'name' => "Md. Naeem Hossain",
					'location' => "Mymanshing Road,Charaljani, Modhupur, Tangail",
					'phone' => "01781-820091",
					'email' => "info@doctorsagrovetltd.com",
					'image' => asset('images/factory.png'),
				],
				[
					'id' => 3,
					'latitude' => "23.163839",
					'longitude' => "89.1932717",
					'title' => "Jessore Depot ",
					'name' => "Md. Aslam Hossain",
					'location' => "Rojiendel, Dharmotala Khaladanga, Dhaka Road, Jessore",
					'phone' => "01754-252933 ",
					'email' => "info@doctorsagrovetltd.com",
					'image' => asset('images/factory.png'),
				],
				[
					'id' => 4,
					'latitude' => "25.6250352",
					'longitude' => "88.9523172",
					'title' => "Rangpur Territory ",
					'name' => "",
					'location' => "Rangpur",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 5,
					'latitude' => "26.1269833",
					'longitude' => "88.9568665",
					'title' => "Lalmonirhat Territory ",
					'name' => "",
					'location' => "Lalmonirhat",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 6,
					'latitude' => "25.9405241",
					'longitude' => "88.2250559",
					'title' => "Thakurgoan Territory ",
					'name' => "",
					'location' => "Thakurgoan",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 7,
					'latitude' => "25.6433841",
					'longitude' => "88.5630214",
					'title' => "Dinajpur Territory ",
					'name' => "",
					'location' => "Dinajpur",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 8,
					'latitude' => "25.4385027",
					'longitude' => "89.2825411",
					'title' => "Pirgonj Territory ",
					'name' => "",
					'location' => "Pirgonj",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 9,
					'latitude' => "25.3399262",
					'longitude' => "89.1913556",
					'title' => "Gaibandha Territory ",
					'name' => "",
					'location' => "Gaibandha",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 10,
					'latitude' => "25.8071398",
					'longitude' => "89.3864996",
					'title' => "Kurigram Territory ",
					'name' => "",
					'location' => "Kurigram",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 11,
					'latitude' => "25.646854",
					'longitude' => "89.0382666",
					'title' => "Badargonj Territory ",
					'name' => "",
					'location' => "Badargonj",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 12,
					'latitude' => "25.8776819",
					'longitude' => "89.1440942",
					'title' => "Gongachara Territory ",
					'name' => "",
					'location' => "Gongachara",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 13,
					'latitude' => "26.0264722",
					'longitude' => "88.6871006",
					'title' => "Nilphamary Territory ",
					'name' => "",
					'location' => "Nilphamary",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 14,
					'latitude' => "25.9351159",
					'longitude' => "88.5453394",
					'title' => "Birgong Territory ",
					'name' => "",
					'location' => "Birgong",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
				[
					'id' => 15,
					'latitude' => "23.660005",
					'longitude' => "90.086998",
					'title' => "Nobabgonj Territory ",
					'name' => "",
					'location' => "Nobabgonj",
					'phone' => "",
					'email' => "",
					'image' => asset('images/client.png'),
				],
			)
		);
	}
}
