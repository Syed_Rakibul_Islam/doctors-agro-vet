<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\PostLike;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\TwitterCard;
use Carbon\Carbon;
use EasyBanglaDate\Types\BnDateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use View;
use SEO;

class NewsController extends Controller
{
	public function __construct() {
		View::share ( 'sharedCategories', Category::where('type', 'post')->get() );
		View::share ( 'sharedPosts', Post::where('status', 'published')
		                                ->orderBy('id', 'desc')
										->limit(3)
		                                ->get() );
	}

	public function index(Request $request){
    	$posts = Post::where('status', 'published')
	                 ->orderBy('id', 'desc')
	                 ->paginate(5);

    	// SEO
		SEO::setTitle(config('app.name') . ' | News');
		SEO::opengraph()->setUrl(url('news/news'));
		SEO::opengraph()->addProperty('type', 'article');
		SEO::twitter()->setSite('@' . config('app.name'));

	    return view('news.index', compact('posts'));
    }
	public function single(Request $request){
		$post = Post::where('name', $request->name)->firstOrFail();

		// SEO
		$categories = '';
		foreach($post->categories as $category){
			$categories .= $category . ', ';
		}
		$author = '';
		if(isset($post->author->name)) $author = $post->author->name;
		SEOMeta::setTitle(config('app.name') . ' | News | ' . $post->title)
			->setDescription(strip_tags($post->description))
			->addMeta('article:published_time', Carbon::parse($post->created_at)->timezone('Asia/Dhaka')->format('F d, Y'), 'property')
			->addMeta('article:section', $categories, 'property')
			->addKeyword([config('app.name'), $post->name, $post->title, $post->title_b, strip_tags($post->description), strip_tags($post->description_b), $author]);
		OpenGraph::setTitle(config('app.name') . ' | News | ' . $post->title)
	         ->setDescription(strip_tags($post->description))
	         ->setArticle([
				'published_time' => Carbon::parse($post->created_at)->timezone('Asia/Dhaka')->format('F d, Y'),
				'author' => $author,
				'site' => config('app.name'),
				'tag' => $categories
			])
			->setUrl(url('news/' . $post->name))
			->addImage(asset('images/upload/news' . $post->image))
			->setType('article');
		TwitterCard::setTitle(config('app.name') . ' | News | ' . $post->title)
			->setDescription(strip_tags($post->description))
			->setSite('@' . config('app.name'))
			->setType('article')
			->setSite(config('app.name'))
			->setUrl(url('news/' . $post->name))
			->setImage(asset('images/upload/news' . $post->image));



		return view('news.single', compact('post'));
	}
	public function category(){
		return view('news.category');
	}
	public function date(){
		$posts = DB::table('posts')
			->selectRaw('DATE(posts.created_at) day')
			->groupBy('day')
			->orderBy('id', 'desc')
			->get();
		return view('news.date', compact('posts'));
	}
	public function categoryArchive(Request $request){
		$categoryName = $request->category;
		$category = Category::where('type', 'post')->where('name', $request->category)->firstOrFail();;
		$posts = Post::whereHas('categories', function ($category) use($categoryName){
					$category->where('name', $categoryName);
				})
	                ->orderBy('id', 'desc')
					->paginate(5);
		return view('news.category-archive', compact('category', 'posts'));
	}
	public function dateArchive(Request $request){
		/*Year*/
		$year = $request->year;

		/*Month*/
		if(isset($request->month) && $request->month > 12) return view('404');
		$month =  isset($request->month) ? $request->month : '01';

		/*Day*/
		if(isset($request->day) && $request->day > Carbon::create($year, $month)->endOfMonth()->format('d')) return view('404');
		$day = isset($request->day) ? $request->day : '01';

		/*Date*/
		$date = Carbon::create($year, $month, $day, 00, 00, 00)->addHours(-6);
		if(!isset($request->day) && !isset($request->month)){
			$endDate = Carbon::create($year, 12, 31, 18, 59, 59);
		}
		elseif (!isset($request->day)){
			$endDate = Carbon::create($year, $month, $day)->endOfMonth()->addHours(-6)->addMinutes(59)->addSeconds(59);
		}
		else{
			$endDate = Carbon::parse($date)->addHours(24)->addMinutes(59)->addSeconds(59);
		}
		$posts = Post::whereBetween('created_at', [$date, $endDate])
		             ->orderBy('id', 'desc')
		             ->paginate(5);
		return view('news.date-archive', compact( 'posts'));
	}
	public function postLike(Request $request){
		if(Auth::check()){
			$postLike = PostLike::where('post_id', $request->post_id)
				->where('user_id', Auth::user()->id)->first();;
			$post = Post::findOrFail($request->post_id);
			if(empty($postLike)){
				PostLike::create([
					'post_id' => $request->post_id,
					'user_id' => Auth::user()->id
					]);
				$totalLike = empty($post->total_like) ? 1 : $post->total_like + 1;
				$post->update(['total_like' => $totalLike]);
			}
			else{
				$totalLike = $post->total_like -  1;
				$post->update(['total_like' => $totalLike]);
				$postLike->delete();
			}

			echo $totalLike;

		}
	}
	public function postComment(Request $request){
		if(Auth::check()){
			Auth::user()->role == 'admin' || Auth::user()->role == 'super' ? $request->request->add(['approve' => 1, 'user_id' => Auth::user()->id]) : $request->request->add(['user_id' => Auth::user()->id]);
			$this->validate($request, [
				'post_id' => 'required',
				'description' => 'required|max:100000',
			]);
			$comment = Comment::create($request->all());
		}
		else{
			$this->validate($request, [
				'post_id' => 'required',
				'user_name' => 'required:max:100',
				'user_email' => 'required:max:100',
				'description' => 'required|max:100000',
			]);

			$comment = Comment::create($request->all());
		}
		$post = Post::findOrFail($request->post_id);
		$totalComment = empty($post->total_comment) ? 1 : $post->total_comment + 1;
		$post->update(['total_comment' => $totalComment]);


		/*Send data*/
		if(!empty($comment->user_name)){
			$userName = $comment->user_name;
		}
		else{
			$userName = isset($comment->user->name) ? $comment->user->name : __('msg.anonymous');
		}
		if(isset($comment->user->id) && !empty($comment->user->image) && file_exists('images/upload/user/' . $comment->user->image)){
			$imgUrl = asset('images/upload/user/' . $comment->user->image);
		}
		else{
			$imgUrl = asset('images/av.png');
		}
		if($request->localization == "bn"){
			$bnDate = new BnDateTime($comment->created_at, new \DateTimeZone('Asia/Dhaka'));
			$date =  $bnDate->getDateTime()->format('F d, Y h:i A'). PHP_EOL;
		}
		else{
			$date = Carbon::parse($comment->created_at)->timezone('Asia/Dhaka')->format('F d, Y h:i A');
		}

		$data = [
			'user_name' => $userName,
			'image' => $imgUrl,
			'date' => $date,
			'description' => $comment->description,
			'approve' => $comment->approve
		];
		echo json_encode($data);
	}

}
