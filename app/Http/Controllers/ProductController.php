<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function index(Request $request){
		$productCount = Product::count('id');
		if($request->has('orderby') && $request->orderby == 'asc'){
			$products = Product::paginate(12);
		}
		else{
			$products = Product::orderBy('id', 'desc')->paginate(12);
		}

		return view('product.index', compact('products', 'productCount'));
	}
	public function category(){
		$categories = Category::where('type', 'product')->get();
		return view('product.category', compact('categories'));
	}
	public function categoryArchive(Request $request){
		$categoryName = $request->category;
		$category = Category::where('type', 'product')->where('name', $categoryName)->firstOrFail();
		$productCount = Product::whereHas('categories', function ($category) use($categoryName){
						$category->where('name', $categoryName);
					})
		                       ->count();

		if($request->has('orderby') && $request->orderby == 'asc'){
			$products = Product::whereHas('categories', function ($category) use($categoryName){
				$category->where('name', $categoryName);
			})
			                   ->paginate(12);

		}
		else{
			$products = Product::whereHas('categories', function ($category) use($categoryName){
				$category->where('name', $categoryName);
			})
			                   ->orderBy('id', 'desc')
			                   ->paginate(12);
		}


		return view('product.category-archive', compact('products', 'productCount', 'category'));
	}
	public function single($name){

		$product = Product::where('name', $name)->firstOrFail();
		return view('product.single', compact('product'));
	}
}
