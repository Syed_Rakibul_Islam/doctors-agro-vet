<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class ProfileController extends Controller
{
	public function index()
	{
		return view('profile.index');
	}
	public function profileEdit()
	{
		$user = Auth::user();
		return view('profile.edit', compact('user'));
	}
	public function profileSave(Request $request){
		$user = Auth::user();

		$this->validate($request, [
			'name' => 'required|max:150',
			'contact' => 'nullable|max:255',
			'user_image' => 'nullable|image|max:10000',
			'address' => 'nullable|max:255',
			'about' => 'nullable|max:255',
		]);
		if ($request->file('user_image')){
			$temp = $request->file('user_image');
			$path = 'images/upload/user/';
			$img_name = $temp->getClientOriginalName();
			$dateTime = Carbon::now()->format('Y_m_d_H_i_s_A');
			$full_name = $dateTime.'__'.$img_name;
			$adding = $temp->move($path, $full_name);
			$request->merge(['image' => $full_name]);

			if(!empty($user->image)){
				$fileName = $path.$user->image;
				if(file_exists($fileName)){
					unlink($fileName);
				}
			}

		}

		$user->update([
			'name' => $request->name,
			'contact' => $request->contact,
			'image' => $request->image,
			'address' => $request->address,
			'about' => $request->about,
		]);

		Session::flash('flash_message', 'Profile Updated!');

		return redirect($request->session()->get('locale') . '/profile');
	}
	public function changePassword(){
		return view('profile.change-password');
	}
	public function savePassword(Request $request){
		$user = Auth::user();

		$this->validate($request, [
			'old_password' => 'required|min:6|max:255',
			'password' => 'required|string|min:6||max:255|confirmed'
		]);

		if(Hash::check($request->old_password, $user->password)){
			$user->update([
				'password' => Hash::make($request->password)
			]);

			Session::flash('flash_message', 'Password Updated!');

			return redirect($request->session()->get('locale') . '/profile');
		}
		else {
			Session::flash('warning_msg', 'Old Password does not match!');
		}

		return redirect($request->session()->get('locale') . '/profile/change-password');
	}
}
