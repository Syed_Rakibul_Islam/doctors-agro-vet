<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if(isset(Auth::user()->id)){
		    if (Auth::user()->role == 'super')
		    {
			    return $next($request);
		    }
		    return redirect(URL::previous());
	    }
	    return redirect('/admin');
    }
}
