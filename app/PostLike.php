<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'post_likes';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['post_id', 'user_id'];

	public $timestamps = false;
}
