<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'name_b', 'type', 'description', 'description_b'];

    /**
     * The table column used for soft-delete.
     *
     * @var string
     */
    protected $dates = ['deleted_at'];


    /**
     * The table methods used for relationship with others table.
     *
     * @var methods
     */
	public function posts()
	{
		return $this->belongsToMany('App\Post', 'post_categories');
	}
	public function products()
	{
		return $this->belongsToMany('App\Product', 'product_categories');
	}
    
}
