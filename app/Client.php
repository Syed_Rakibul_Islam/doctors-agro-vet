<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
	use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clients';

	/**
	 * The database primary key value.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'name_b', 'email', 'contact', 'image', 'location', 'latitude', 'longitude',  'about', 'district_id'];

	/**
	 * The table column used for soft-delete.
	 *
	 * @var string
	 */
	protected $dates = ['deleted_at'];

	/**
	 * The table methods used for relationship with others table.
	 *
	 * @var methods
	 */

	public function district(){
		return $this->belongsTo('App\District');
	}
}
