/****** Alert *****/
$(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissable").alert('close');
});
/****** /Alert *****/

$(function() {
    $('.post-like-button').click(function(){

        var thisBlock = $(this);

        $.get($(this).data('base-url') + '/ajax/post-like', {post_id: $(this).data('post-id')}, function(data,status){

            if(thisBlock.find('i').css('color') == 'rgb(255, 0, 0)'){
                thisBlock.find('i').css('color', "#ffffff");
            }
            else{
                thisBlock.find('i').css('color', "#ff0000");
            }
            thisBlock.find('span').html(data);
        });
    });
});