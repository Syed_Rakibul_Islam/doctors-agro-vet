@extends('layouts.app')

@section('pageTitle', '%%modelName%% | View | ' . $%%crudNameSingular%%->%%primaryKey%% )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('design/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('/%%routeGroup%%%%viewName%%') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        @if(!empty(Auth::user()->is_admin))
                            <a href="{{ url('/%%routeGroup%%%%viewName%%/' . $%%crudNameSingular%%->%%primaryKey%% . '/edit') }}" class="btn btn-primary pa-5" title="Edit %%modelName%%"><i class="zmdi zmdi-edit txt-light"></i></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['%%routeGroup%%%%crudName%%', $%%crudNameSingular%%->%%primaryKey%%],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger pa-5',
                                    'title' => 'Delete %%modelName%%',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $%%crudNameSingular%%->%%primaryKey%% }}</td>
                                    </tr>
                                    %%formBodyHtmlForShowView%%
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('design/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('design/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
