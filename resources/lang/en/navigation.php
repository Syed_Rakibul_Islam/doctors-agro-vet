<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Navigation Language Lines
	|--------------------------------------------------------------------------
	|
	|
	|
	|
	|
	*/

	'app_name' => 'Doctor\'s Agro-vet',
	'home' => 'Home',
	'company' => 'Company',
	'our_company' => 'Our Company',
	'product' => 'Product',
	'products' => 'Products',
	'all_products' => 'All Products',
	'client' => 'Client',
	'clients' => 'Clients',
	'partners' => 'Partners',
	'global_partners' => 'Global Partners',
	'news' => 'News',
	'contact' => 'Contact',
	'search' => 'Search',
	'category' => 'Category',
	'categories' => 'Categories',
	'date' => 'Date',
	'month' => 'Month',
	'archives' => 'Archives',
	'account' => 'Account',
	'sign_up' => 'Sign Up',
	'sign_in' => 'Sign In',
	'sign_out' => 'Sign Out',
	'forgotten_acc' => 'Forgotten Account',
	'profile' => 'Profile',
	'my_profile' => 'My Profile',
	'edit' => 'Edit',
	'change_password' => 'Change Password',
	'district' => 'District',
	'reset_password' => 'Reset Password'
];