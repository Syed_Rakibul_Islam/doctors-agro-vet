<?php

return [

	'not_found' => 'Sorry! Did not find any :name.',
	'oops' => 'Oops',
	'error_404' => 'Error 404',
	'pnf' => 'Page Not Found',
	'latest_post' => 'Latest Posts',
	'anonymous' => 'Anonymous',
	'undefined' => 'Undefined',
	'result_msg'=> 'Showing  :show results from :total',
	'new' => 'New',
	'out' => 'Out',
	'newest_first' => 'Newest First',
	'oldest_first' => 'oldest First',
	'is_acc' => 'Already have an account?',
	'no_acc' => 'Have not any account?',
	'reset_password' => 'Reset Password',
	'latest_news' => 'Latest News',
	'info_updated' => ':name Updated',
	'add_comment' => "ADD A COMMENT",
	'comments' => 'Comments'

];