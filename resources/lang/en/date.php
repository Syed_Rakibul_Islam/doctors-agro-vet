<?php

return [
	'date' => 'Date',
	'dates' => 'Dates',
	'month' => 'Month',
	'months' => 'Months',
	'january' => 'January',
	'february' => 'February',
	'march' => 'March',
	'april' => 'April',
	'may' => 'May',
	'june' => 'June',
	'july' => 'July',
	'august' => 'August',
	'september' => 'September',
	'october' => 'October',
	'november' => 'November',
	'december' => 'December',
	'number-month' => [
		'01' => 'January',
		'02' => 'February',
		'03' => 'March',
		'04' => 'April',
		'05' => 'May',
		'06' => 'June',
		'07' => 'July',
		'08' => 'August',
		'09' => 'September',
		'10' => 'October',
		'11' => 'November',
		'12' => 'December',
	]
];