<?php

return [

	'not_found' => 'দুঃখিত! কোন :name পাওয়া যায়নি।',
	'oops' => 'ওহো',
	'error_404' => 'ভুল ৪০৪',
	'pnf' => 'পৃষ্ঠা খুঁজে পাওয়া যায়নি',
	'latest_post' => 'সর্বশেষ সংবাদ',
	'anonymous' => 'অজ্ঞাতনামা',
	'undefined' => 'অনির্দিষ্ট',
	'result_msg'=> ':total এর মধ্য :show টি ফলাফল দেখাচ্ছে',
	'new' => 'নতুন',
	'out' => 'শেষ',
	'newest_first' => 'নতুন প্রথম',
	'oldest_first' => 'পুরাতন প্রথম',
	'is_acc' => 'অ্যাকাউন্ট খোলা আছে?',
	'no_acc' => 'অ্যাকাউন্ট খোলা নাই?',
	'reset_password' => 'পাসওয়ার্ড রিসেট',
	'latest_news' => 'নতুন নিউজ',
	'info_updated' => ':name আপডেট হয়েছে',
	'add_comment' => "কমেন্ট করুন",
	'comments' => 'কমেন্ট'

];