<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

	'failed' => 'এই তথ্যগুলো আমাদের রেকর্ডগুলির সাথে মেলে না।',
	'throttle' => 'অনেকবার লগইন প্রচেষ্টা। :seconds সেকেন্ড পরে আবার চেষ্টা করুন।',
	'username' => 'ইউজারনেম',
	'password' => 'পাসওয়ার্ড',
	'email' => 'ইমেইল',
	'submit' => 'সাবমিট',
	'name' => 'নাম',
	'contact_no' => 'যোগাযোগের নাম্বার',
	'address' => 'ঠিকানা',
	'password_confirmation' => 'পাসওয়ার্ড নিশ্চিত করুন',
	'about' => 'আমার সম্পর্কে',
	'old_password' => 'আগের পাসওয়ার্ড',
	'subject' => 'সাবজেক্ট',
	'message' => 'মেসেজ',
	'comment' => 'কমেন্ট'

];