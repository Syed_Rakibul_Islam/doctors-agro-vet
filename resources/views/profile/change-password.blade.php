@extends('layouts.app')

@section('pageTitle', __('navigation.change_password'))

@section('content')

    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-100"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form id="login-form" class="login-form" method="post" action="{{ url('profile/change-password') }}">
                                <h1 class="text-center"> {{ __('navigation.change_password') }}</h1>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                                            <input type="password" name="old_password" class="form-control" placeholder="{{ __('auth.old_password') }} *" required />
                                            @if ($errors->has('old_password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('old_password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input type="password" name="password" class="form-control" placeholder=" {{ __('msg.new') }} {{ __('auth.password') }} *" required />
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="{{ __('auth.password_confirmation') }} *" required />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" value="{{ __('auth.submit') }}" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-100"></div>
        </div><!-- Contact Forms Section /- -->
    </main>

@endsection

@section('script')

    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>

@endsection
