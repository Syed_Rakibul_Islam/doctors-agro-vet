@extends('layouts.app')

@section('pageTitle', 'Sign Up')

@section('style')
    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-50"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form id="login-form" class="login-form" method="post" action="{{ url('profile/edit') }}" enctype="multipart/form-data">
                                <h1 class="text-center">{{ __('navigation.my_profile') }} | {{ __('navigation.edit') }}</h1>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                            <input type="text" name="name" class="form-control" value="{{ !empty(old('name')) ? old('name') : $user->name }}" placeholder="{{ __('auth.name') }} *" required />
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{ $user->username }}" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{ $user->email }}" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
                                            <input type="text" name="contact" class="form-control" value="{{ !empty(old('contact')) ? old('contact') : $user->contact }}" placeholder="{{ __('auth.contact_no') }} " />
                                            @if ($errors->has('contact'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contact') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('user_image') ? ' has-error' : '' }}">
                                            <input type="file" name="user_image" class="form-control dropify" data-default-file="{{ !empty($user->image) && file_exists('images/upload/user/' . $user->image) ? url('images/upload/user/' . $user->image) : url('images/user.png') }}" data-max-file-size="300K" />
                                            @if ($errors->has('user_image'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('user_image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <p class="text-info text-danger">NB: Image size must be less than 300KB.</p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                            <textarea rows="2" name="address" class="form-control"  placeholder="{{ __('auth.address') }}">{{ !empty(old('address')) ? old('address') : $user->address }}</textarea>
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('about') ? ' has-error' : '' }}">
                                            <textarea rows="2" name="about" class="form-control" placeholder="{{ __('auth.about') }}">{{ !empty(old('about')) ? old('about') : $user->about }}</textarea>
                                            @if ($errors->has('about'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('about') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" value="{{ __('auth.submit') }}" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-50"></div>
        </div><!-- Contact Forms Section /- -->
    </main>

@endsection

@section('script')

    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>

@endsection
