@extends('layouts.app')

@section('pageTitle', 'Sign In')

@section('content')

    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-50"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="col-md-12">
                                <div class="testimonial-content text-center">
                                    <h3>{{ Auth::user()->name }}</h3>
                                    <h3>{{ Auth::user()->username }}</h3>
                                    <h3>{{ Auth::user()->email }}</h3>
                                    <p>{{ Auth::user()->about }}</p>
                                    <img class="img-circle" src="{{ !empty(Auth::user()->image) && file_exists('images/upload/user/' . Auth::user()->image) ? url('images/upload/user/' . Auth::user()->image) : url('images/user.png') }}" alt="{{ Auth::user()->name }}" width="300" height="300" />
                                    @if(!empty(Auth::user()->contact))
                                        <h2 class="text-justify">{{ __('auth.contact_no') }}: {{ Auth::user()->contact }}</h2>
                                    @endif
                                    @if(!empty(Auth::user()->address))
                                        <h2 class="text-justify">{{ __('auth.address') }}: {{ Auth::user()->address }}</h2>
                                    @endif
                                    <div class="clearfix"></div>
                                    <a class="btn btn-yellow pull-right" href="{{ url('profile/edit') }}">{{ __('navigation.edit') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-50"></div>
        </div><!-- Contact Forms Section /- -->
    </main>

@endsection
