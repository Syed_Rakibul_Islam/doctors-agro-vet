<html>
<head>
    <link rel="icon" href="{{ asset('favicons/favicon-16x16.png') }}" type="image/png" sizes="16x16" />
</head>
<body>
    <h1 style="text-align: center">{{ config('app.name') }} | Admin | Contact</h1>
    <h4>User Name: {{ $name }}</h4>
    <h4>User Email: {{ $email }}</h4>
    <h4>Mail Subject: {{ $subject }}</h4>
    <h4>Mail Message:</h4>
    <h6>{!! nl2br($description) !!}</h6>
</body>
</html>
