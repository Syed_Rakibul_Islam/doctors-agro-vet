<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class=""><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }} | @yield('pageTitle')</title>

    {!! SEO::generate() !!}



    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images//apple-touch-icon-114x114-precomposed.png') }}">

    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images//apple-touch-icon-72x72-precomposed.png') }}">

    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images//apple-touch-icon-57x57-precomposed.png') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/lightslider/lightslider.min.css') }}" />


    <!-- Custom - Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('styles.css') }}" />

    <!-- Page-Style -->
    @yield('style')
    <!-- /Page-Style -->

    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5/respond.min.js') }}"></script>
    <![endif]-->

</head>
<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">

    <!-- Alert Section -->
    @include('common.alert')
    <!-- Alert Section /- -->

    <!-- Header Section -->
    @include('common.header')
    <!-- Header Section /- -->


    {{-- MAIN CONTENT--}}
    @yield('content')
    {{-- END MAIN CONTENT--}}



    <!-- Footer -->
    @include('common.footer')
    <!-- Footer /- -->


    <script>
        var baseURL = "{{ url('') }}";
        var assetURL = "{{ asset('') }}";
        var appLocale = "{{ config('app.locale') }}";
    </script>
    <!-- JQuery v1.11.3 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Library JS -->
    <script src="{{ asset('libraries/lib.js') }}"></script>
    <script src="{{ asset('libraries/lightslider/lightslider.min.js') }}"></script>


    <!-- Library - Google Map API -->
    <script src="{{ asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyBcpfk0wyr-B82_-kTAugxyjOgLUGHHmkw') }}"></script>

    <!-- Library - Theme JS -->
    <script src="{{ asset('js/functions.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-XXXXX-Y', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <!-- Page-Style -->
    @yield('script')
    <!-- /Page-Style -->

</body>
</html>