@extends('layouts.app')

@section('pageTitle', __('navigation.partners'))

@section('content')
    <main class="site-main page-spacing">

        <!-- Interractive Banner -->
        <div class="interractive-banner container-fluid no-padding">
            <div class="container">
                <!-- Breadcrumb Area -->
                <div class="page-breadcrumb container-fluid no-padding">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li class="active">{{ __('navigation.partners') }}</li>
                        </ol>
                    </div>
                </div>
                <!-- Breadcrumb Area /- -->
            </div>
            <div class="padding-50"></div>
            <!-- Container -->
            <div class="container">
                <div class="row">
                    @forelse($partners as $partner)
                    @php
                        $partnerName = config('app.locale') == "bn" && !empty($partner->name_b) ? $partner->name_b : $partner->name;
                    @endphp
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <a href="{{ url('partners/' . $partner->id) }}" alt="{{ $partnerName }}">
                            <div class="interractive-box">
                                <div class="interractive-content">
                                    <img src="{{ !empty($partner->image) && file_exists('images/upload/partner/' . $partner->image) ? url('images/upload/partner/' . $partner->image) : url('images/partner.png') }}" alt="{{ $partnerName }}">
                                    <h3>{{ $partnerName }}</h3>
                                </div>
                                <div class="interractive-back-content">
                                    <p class="text-justify">{{ \Illuminate\Support\Str::words($partner->about, 40,'....') }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @empty
                        <h1>{{ __('msg.not_found', ['name' => __('navigation.partner')]) }}</h1>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="padding-50"></div>
    </main>
@endsection