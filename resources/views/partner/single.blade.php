@extends('layouts.app')

@php
    $partnerName = config('app.locale') == "bn" && !empty($partner->name_b) ? $partner->name_b : $partner->name;
@endphp

@section('pageTitle', __('navigation.partners') . ' | ' . $partnerName)

@section('content')
    <main>
        <div class="container blog blogpost blogpost2">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('partners') }}">{{ __('navigation.partners') }}</a></li>
                        <li class="active">{{ $partnerName }}</li>
                    </ol>
                </div>
            </div>
            <div class="padding-50"></div>
            <!-- Breadcrumb Area /- -->
            <div class="row">
                <!-- Content Area -->
                <div class="col-md-12 content-area">
                    <article class="type-post image-post">
                        <div class="entry-cover text-center">
                            <a href="#" title="Cover"><img src="{{ !empty($partner->image) && file_exists('images/upload/partner/' . $partner->image) ? url('images/upload/partner/' . $partner->image) : url('images/partner.png') }}" style="width: 250px; height: 70px" /></a>
                        </div>
                        <div class="entrycontent-block">
                            <div class="entry-title">
                                <h3 class="text-center">{{ $partnerName }}</h3>
                                <h3 class="text-center">{{ isset($partner->country->name) ? $partner->country->name : '' }}</h3>
                                <h3 class="text-center">{{  $partner->contact }}</h3>
                                <h4 class="text-center">{{  $partner->address }}</h4>
                                <h4 class="text-center"><a href="{{ $partner->url }}" target="_blank">{{  $partner->url }}</a></h4>
                            </div>
                            <div class="entry-content">
                                {{ $partner->about }}
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <div class="padding-100"></div>
    </main>
@endsection