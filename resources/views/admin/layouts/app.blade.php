<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{ config('app.name') }} | Admin | @yield('pageTitle') </title>
    <meta name="description" content="{{ config('app.name') }} software by Syed Rakibul Islam Romeo."  />
    <meta name="keywords" content="Doctors agro-vet, manager, {{ config('app.name') }}, web app, application"  />
    <meta name="author" content="Syed Rakibul Islam Romeo"/>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">



    <!-- Page-Style -->
    @yield('style')
    <!-- /Page-Style -->

    <!-- Custom CSS -->
    <link href="{{ asset('admin-resources/dist/css/style.css') }}" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    @include('admin.common.header')
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    @include('admin.common.left-sidebar')
    <!-- /Left Sidebar Menu -->

    <!-- Main Content -->
    <div class="page-wrapper">

        <div class="container-fluid {{ Request::url() == url('/') ? 'pt-25' : '' }} ">

            @if( Request::url() != url('/') )
                <!-- Breadcrumbs -->
                @include('admin.common.breadcrumbs')
                <!-- /Breadcrumbs -->
            @endif

            <!-- Breadcrumbs -->
            @include('admin.common.alert')
            <!-- /Breadcrumbs -->

            <!-- Content -->
            @yield('content')
            <!-- /Content -->

        </div>

        <!-- Footer -->
        @include('admin.common.footer')
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{ asset('admin-resources/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('admin-resources/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ asset('admin-resources/dist/js/dropdown-bootstrap-extended.js') }}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ asset('admin-resources/dist/js/jquery.slimscroll.js') }}"></script>

<!-- Owl JavaScript -->
<script src="{{ asset('admin-resources/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js') }}"></script>

<!-- Switchery JavaScript -->
<script src="{{ asset('admin-resources/vendors/bower_components/switchery/dist/switchery.min.js') }}"></script>

<!-- Page-Style -->
@yield('script')
<!-- /Page-Style -->
<!-- Init JavaScript -->
<script src="{{ asset('admin-resources/dist/js/init.js') }}"></script>

</body>

</html>
