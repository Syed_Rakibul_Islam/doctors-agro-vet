<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'E-mail', ['class' => 'control-label mb-10']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email address', 'maxlength' => '100']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
    {!! Form::label('contact', 'Contact', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Enter contact number', 'maxlength' => '100']) !!}
    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('client_image') ? 'has-error' : ''}}">
    {!! Form::label('client_image', 'Client image', ['class' => 'control-label mb-10']) !!}
    @if(isset($client) && !empty($client->image))
        {!! Form::file('client_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/client/' . $client->image), 'data-min-width' =>'280', 'data-min-height' =>'280', 'data-max-width' =>'305', 'data-max-height' =>'305']) !!}
    @else
        {!! Form::file('client_image', ['class' => 'form-control dropify', 'data-min-width' =>'280', 'data-min-height' =>'280', 'data-max-width' =>'305', 'data-max-height' =>'305']) !!}
    @endif
    {!! $errors->first('client_image', '<p class="help-block">:message</p>') !!}
</div>
<p class="text-info">NB: Image dimension must be 280x280 to 300x300.</p>
<div class="form-group">
    {!! Form::label('address', 'Address', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div id="google-map" style="width:100%; height: 400px"></div>
{!! Form::hidden('location', null  ) !!}
{!! Form::hidden('latitude', null  ) !!}
{!! Form::hidden('longitude', null  ) !!}
<div class="form-group {{ $errors->has('district_id') ? 'has-error' : ''}}">
    {!! Form::label('district_id', 'District', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('district_id', $districts, null, ['class' => 'form-control', 'id' => 'district_id', 'data-style' => 'form-control btn-default btn-outline', 'placeholder' => 'Select district']) !!}
    {!! $errors->first('district_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
    {!! Form::label('about', 'About', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('about', null, ['class' => 'form-control', 'placeholder' => 'Enter about client ...', 'maxlength' => '100000']) !!}
    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
</div>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>
<div class="form-group {{ $errors->has('name_b') ? 'has-error' : ''}}">
    {!! Form::label('name_b', 'বাংলায় নাম', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় নাম লিখুন', 'maxlength' => '100']) !!}
    {!! $errors->first('name_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
