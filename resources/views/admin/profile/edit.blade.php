@extends('admin.layouts.app')

@section('pageTitle', ' My Profile | Edit' )

@section('style')

    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {!! Form::model($user, ['method' => 'POST', 'url' => ['/admin/profile/edit'], 'data-toggle' => 'validator', 'files'=>true]) !!}

                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                            {!! Form::label('name', 'Name *', ['class' => 'control-label mb-10']) !!}
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name', 'maxlength' => '100', 'required' => 'required']) !!}
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                            {!! Form::label('contact', 'Contact No', ['class' => 'control-label mb-10']) !!}
                                            {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Enter contact no', 'maxlength' => '100']) !!}
                                            {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('user_image') ? 'has-error' : ''}}">
                                            {!! Form::label('user_image', 'Profile image', ['class' => 'control-label mb-10']) !!}
                                            @if(isset($user) && !empty($user->image) && file_exists('images/upload/user/' . $user->image))
                                                {!! Form::file('user_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/user/' . $user->image), 'data-max-file-size' => '300K']) !!}
                                            @else
                                                {!! Form::file('user_image', ['class' => 'form-control dropify', 'data-max-file-size' => '300K' ]) !!}
                                            @endif
                                            {!! $errors->first('user_image', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <p class="text-info">NB: Image size must be less than 300KB.</p>
                                        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                            {!! Form::label('address', 'Address', ['class' => 'control-label mb-10']) !!}
                                            {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter address', 'maxlength' => '255']) !!}
                                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                                            {!! Form::label('about', 'About me', ['class' => 'control-label mb-10']) !!}
                                            {!! Form::textarea('about', null, ['class' => 'form-control textarea_editor', 'maxlength' => '100000', 'placeholder' => 'Enter about me']) !!}
                                            {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <hr>
                                        <h4 class="text-center">বাংলা সংস্করণ</h4>

                                        <div class="form-group {{ $errors->has('name_b') ? 'has-error' : ''}}">
                                            {!! Form::label('name_b', 'বাংলায় শিরোনাম', ['class' => 'control-label mb-10']) !!}
                                            {!! Form::text('name_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় নাম', 'maxlength' => '100']) !!}
                                            {!! $errors->first('name_b', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
                                        </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";


            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify');
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });

        });
    </script>
@endsection