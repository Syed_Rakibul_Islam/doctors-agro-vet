@extends('admin.layouts.app')

@section('pageTitle', 'My Profile' )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('admin/profile') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        <a href="{{ url('admin/profile/edit') }}" class="btn btn-primary pa-5" title="Edit Profile"><i class="zmdi zmdi-edit txt-light"></i></a>
                        <a href="{{ url('admin/profile/change-password') }}" class="btn btn-danger pa-5" title="Change Password"><i class="zmdi zmdi-key txt-light"></i></a>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ Auth::user()->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name </th>
                                        <td> {{ Auth::user()->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Username </th>
                                        <td> {{ Auth::user()->username }} </td>
                                    </tr>
                                    <tr>
                                        <th> Email </th>
                                        <td> {!! Auth::user()->email !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Contact No </th>
                                        <td> {!! Auth::user()->contact !!} </td>
                                    </tr>
                                    @if(!empty(Auth::user()->image) && file_exists('images/upload/user/' . Auth::user()->image))
                                        <tr>
                                            <th> Feature image </th>
                                            <td> <img src="{{ asset('images/upload/user/' . Auth::user()->image) }}" title="{{ Auth::user()->name }}"> </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th> Address </th>
                                        <td> {!! Auth::user()->address !!} </td>
                                    </tr>
                                    <tr>
                                        <th> About </th>
                                        <td> {!! Auth::user()->about !!} </td>
                                    </tr>
                                    <tr>
                                        <th> বাংলায় নাম </th>
                                        <td> {{ Auth::user()->name_b }} </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
