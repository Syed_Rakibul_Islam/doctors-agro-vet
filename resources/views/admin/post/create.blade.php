@extends('admin.layouts.app')

@section('pageTitle', 'News | Create' )

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- select2 CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Wysihtml5 css -->
    <link rel="stylesheet" href="{{ asset('admin-resources/vendors/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css') }}" />

    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {!! Form::open(['url' => 'admin/news', 'data-toggle' => 'validator', 'files'=>true]) !!}

                                    @include ('admin.post.form')

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/wysihtml5x/dist/wysihtml5x.min.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            //editor
            $('.textarea_editor').wysihtml5({
                toolbar: {
                    fa: true,
                    "link": true
                }
            });

            $('#category').select2({
                placeholder: "Choose post categories",
                allowClear: true,
                multiple:true
            });

            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });




            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("input[name=title]").change(function(){
                $.ajax({
                    type: 'GET',
                    url: baseUrl + '/admin/ajax/check-post-name',
                    data: {name:this.value},
                    success: function (result) {
                        $('#post_name').html('<br/> Post name : <button type="button" class="btn-link" onclick="editPostName(\'' + result + '\')"> ' + result + '</button> <button type="button" class="btn btn-default btn-xs" onclick="editPostName(\'' + result + '\')"> Edit </button>');
                        $('input[name=name]').val(result);
                    }
                });
            });



        });
        function editPostName(v) {
            $('#post_name').html('<br/> <input type="text" name="edit-post-name" id="edit-post-name" value="' + v + '"> <button type="button" onclick="editCheckPost()">Ok</button>');
        }
        function editCheckPost(){
            var editPostName = $('#edit-post-name').val();
            $.ajax({
                type: 'GET',
                url: baseUrl + '/admin/ajax/check-post-name',
                data: {name:editPostName},
                success: function (result) {
                    $('#post_name').html('<br/> Post name : <button type="button" class="btn-link" onclick="editPostName(\'' + result + '\')"> ' + result + '</button> <button type="button" class="btn btn-default btn-xs" onclick="editPostName(\'' + result + '\')"> Edit </button>');
                }
            });
        }
    </script>
@endsection