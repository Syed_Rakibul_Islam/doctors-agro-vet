{!! Form::hidden('name', (isset($post->name) && !empty($post->name)) ? $post->name : null  ) !!}
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter post title', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    <span id="post_name">
        @if(isset($post->name) && !empty($post->name))
            <br/> Post name : <button type="button" class="btn-link" onclick="editPostName('{{ $post->name }}')"> {{ $post->name }}</button> <button type="button" class="btn btn-default btn-xs" onclick="editPostName('{{ $post->name }}')"> Edit </button>
        @endif
    </span>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control textarea_editor', 'maxlength' => '100000', 'placeholder' => 'Enter post description...']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
    {!! Form::label('feature_image', 'Feature image', ['class' => 'control-label mb-10']) !!}
    @if(isset($post) && !empty($post->image))
        {!! Form::file('feature_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/news/' . $post->image), 'data-max-file-size' => '1.5M']) !!}
    @else
        {!! Form::file('feature_image', ['class' => 'form-control dropify', 'data-max-file-size' => '1.5M']) !!}
    @endif
    {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
</div>
<p class="text-info">NB: Image size must be less than 1.5MB.</p>
<div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
    {!! Form::label('category', 'Post Category', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('category[]', $categories, (isset($post->categories) && !empty($post->categories)) ? $post->categories : null, ['class' => 'form-control selectpicker', 'id' => 'category', 'multiple', 'data-style' => 'form-control btn-default btn-outline']) !!}
    {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
</div>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>

<div class="form-group {{ $errors->has('title_b') ? 'has-error' : ''}}">
    {!! Form::label('title_b', 'বাংলায় শিরোনাম', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('title_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় শিরোনাম লিখুন', 'maxlength' => '100']) !!}
    {!! $errors->first('title_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_b') ? 'has-error' : ''}}">
    {!! Form::label('description_b', 'বাংলায় বিবরণ', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description_b', null, ['class' => 'form-control textarea_editor', 'placeholder' => 'বাংলায় বিবরণী লিখুন ...']) !!}
    {!! $errors->first('description_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
