@extends('admin.layouts.app')

@section('pageTitle', 'News | Comment | ' . $comment->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('admin/news/' . $postId . '/comment' ) }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        @if(empty($comment->approve))
                            {!! Form::open([
                                'method'=>'PATCH',
                                'url' => ['admin/news/' . $postId . '/comment', $comment->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="zmdi zmdi-check txt-light"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-success pa-5',
                                    'title' => 'Approve Comment',
                                    'onclick'=>'return confirm("Confirm Approve?")'
                            )) !!}
                            {!! Form::hidden('status', 'approve'  ) !!}
                            {!! Form::close() !!}
                        @endif
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/news/' . $postId . '/comment', $comment->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger pa-5',
                                'title' => 'Delete Comment',
                                'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>Comment ID</th>
                                        <td>{{ $comment->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Written by</th>
                                        @if(!empty($comment->user_id))
                                            @if(isset($comment->user->id))
                                                <td><a href="{{ url('admin/user/' . $comment->user->id) }}">{{ $comment->user->name }}</a></td>
                                            @else
                                                <td class="text-danger">Deleted</td>
                                            @endif
                                        @else
                                            <td class="text-primary">{{ $comment->user_name }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Comment</th>
                                        <td>{{ $comment->description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <td>{{ \Carbon\Carbon::parse($comment->created_at)->timezone('Asia/Dhaka')->format('F d, Y h:i A') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
