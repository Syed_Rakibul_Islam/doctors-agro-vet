<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter category name', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => '100000', 'placeholder' => 'Enter category description']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>

<div class="form-group {{ $errors->has('name_b') ? 'has-error' : ''}}">
    {!! Form::label('name_b', 'বাংলায় নাম', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় নাম লিখুন', 'maxlength' => '100']) !!}
    {!! $errors->first('name_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_b') ? 'has-error' : ''}}">
    {!! Form::label('description_b', 'বাংলায় বিবরণ', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় বিবরণী লিখুন ...', 'maxlength' => '100000']) !!}
    {!! $errors->first('description_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
