<div class="form-group {{ $errors->has('post_id') ? 'has-error' : ''}}">
    {!! Form::label('post_id', 'Post Name', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('post_id', $posts, null, ['class' => 'form-control', 'id' => 'post_id', 'required' => 'required']) !!}
    {!! $errors->first('post_id', '<p class="help-block">:message</p>') !!}
    <span id="post_name"></span>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => '100000', 'placeholder' => 'Enter comment', 'required' => 'required']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
