@extends('admin.layouts.app')

@section('pageTitle', 'News | Comment')

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a href="{{ url('admin/news/comment/create') }}" class="btn btn-success pull-right pa-5" title="Add New Comment"><i class="zmdi zmdi-plus txt-light"></i></a>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15" title="Full screen">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                        <div class="pull-left inline-block dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button" title="Column visible / hide"><i class="zmdi zmdi-eye"></i> / <i class="zmdi zmdi-eye-off text-danger"></i></a>
                            <ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
                                <li class="show-hide-column" data-columnindex="0">S.No</li>
                                <li class="show-hide-column" data-columnindex="1">ID</li>
                                <li class="show-hide-column" data-columnindex="2">Post Name</li>
                                <li class="show-hide-column" data-columnindex="3">Editor Name</li>
                                <li class="show-hide-column" data-columnindex="4" style="color: rgb(234, 108, 65);">Description</li>
                                <li class="show-hide-column" data-columnindex="5">Status</li>
                                <li class="show-hide-column" data-columnindex="6">Date</li>
                                <li class="show-hide-column" data-columnindex="7">Actions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>ID</th>
                                        <th>Post Name</th>
                                        <th>Editor Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>S.No</th>
                                        <th>ID</th>
                                        <th>Post Name</th>
                                        <th>Editor Name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($comments as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->id }}</td>
                                            <td>
                                                @if(isset($item->post->name))
                                                    <a href="{{ url('admin/news/' . $item->post->id) }}"> {{ $item->post->name }} </a>
                                                @else
                                                    <span class="text-danger"> Deleted </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($item->user_id))
                                                    @if(isset($item->user->id))
                                                        <a href="{{ url('admin/user/' . $item->user->id) }}">{{ $item->user->name }}</a>
                                                    @else
                                                        <span class="text-danger">Deleted</span>
                                                    @endif
                                                @else
                                                    <span class="text-primary">{{ $item->user_name }}</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->description }}</td>
                                            @if(empty($item->approve))
                                                <td class="text-warning">Pending</td>
                                            @else
                                                <td class="text-success">Approved</td>
                                            @endif
                                            <td>{{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}</td>
                                            <td>
                                                <a href="{{ url('admin/news/comment/' . $item->id) }}" class="btn btn-info pa-5" title="View Comment"><i class="zmdi zmdi-eye txt-light"></i></a>
                                                @if(empty($item->approve))
                                                {!! Form::open([
                                                    'method'=>'PATCH',
                                                    'url' => ['admin/news/comment', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<i class="zmdi zmdi-check txt-light"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-success pa-5',
                                                        'title' => 'Approve Comment',
                                                        'onclick'=>'return confirm("Confirm Approve?")'
                                                )) !!}
                                                {!! Form::hidden('status', 'approve'  ) !!}
                                                {!! Form::close() !!}
                                                @endif
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['admin/news/comment', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger pa-5',
                                                        'title' => 'Delete Comment',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>

    <script>
        /*Export Table Init*/
        $(document).ready(function() {
            var exportDataTable = $('#data-table').DataTable( {
                responsive: true,
                dom: 'lBfrtip',
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "aoColumns": [null, null, null, null, null, null, { "sType": 'date' }, { "bSortable": false }],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'copyButton',
                        key: {
                            key: 'c',
                            altKey: true
                        },
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'print',
                        key: {
                            key: 'p',
                            altKey: true
                        },
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    }
                ]
            } );

            exportDataTable.columns( [4] ).visible( false, false );

            $('.show-hide-column').on('click', function () {
                var tableColumn = exportDataTable.column($(this).attr('data-columnindex'));
                tableColumn.visible(!tableColumn.visible());
                if(!tableColumn.visible()){
                    $(this).css('color', '#ea6c41 ');
                }
                else{
                    $(this).css('color', '#333');
                }

            });
        } );
    </script>
@endsection
