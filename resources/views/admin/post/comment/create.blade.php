@extends('admin.layouts.app')

@section('pageTitle', 'News | Comment | Create' )

@section('style')

    <!-- select2 CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>


@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {!! Form::open(['url' => 'admin/news/comment', 'data-toggle' => 'validator']) !!}

                                    @include ('admin.post.comment.form')

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection


@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function() {
            $('#post_id').select2({});
        });
    </script>
@endsection