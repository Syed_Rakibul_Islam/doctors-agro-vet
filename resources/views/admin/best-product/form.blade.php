<div class="form-group {{ $errors->has('product_id') ? 'has-error' : ''}}">
    {!! Form::label('product_id', 'District', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('product_id', $products, null, ['class' => 'form-control', 'id' => 'product_id', 'placeholder' => 'Select Product', 'data-style' => 'form-control btn-default btn-outline']) !!}
    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description', 'maxlength' => '10000']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>
<div class="form-group {{ $errors->has('description_b') ? 'has-error' : ''}}">
    {!! Form::label('description_b', 'বাংলায় বিবরণ', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('description_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় বিবরণ লিখুন', 'maxlength' => '10000']) !!}
    {!! $errors->first('description_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
