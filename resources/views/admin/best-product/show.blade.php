@extends('admin.layouts.app')

@section('pageTitle', ' Best Product | View | ' . $bestProduct->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('/admin/best-product') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        <a href="{{ url('/admin/best-product/' . $bestProduct->id . '/edit') }}" class="btn btn-primary pa-5" title="Edit Best Product"><i class="zmdi zmdi-edit txt-light"></i></a>
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $bestProduct->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Product Name </th>
                                            <td> {{ isset($bestProduct->product->name) ? $bestProduct->product->name : '' }} </td>
                                        </tr>
                                        <tr>
                                            <th> Product Title </th>
                                            <td> {{ isset($bestProduct->product->title) ? $bestProduct->product->title : '' }} </td>
                                        </tr>
                                        <tr>
                                            <th> Product image </th>
                                            <td> <img src="{{ !empty($bestProduct->product->image) && file_exists('images/upload/product/' . $bestProduct->product->image) ? asset('images/upload/product/' . $bestProduct->product->image) : asset('images/product.png') }}" title="{{ isset($bestProduct->product->title) ? $bestProduct->product->title : '' }}"> </td>
                                        </tr>
                                        <tr>
                                            <th> Description </th>
                                            <td> {{ $bestProduct->description }} </td>
                                        </tr>
                                        <tr>
                                            <th> বাংলা বিবরণ </th>
                                            <td> {{ $bestProduct->description_b }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#data-table').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
