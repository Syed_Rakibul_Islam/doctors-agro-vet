@extends('admin.layouts.app')

@section('pageTitle', 'Best Product')

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15" title="Full screen">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                        <div class="pull-left inline-block dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button" title="Column visible / hide"><i class="zmdi zmdi-eye"></i> / <i class="zmdi zmdi-eye-off text-danger"></i></a>
                            <ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
                                <li class="show-hide-column" data-columnindex="0">S.No</li>
                                <li class="show-hide-column" data-columnindex="1">ID</li>
                                <li class="show-hide-column" data-columnindex="2">Name</li>
                                <li class="show-hide-column" data-columnindex="3">Title</li>
                                <li class="show-hide-column" data-columnindex="4">Description</li>
                                <li class="show-hide-column" data-columnindex="5">Actions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>S.No</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($bestProducts as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ isset($item->product->name) ? $item->product->name : '' }}</td>
                                            <td>{{ isset($item->product->title) ? $item->product->title : '' }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>
                                                <a href="{{ url('admin/best-product/' . $item->id) }}" class="btn btn-info pa-5" title="View Best Product"><i class="zmdi zmdi-eye txt-light"></i></a>
                                                <a href="{{ url('admin/best-product/' . $item->id . '/edit') }}" class="btn btn-primary pa-5" title="Edit Best Product"><i class="zmdi zmdi-edit txt-light"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>

    <script>
        /*Export Table Init*/
        $(document).ready(function() {
            var exportDataTable = $('#data-table').DataTable( {
                responsive: true,
                dom: 'lBfrtip',
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "aoColumns": [{ "sType": 'numeric' }, { "sType": 'numeric' }, null, null, null, { "bSortable": false }],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'copyButton',
                        key: {
                            key: 'c',
                            altKey: true
                        },
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'print',
                        key: {
                            key: 'p',
                            altKey: true
                        },
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    }
                ]
            } );

            exportDataTable.columns( [] ).visible( false, false );

            $('.show-hide-column').on('click', function () {
                var tableColumn = exportDataTable.column($(this).attr('data-columnindex'));
                tableColumn.visible(!tableColumn.visible());
                if(!tableColumn.visible()){
                    $(this).css('color', '#ea6c41 ');
                }
                else{
                    $(this).css('color', '#333');
                }

            });
        } );
    </script>
@endsection
