@extends('admin.layouts.app')

@section('pageTitle', 'Retrieve | Partner | View | ' . $partner->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('/admin/retrieve/partner') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        {!! Form::open([
                            'method'=>'PUT',
                            'url' => ['admin/retrieve/partner', $partner->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="zmdi zmdi-undo txt-light"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-success pa-5',
                                    'title' => 'Restore Partner',
                                    'onclick'=>'return confirm("Confirm restore?")'
                            )) !!}
                        {!! Form::close() !!}
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/retrieve/partner', $partner->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger pa-5',
                                    'title' => 'Permanently Delete Partner',
                                    'onclick'=>'return confirm("Confirm permanent delete?")'
                            )) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $partner->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Name </th>
                                            <td> {{ $partner->name }} </td>
                                        </tr>
                                        <tr>
                                            <th> Email </th>
                                            <td> {{ $partner->email }} </td>
                                        </tr>
                                        <tr>
                                            <th> Contact Number </th>
                                            <td> {{ $partner->contact }} </td>
                                        </tr>
                                        @if(!empty($partner->image))
                                            <tr>
                                                <th> Partner image </th>
                                                <td> <img src="{{ asset('images/upload/partner/' . $partner->image) }}" title="{{ $partner->name }}"> </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th> Address </th>
                                            <td> {{ $partner->address }} </td>
                                        </tr>
                                        <tr>
                                            <th> About </th>
                                            <td> {{ $partner->about }} </td>
                                        </tr>
                                        <tr>
                                            <th> বাংলা নাম </th>
                                            <td> {{ $partner->name_b }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#data-table').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
