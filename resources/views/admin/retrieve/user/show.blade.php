@extends('admin.layouts.app')

@section('pageTitle', 'Retrieve | User | View | ' . $user->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('/admin/retrieve/user') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        {!! Form::open([
                            'method'=>'PUT',
                            'url' => ['admin/retrieve/user', $user->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-undo txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-success pa-5',
                                'title' => 'Restore User',
                                'onclick'=>'return confirm("Confirm restore?")'
                        )) !!}
                        {!! Form::close() !!}
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/retrieve/user', $user->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger pa-5',
                                    'title' => 'Permanently Delete User',
                                    'onclick'=>'return confirm("Confirm permanent delete?")'
                            )) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $user->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Name </th>
                                            <td> {{ $user->name }} </td>
                                        </tr>
                                        <tr>
                                            <th> Username </th>
                                            <td> {{ $user->username }} </td>
                                        </tr>
                                        <tr>
                                            <th> Email </th>
                                            <td> {{ $user->email }} </td>
                                        </tr>
                                        <tr>
                                            <th> Contact Number </th>
                                            <td> {{ $user->contact }} </td>
                                        </tr>
                                        <tr>
                                            <th> Role </th>
                                            <td> {{ strtoupper($user->role) }} </td>
                                        </tr>
                                        @if(!empty($user->image))
                                            <tr>
                                                <th> User image </th>
                                                <td> <img src="{{ asset('images/upload/user/' . $user->image) }}" title="{{ $user->username }}"> </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th> Address </th>
                                            <td> {{ $user->address }} </td>
                                        </tr>
                                        <tr>
                                            <th> About </th>
                                            <td> {{ $user->about }} </td>
                                        </tr>
                                        <tr>
                                            <th> বাংলা নাম </th>
                                            <td> {{ $user->name_b }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#data-table').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
