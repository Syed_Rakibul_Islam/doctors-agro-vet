@extends('admin.layouts.app')

@section('pageTitle', 'Retrieve | News')

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15" title="Full screen">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                        <div class="pull-left inline-block dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button" title="Column visible / hide"><i class="zmdi zmdi-eye"></i> / <i class="zmdi zmdi-eye-off text-danger"></i></a>
                            <ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
                                <li class="show-hide-column" data-columnindex="0">S.No</li>
                                <li class="show-hide-column" data-columnindex="1">ID</li>
                                <li class="show-hide-column" data-columnindex="2">Name</li>
                                <li class="show-hide-column" data-columnindex="3">Title</li>
                                <li class="show-hide-column" style="color: rgb(234, 108, 65);" data-columnindex="4">Author</li>
                                <li class="show-hide-column" data-columnindex="5">Category</li>
                                <li class="show-hide-column" data-columnindex="6">Status</li>
                                <li class="show-hide-column" data-columnindex="7">Date</li>
                                <li class="show-hide-column" data-columnindex="8">Actions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>S.No</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($posts as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>
                                                @if(isset($item->author->id))
                                                    <a href="{{ url('admin/user/' . $item->author->id) }}" target="_blank">{{ $item->author->name }}</a>
                                                @else
                                                    <a href="#" class="text-danger">Deleted</a>
                                                @endif
                                            </td>
                                            <td>
                                                @foreach($item->categories as $category)
                                                    <a href="{{ url('admin/news/category/' . $category->id) }}" target="_blank">{{ $category->name }}</a>
                                                @endforeach
                                            </td>
                                            <td>{{ $item->status }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}</td>
                                            <td>
                                                <a href="{{ url('admin/retrieve/news/' . $item->id) }}" class="btn btn-info pa-5" title="View News"><i class="zmdi zmdi-eye txt-light"></i></a>
                                                {!! Form::open([
                                                    'method'=>'PUT',
                                                    'url' => ['admin/retrieve/news', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="zmdi zmdi-undo txt-light"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-success pa-5',
                                                            'title' => 'Restore News',
                                                            'onclick'=>'return confirm("Confirm restore?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['admin/retrieve/news', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger pa-5',
                                                            'title' => 'Permanently Delete News',
                                                            'onclick'=>'return confirm("Confirm permanent delete? If you delete this news, it will delete those comments that belongs to this news.")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>

    <script>
        /*Export Table Init*/
        $(document).ready(function() {
            var exportDataTable = $('#data-table').DataTable( {
                responsive: true,
                dom: 'lBfrtip',
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "aoColumns": [null, null, null, null, null, null, null, { "sType": 'date' }, { "bSortable": false }],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'copyButton',
                        key: {
                            key: 'c',
                            altKey: true
                        },
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },

                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'print',
                        key: {
                            key: 'p',
                            altKey: true
                        },
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                page: 'current'
                            }
                        }
                    }
                ]
            } );

            exportDataTable.columns( [ 4 ] ).visible( false, false );

            $('.show-hide-column').on('click', function () {
                var tableColumn = exportDataTable.column($(this).attr('data-columnindex'));
                tableColumn.visible(!tableColumn.visible());
                if(!tableColumn.visible()){
                    $(this).css('color', '#ea6c41 ');
                }
                else{
                    $(this).css('color', '#333');
                }

            });
        } );
    </script>
@endsection
