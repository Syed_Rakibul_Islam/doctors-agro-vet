@extends('admin.layouts.app')

@section('pageTitle', 'News | Comment | View | ' . $comment->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('/admin/retrieve/news/comment') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        {!! Form::open([
                            'method'=>'PUT',
                            'url' => ['admin/retrieve/news/comment', $comment->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-undo txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-success pa-5',
                                'title' => 'Restore Comment',
                                'onclick'=>'return confirm("Confirm restore?")'
                        )) !!}
                        {!! Form::close() !!}
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/retrieve/news/comment', $comment->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger pa-5',
                                'title' => 'Permanent Delete Comment',
                                'onclick'=>'return confirm("Confirm permanent delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $comment->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Post Name </th>
                                            <td>
                                                @if(isset($comment->post->name))
                                                    <a href="{{ url('admin/news/' . $comment->post->id) }}">{{ $comment->post->name }}</a>
                                                @else
                                                    <span class="text-danger">Deleted</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th> User Name </th>
                                            <td>
                                                @if(!empty($comment->user_id))
                                                    @if(isset($comment->user->id))
                                                        <a href="{{ url('admin/user/' . $comment->user->id) }}">{{ $comment->user->name }}</a>
                                                    @else
                                                        <span class="text-danger">Deleted</span>
                                                    @endif
                                                @else
                                                    <span class="text-primary">{{ $comment->user_name }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th> Description </th>
                                            <td> {{ $comment->description }} </td>
                                        </tr>
                                        <tr>
                                            <th> Status </th>
                                            @if(empty($comment->approve))
                                                <td class="text-warning">Pending</td>
                                            @else
                                                <td class="text-success">Approved</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th> Date </th>
                                            <td> {{ Carbon\Carbon::parse($comment->created_at)->format('d-m-Y') }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#data-table').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
