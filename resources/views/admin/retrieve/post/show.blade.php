@extends('admin.layouts.app')

@section('pageTitle', 'Retrieve | News | View | ' . $post->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('admin/retrieve/news') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        {!! Form::open([
                            'method'=>'PUT',
                            'url' => ['admin/retrieve/news', $post->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-undo txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-success pa-5',
                                'title' => 'Restore News',
                                'onclick'=>'return confirm("Confirm restore?")'
                        )) !!}
                        {!! Form::close() !!}
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/retrieve/news', $post->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger pa-5',
                                'title' => 'Permanently Delete News',
                                'onclick'=>'return confirm("Confirm permanent delete? If you delete this news, it will delete those comments that belongs to this news.")'
                        )) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $post->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Name </th>
                                            <td> {{ $post->name }} </td>
                                        </tr>
                                        <tr>
                                            <th> Title </th>
                                            <td> {{ $post->title }} </td>
                                        </tr>
                                        <tr>
                                            <th> Description </th>
                                            <td> {!! $post->description !!} </td>
                                        </tr>
                                        @if(!empty($post->image))
                                        <tr>
                                            <th> Feature image </th>
                                            <td> <img src="{{ asset('images/upload/news/' . $post->image) }}" title="{{ $post->name }}"> </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <th> Bengali Title</th>
                                            <td> {{ $post->title_b }} </td>
                                        </tr>
                                        <tr>
                                            <th> Bengali Description</th>
                                            <td> {!! $post->description_b !!} </td>
                                        </tr>
                                        <tr>
                                            <th> Total Comments</th>
                                            <td> <a href="{{ url('admin/news/' . $post->id . '/comment') }}"> {{ $post->comments->count() }} </a>  </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
