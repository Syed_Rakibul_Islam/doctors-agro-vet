<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Component</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin') || request()->is('admin/dashboard') ? 'active' : '' }}" href="{{ url('/admin/dashboard') }}"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/product*') ? 'active' : '' }}" href="javascript:void(0);" data-toggle="collapse" data-target="#product_dir"><div class="pull-left"><i class="zmdi zmdi-dropbox mr-20"></i><span class="right-nav-text">Product </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="product_dir" class="collapse collapse-level-1">
                <li>
                    <a href="{{ url('admin/product') }}">Product</a>
                </li>
                <li>
                    <a href="{{ url('admin/product/category') }}">Category</a>
                </li>
            </ul>
        </li>
        {{--<li>--}}
            {{--<a class="{{ request()->is('admin/client*') ? 'active' : '' }}" href="{{ url('/admin/client') }}"><div class="pull-left"><i class="zmdi zmdi-male-female mr-20"></i><span class="right-nav-text">Client</span></div><div class="clearfix"></div></a>--}}
        {{--</li>--}}
        <li>
            <a class="{{ request()->is('admin/news*') ? 'active' : '' }}" href="javascript:void(0);" data-toggle="collapse" data-target="#news_dr"><div class="pull-left"><i class="zmdi zmdi-comment mr-20"></i><span class="right-nav-text">News </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="news_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{ url('admin/news') }}">News</a>
                </li>
                <li>
                    <a href="{{ url('admin/news/comment') }}">Comment</a>
                </li>
                <li>
                    <a href="{{ url('admin/news/category') }}">Category</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="{{ request()->is('admin/partner*') ? 'active' : '' }}" href="{{ url('admin/partner') }}"><div class="pull-left"><i class="zmdi zmdi-accounts-list mr-20"></i><span class="right-nav-text">Partner</span></div><div class="clearfix"></div></a>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Settings</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{ request()->is('admin/user*') ? 'active' : '' }}"  href="{{ url('admin/user') }}"><div class="pull-left"><i class="zmdi zmdi-accounts mr-20"></i><span class="right-nav-text">User</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/slide*') ? 'active' : '' }}" href="{{ url('admin/slide') }}"><div class="pull-left"><i class="zmdi zmdi-widgets mr-20"></i><span class="right-nav-text">Slide</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{ request()->is('admin/best-product*') ? 'active' : '' }}" href="{{ url('admin/best-product') }}"><div class="pull-left"><i class="zmdi zmdi-dropbox mr-20"></i><span class="right-nav-text">Best Product</span></div><div class="clearfix"></div></a>
        </li>
        @if(Auth::user()->role == 'super')
            <li>
                <a class="{{ request()->is('admin/retrieve*') ? 'active' : '' }}" href="javascript:void(0);" data-toggle="collapse" data-target="#multilevel_dropdown"><div class="pull-left"><i class="zmdi zmdi-filter-list mr-20"></i><span class="right-nav-text">Retrieve</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="multilevel_dropdown" class="collapse collapse-level-1">
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#retrieve_product_dir">Product<div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                        <ul id="retrieve_product_dir" class="collapse collapse-level-2">
                            <li>
                                <a href="{{ url('admin/retrieve/product') }}">Product</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/retrieve/product/category') }}">Category</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#retrieve_news_dir">News<div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                        <ul id="retrieve_news_dir" class="collapse collapse-level-2">
                            <li>
                                <a href="{{ url('admin/retrieve/news') }}">News</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/retrieve/news/comment') }}">Comment</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/retrieve/news/category') }}">Category</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('admin/retrieve/partner') }}">Partner</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/retrieve/user') }}">User</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/retrieve/slide') }}">Slide</a>
                    </li>
                </ul>
            </li>
        @endif
    </ul>
</div>