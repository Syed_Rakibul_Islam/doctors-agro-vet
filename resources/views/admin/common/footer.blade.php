<footer class="footer container-fluid pl-30 pr-30">
    <div class="row">
        <div class="col-sm-12">
            <p>&copy; Copyright {{ date("Y") }} <a href="https://www.trustdigitalmedia.com/" target="_blank">Trust Digital Media</a>. All rights reserved.</p>
        </div>
    </div>
</footer>