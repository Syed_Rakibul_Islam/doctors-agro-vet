<!-- Title -->
<div class="row heading-bg">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h5 class="txt-dark"> @yield('pageTitle') </h5>
    </div>
    <!-- Breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li>
                <i class="zmdi zmdi-home"></i>
                <a href="{{ url('/') }}">Home</a>
            </li>
            @foreach(Request::segments() as $segment)
                @if(count(Request::segments()) == $loop->iteration)
                    <li class="active" style="text-transform: capitalize;">
                        {{ str_replace('_', ' ', Request::segment($loop->iteration)) }}
                    </li>
                @else
                    <li>
                        <a style="text-transform: capitalize;" href="{{ url('/') }}@for($i = 1; $i <= $loop->iteration; $i++)/{{Request::segment($i)}}@endfor">
                            {{ str_replace('_', ' ', Request::segment($loop->iteration)) }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ol>
    </div>
    <!-- /Breadcrumb -->
</div>
<!-- /Title -->