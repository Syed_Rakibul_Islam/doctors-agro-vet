@php
    $comments = \App\Comment::whereNull('approve')->orderBy('id', 'desc')->get();
@endphp
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <div class="nav-header pull-left">
            <div class="logo-wrap">
                <a href="{{ url('/') }}">
                    <img class="brand-img" src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" style="height: 16px"/>
                    <span class="brand-text" style="color:#02bcef"> Doctor's Agro-vet Ltd </span>
                </a>
            </div>
        </div>
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
        <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

    </div>
    <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <ul class="nav navbar-right top-nav pull-right">
            <li class="dropdown alert-drp">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="zmdi zmdi-notifications top-nav-icon"></i>
                    @if(count($comments) != 0)
                        <span class="top-nav-icon-badge"> {{  count($comments) }} </span>
                    @endif
                </a>
                <ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
                    <li>
                        <div class="notification-box-head-wrap">
                            <span class="notification-box-head pull-left inline-block">Pending Comments </span>
                            <div class="clearfix"></div>
                            <hr class="light-grey-hr ma-0"/>
                        </div>
                    </li>
                    <li>
                        <div class="streamline message-nicescroll-bar">
                            @foreach($comments as $comment)
                                <div class="sl-item">
                                    <a href="{{ isset($comment->post->id) ? url('admin/news/' . $comment->post->id . '/comment/' . $comment->id) : url('admin/news/comment/' . $comment->id)  }}">
                                        <div class="icon bg-green">
                                            <i class="zmdi zmdi-flag"></i>
                                        </div>
                                        <div class="sl-content">
												<span class="inline-block capitalize-font  pull-left truncate head-notifications">
												    @if(!empty($comment->user_id) && isset($comment->user->name))
                                                        {{ $comment->user->name }}
                                                    @else
                                                        {{ $comment->user_name }}
                                                    @endif
                                                </span>
                                            <span class="inline-block font-11  pull-right notifications-time">{{ $comment->date }}</span>
                                            <div class="clearfix"></div>
                                            <p class="truncate">{{ $comment->description }}</p>
                                        </div>
                                    </a>
                                </div>
                                <hr class="light-grey-hr ma-0"/>
                            @endforeach
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown auth-drp">
                <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="{{ asset('admin-resources/dist/img/user1.png') }}" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
                <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <li>
                        <a href="{{ url('admin/profile') }}"><i class="zmdi zmdi-account"></i><span>Profile</span></a>
                    </li>
                    <li>
                        <a href="{{ url('admin/profile/change-password') }}"><i class="zmdi zmdi-key"></i><span>Change Password</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="zmdi zmdi-power"></i><span>Sign Out</span></a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>