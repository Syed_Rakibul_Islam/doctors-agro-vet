<div class="form-group {{ $errors->has('text1') ? 'has-error' : ''}}">
    {!! Form::label('text1', 'Text 1 *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('text1', null, ['class' => 'form-control', 'placeholder' => 'Enter slide text 1', 'maxlength' => '100']) !!}
    {!! $errors->first('text1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text2') ? 'has-error' : ''}}">
    {!! Form::label('text2', 'Text 2 *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('text2', null, ['class' => 'form-control', 'placeholder' => 'Enter slide text 2', 'maxlength' => '100']) !!}
    {!! $errors->first('text2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text3') ? 'has-error' : ''}}">
    {!! Form::label('text3', 'Text 3 *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('text3', null, ['class' => 'form-control', 'placeholder' => 'Enter slide text 3', 'maxlength' => '100']) !!}
    {!! $errors->first('text3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slide_image') ? 'has-error' : ''}}">
    {!! Form::label('slide_image', 'Slide image *', ['class' => 'control-label mb-10']) !!}
    @if(isset($slide) && !empty($slide->image))
        {!! Form::file('slide_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/slide/' . $slide->image), 'data-min-width' =>'1500', 'data-min-height' =>'1000', 'data-max-file-size' => '2M']) !!}
    @else
        {!! Form::file('slide_image', ['class' => 'form-control dropify', 'data-min-width' =>'1500', 'data-min-height' =>'1000', 'data-max-file-size' => '2M', 'required' => 'required']) !!}
    @endif
    {!! $errors->first('slide_image', '<p class="help-block">:message</p>') !!}
</div>
<p class="text-info">NB: Image dimension must be grater than 1500x1000 and size less than 2MB.</p>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>
<div class="form-group {{ $errors->has('text1_b') ? 'has-error' : ''}}">
    {!! Form::label('text1_b', 'বাংলায় ১', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('text1_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় লিখুন ১', 'maxlength' => '100']) !!}
    {!! $errors->first('text1_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text2_b') ? 'has-error' : ''}}">
    {!! Form::label('text2_b', 'বাংলায় ২', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('text2_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় লিখুন ২', 'maxlength' => '100']) !!}
    {!! $errors->first('text2_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text3_b') ? 'has-error' : ''}}">
    {!! Form::label('text3_b', 'বাংলায় ৩', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('text3_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় লিখুন ৩', 'maxlength' => '100']) !!}
    {!! $errors->first('text3_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
