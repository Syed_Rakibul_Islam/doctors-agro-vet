@extends('admin.layouts.app')

@section('pageTitle', ' Product | View | ' . $product->id )

@section('style')

    <!-- Data table CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <a class="btn btn-warning pa-5" href="{{ url('admin/product') }}" title="Back" class=""><i class="zmdi zmdi-arrow-left txt-light"></i></a>
                        <a href="{{ url('admin/product' . $product->id . '/edit') }}" class="btn btn-primary pa-5" title="Edit Product"><i class="zmdi zmdi-edit txt-light"></i></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/product', $product->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="zmdi zmdi-delete txt-light"></i>', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger pa-5',
                                'title' => 'Delete Product',
                                'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        <a href="#" class="pull-left inline-block full-screen mr-15">
                            <i class="zmdi zmdi-fullscreen"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Property</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $product->id }}</td>
                                        </tr>
                                        <tr>
                                            <th> Name </th>
                                            <td> {{ $product->name }} </td>
                                        </tr>
                                        <tr>
                                            <th> Title </th>
                                            <td> {{ $product->title }} </td>
                                        </tr>
                                        <tr>
                                            <th> Description </th>
                                            <td> {!! $product->description !!} </td>
                                        </tr>
                                        @if(!empty($product->image))
                                        <tr>
                                            <th> Feature image </th>
                                            <td> <img src="{{ asset('images/upload/product/' . $product->image) }}" title="{{ $product->name }}"> </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <th> Description </th>
                                            <td> {{ $product->price }} </td>
                                        </tr>
                                        <tr>
                                            <th> Status </th>
                                            <td> {{ ucfirst($product->status) }} </td>
                                        </tr>
                                        <tr>
                                            <th> Categories </th>
                                            <td>
                                                @foreach($product->categories as $category)
                                                <a href="{{ url('admin/product/category' . $category->id) }}" target="_blank"> {{ $category->name }} </a>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th> Bengali Title</th>
                                            <td> {{ $product->title_b }} </td>
                                        </tr>
                                        <tr>
                                            <th> Bengali Description</th>
                                            <td> {!! $product->description_b !!} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection

@section('script')
    <!-- Data table JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-resources/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script>
        /*Export Table Init*/

        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                'bSort': false,
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'columns': [
                    { 'searchable': false },
                    null
                ]
            } );
        } );
    </script>
@endsection
