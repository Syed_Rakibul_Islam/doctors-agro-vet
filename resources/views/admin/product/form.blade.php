{!! Form::hidden('name', (isset($product->name) && !empty($product->name)) ? $product->name : null  ) !!}
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter product title', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    <span id="product_name">
        @if(isset($product->name) && !empty($product->name))
            <br/> Product name : <button type="button" class="btn-link" onclick="editProductName('{{ $product->name }}')"> {{ $product->name }}</button> <button type="button" class="btn btn-default btn-xs" onclick="editProductName('{{ $product->name }}')"> Edit </button>
        @endif
    </span>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control textarea_editor', 'maxlength' => '100000', 'placeholder' => 'Enter product description...']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('feature_image') ? 'has-error' : ''}}">
    {!! Form::label('feature_image', 'Feature image', ['class' => 'control-label mb-10']) !!}
    @if(isset($product) && !empty($product->image))
        {!! Form::file('feature_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/product/' . $product->image), 'data-min-width' =>'480', 'data-min-height' =>'480', 'data-max-width' =>'505', 'data-max-height' =>'505']) !!}
    @else
        {!! Form::file('feature_image', ['class' => 'form-control dropify', 'data-min-width' =>'480', 'data-min-height' =>'480', 'data-max-width' =>'505', 'data-max-height' =>'505' ]) !!}
    @endif
    {!! $errors->first('feature_image', '<p class="help-block">:message</p>') !!}
</div>
<p class="text-info">NB: Image dimension must be 480x480 to 500x500.</p>
<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Enter product price', 'maxlength' => '100']) !!}
    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
    {!! Form::label('category', 'Product Category', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('category[]', $categories, (isset($product->categories) && !empty($product->categories)) ? $product->categories : null, ['class' => 'form-control selectpicker', 'id' => 'category', 'multiple', 'data-style' => 'form-control btn-default btn-outline']) !!}
    {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Product Status', ['class' => 'control-label mb-10']) !!}
    <br/>
    <div class="radio-inline pl-0">
        <span class="radio radio-info">
            {!! Form::radio('status', 'new', false, ['id' => 'status_new']) !!}
            <label for="status_new">New</label>
        </span>
    </div>
    <div class="radio-inline">
        <span class="radio radio-info">
            {!! Form::radio('status', 'old', false, ['id' => 'status_old']) !!}
        <label for="status_old">Available </label>
        </span>
    </div>
    <div class="radio-inline">
        <span class="radio radio-info">
            {!! Form::radio('status', 'out', false, ['id' => 'status_out']) !!}
            <label for="status_out">Unavailable </label>
        </span>
    </div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>

<div class="form-group {{ $errors->has('title_b') ? 'has-error' : ''}}">
    {!! Form::label('title_b', 'বাংলায় শিরোনাম', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('title_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় শিরোনাম লিখুন', 'maxlength' => '100']) !!}
    {!! $errors->first('title_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_b') ? 'has-error' : ''}}">
    {!! Form::label('description_b', 'বাংলায় বিবরণ', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('description_b', null, ['class' => 'form-control textarea_editor', 'placeholder' => 'বাংলায় বিবরণী লিখুন ...']) !!}
    {!! $errors->first('description_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
