@extends('admin.layouts.auth')

@section('pageTitle', 'Sign In')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-dark mb-10">Sign in to <span class="text-danger">{{ config('app.name') }}</span></h3>
                                <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                            </div>
                            <div class="form-wrap">
                                <form method="POST" action="{{ route('admin.login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label mb-10" for="email">Email / Username</label>
                                        <input type="text" class="form-control" name="email" required="" id="email" placeholder="Enter email ID / username">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="pull-left control-label mb-10" for="password">Password</label>
                                        <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="{{ route('admin.password.request') }}">forgot password ?</a>
                                        <div class="clearfix"></div>
                                        <input type="password" class="form-control" name="password" required="" id="password" placeholder="Enter password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox checkbox-primary pr-10 pull-left">
                                            <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label for="remember"> Keep me logged in</label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-info btn-rounded">sign in</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection
