@extends('admin.layouts.auth')

@section('pageTitle', 'Sign Up')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">
                <div class="auth-form  ml-auto mr-auto no-float">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="mb-30">
                                <h3 class="text-center txt-dark mb-10">Sign up to <span class="text-danger">{{ config('app.name') }}</span></h3>
                                <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                            </div>
                            <div class="form-wrap">
                                <form method="POST" action="{{ route('admin.register') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="control-label mb-10" for="name">Name</label>
                                        <input type="text" class="form-control" name="name" required="" id="name" placeholder="Enter your name" autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                        <label class="control-label mb-10" for="username">Username</label>
                                        <input type="text" class="form-control" name="username" required="" id="username" placeholder="Enter username" autofocus>
                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label mb-10" for="email">Email</label>
                                        <input type="email" class="form-control" name="email" required="" id="name" placeholder="Enter your email ID">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
                                        <label class="control-label mb-10" for="contact">Contact number</label>
                                        <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter your contact number">
                                        @if ($errors->has('contact'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('contact') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="pull-left control-label mb-10" for="password">Password</label>
                                        <input type="password" class="form-control" name="password" required="" id="password" placeholder="Enter password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="pull-left control-label mb-10" for="password_confirmation">Confirm Password</label>
                                        <input type="password" class="form-control" name="password_confirmation" required="" id="password_confirmation" placeholder="Enter confirm password">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-info btn-rounded">Sign up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>

@endsection
