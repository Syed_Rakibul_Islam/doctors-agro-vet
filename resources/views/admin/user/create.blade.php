@extends('admin.layouts.app')

@section('pageTitle', ' User | Create' )

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- select2 CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Dropify CSS -->
    <link href="{{ asset('admin-resources/vendors/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" align="center" id="error-alert">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <ul style="list-style-type: none;">
                                                @foreach ($errors->all() as $error)
                                                    <li >{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {!! Form::open(['url' => 'admin/user', 'id' => 'user-form', 'data-toggle' => 'validator', 'files'=>true]) !!}

                                    @include ('admin.user.form')

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection


@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script>
        var baseUrl = '{{ url('/') }}';
        $(function() {

            "use strict";

            $('#role').select2({});

            // Input File

            /* Basic Init*/
            $('.dropify').dropify();

            /* Used events */
            //
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });




            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("input[name=username]").change(function(){
                var username = this.value;
                $.ajax({
                    type: 'GET',
                    url: baseUrl + '/admin/ajax/check-username',
                    data: {username:username},
                    success: function (result) {
                        if(result == 'no'){
                            $('#username-field').html('<p class="text-success"> Valid username </p>');
                        }
                        else{
                            $("input[name=username]").val('');
                            $("#user-form").validator('validate');
                            $('#username-field').html('<p class="text-danger" > This "' + username + '" username already exist. Please try different username. </p>');
                        }

                    }
                });
            });
            $("input[name=email]").change(function(){
                var email = this.value;
                $.ajax({
                    type: 'GET',
                    url: baseUrl + '/admin/ajax/check-user-email',
                    data: {email:email},
                    success: function (result) {
                        if(result == 'no'){
                            $('#email-field').html('<p class="text-success"> Valid email </p>');
                        }
                        else{
                            $("input[name=email]").val('');
                            $("#user-form").validator('validate');
                            $('#email-field').html('<p class="text-danger" > This "' + email + '" email ID already exist. Please try different email ID. </p>');
                        }

                    }
                });
            });



        });
    </script>
@endsection