<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
@if(!isset($user))
<div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
    {!! Form::label('username', 'Username *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Enter username', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    <span id="username-field"></span>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'Enter password', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    <span id="username-field"></span>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    <span id="email-field"></span>
</div>
@endif
<div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
    {!! Form::label('contact', 'Contact Number', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Enter contact number', 'maxlength' => '100']) !!}
    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('role') ? 'has-error' : ''}}">
    {!! Form::label('role', 'Role', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('role', ['user' => 'User', 'admin' => 'Admin'], null, ['class' => 'form-control selectpicker', 'id' => 'role', 'data-style' => 'form-control btn-default btn-outline']) !!}
    {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user_image') ? 'has-error' : ''}}">
    {!! Form::label('user_image', 'User image', ['class' => 'control-label mb-10']) !!}
    @if(isset($user) && !empty($user->image))
        {!! Form::file('user_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/user/' . $user->image), 'data-max-file-size' => '300K']) !!}
    @else
        {!! Form::file('user_image', ['class' => 'form-control dropify', 'data-max-file-size' => '300K']) !!}
    @endif
    {!! $errors->first('user_image', '<p class="help-block">:message</p>') !!}
</div>
<p class="text-info">NB: Image size must be less than 300KB.</p>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter address', 'maxlength' => '255']) !!}
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
    {!! Form::label('about', 'About', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('about', null, ['class' => 'form-control', 'placeholder' => 'Enter about user ...', 'maxlength' => '100000']) !!}
    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
</div>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>
<div class="form-group {{ $errors->has('name_b') ? 'has-error' : ''}}">
    {!! Form::label('name_b', 'বাংলায় নাম', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় নাম লিখুন', 'maxlength' => '100']) !!}
    {!! $errors->first('name_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
