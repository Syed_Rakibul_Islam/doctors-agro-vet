<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name', 'maxlength' => '100', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'E-mail', ['class' => 'control-label mb-10']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email address', 'maxlength' => '100']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
    {!! Form::label('contact', 'Contact', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Enter contact number', 'maxlength' => '100']) !!}
    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('partner_image') ? 'has-error' : ''}}">
    {!! Form::label('partner_image', 'Partner image', ['class' => 'control-label mb-10']) !!}
    @if(isset($partner) && !empty($partner->image))
        {!! Form::file('partner_image', ['class' => 'form-control dropify', 'data-default-file' => asset('images/upload/partner/' . $partner->image), 'data-max-file-size' => '300K']) !!}
    @else
        {!! Form::file('partner_image', ['class' => 'form-control dropify', 'data-max-file-size' => '300K']) !!}
    @endif
    {!! $errors->first('partner_image', '<p class="help-block">:message</p>') !!}
</div>
<p class="text-info">NB: Image size must be less than 300KB.</p>
<div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
    {!! Form::label('country_id', 'Country', ['class' => 'control-label mb-10']) !!}
    {!! Form::select('country_id', $countries, null, ['class' => 'form-control', 'id' => 'country_id', 'placeholder' => 'Select Country', 'data-style' => 'form-control btn-default btn-outline']) !!}
    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter partner address', 'maxlength' => '100']) !!}
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    {!! Form::label('url', 'Website URL', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Enter partner website url', 'maxlength' => '100']) !!}
    {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
    {!! Form::label('about', 'About', ['class' => 'control-label mb-10']) !!}
    {!! Form::textarea('about', null, ['class' => 'form-control', 'placeholder' => 'Enter about partner ...', 'maxlength' => '100000']) !!}
    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
</div>
<hr>
<h4 class="text-center">বাংলা সংস্করণ</h4>
<div class="form-group {{ $errors->has('name_b') ? 'has-error' : ''}}">
    {!! Form::label('name_b', 'বাংলায় নাম', ['class' => 'control-label mb-10']) !!}
    {!! Form::text('name_b', null, ['class' => 'form-control', 'placeholder' => 'বাংলায় নাম লিখুন', 'maxlength' => '100']) !!}
    {!! $errors->first('name_b', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group mb-0">
    <button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Submit</span></button>
</div>
