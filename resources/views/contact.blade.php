@extends('layouts.app')

@section('pageTitle', __('navigation.contact'))

@section('style')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/map.css') }}" />
@endsection

@section('content')
    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-100"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <!-- Map -->
                        <div class="map col-md-12">
                            <div id="map_canvas_for_contact" class="map-canvas map_2"></div>
                        </div><!-- Map /- -->
                        <div class="col-md-12">
                            <div class="contactinfo-block">
                                <div class="col-md-4 contactbox">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <div class="contactdetail">
                                        <h3>{{ __('auth.address') }}</h3>
                                        <p>Nurjehan Tower (5th Floor), 2, Link Road, Banglamotor, Dhaka-1200, Bangladesh</p>
                                    </div>
                                </div>
                                <div class="col-md-4 contactbox">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <div class="contactdetail">
                                        <h3>{{ __('auth.email') }}</h3>
                                        <a title="info@doctorsagrovetltd.com" href="mailto:info@doctorsagrovetltd.com">info@doctorsagrovetltd.com</a>
                                    </div>
                                </div>
                                <div class="col-md-4 contactbox">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <div class="contactdetail">
                                        <h3>{{ __('auth.contact_no') }}</h3>
                                        <a href="tel:8802-9673737" title="8802-9673737">+8802-9673737</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form id="contact-form1" class="contactus-form">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" name="contact_name" class="form-control" placeholder="{{ __('auth.name') }} *" required=""/>
                                                    <span class="help-block" id="contact_name_help"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="email" name="contact_email" class="form-control" placeholder="{{ __('auth.email') }} *" required=""/>
                                                    <span class="help-block" id="contact_email_help"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" name="contact_subject" class="form-control" placeholder="{{ __('auth.subject') }} *"/>
                                                    <span class="help-block" id="contact_subject_help"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <textarea rows="5" required="" name="contact_message" class="form-control" placeholder="{{ __('auth.message') }} *"></textarea>
                                                    <span class="help-block" id="contact_message_help"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            {!! Recaptcha::render() !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="submit" value="{{ __('auth.submit') }}" id="btn_contact_submit" title="Send" name="post">
                                        </div>
                                    </div>
                                    <div id="contact-alert-message" class="alert-msg"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-100"></div>
        </div><!-- Contact Forms Section /- -->
    </main>
@endsection

@section('script')
    <script src="{{ asset('js/richmarker-compiled.js') }}"></script>
    <script src="{{ asset('js/markerclusterer_packed.js') }}"></script>
    <script src="{{ asset('js/infobox.js') }}"></script>

    <script src="{{ asset('js/contact.maps.js') }}"></script>


    <script>
        //map height
        var optimizedDatabaseLoading = 0;
        var _latitude = 23.6850;
        var _longitude = 90.3563;
        var element = "map_canvas_for_contact";
        var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
        var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
        var showMarkerLabels = false; // next to every marker will be a bubble with title
        var mapDefaultZoom = 9; // default zoom
        heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);

        (function($)
        {
            "use strict"


            $( "input[name=contact_name]" ).on( "keyup", function() {
                var value = $(this).val();
                if(value.length == 0){
                    $('#contact_name_help').css({'color': '#F00'}).html('Name required');
                }
                else{
                    $('#contact_name_help').css({'color': '#737373'}).html('');
                }

            });
            $( "input[name=contact_email]" ).on( "keyup", function() {
                var value = $(this).val();
                if(value.length == 0){
                    $('#contact_email_help').css({'color': '#F00'}).html('Email required');
                }
                else{
                    $('#contact_email_help').css({'color': '#737373'}).html('');
                }

            });
            $( "input[name=contact_subject]" ).on( "keyup", function() {
                var value = $(this).val();
                if(value.length == 0){
                    $('#contact_subject_help').css({'color': '#F00'}).html('Subject required');
                }
                else{
                    $('#contact_subject_help').css({'color': '#737373'}).html('');
                }

            });
            $( "textarea[name=contact_message]" ).on( "keyup", function() {
                var value = $(this).val();
                if(value.length == 0){
                    $('#contact_message_help').css({'color': '#F00'}).html('Message required');
                }
                else{
                    $('#contact_message_help').css({'color': '#737373'}).html('');
                }

            });
            /* - Contact Form 2*/
            $( "#btn_contact_submit" ).on( "click", function(event) {
                event.preventDefault();
                var contactName = $('input[name=contact_name]').val();
                var contactEmail = $('input[name=contact_email]').val();
                var contactSubject = $('input[name=contact_subject]').val();
                var contactMessage = $('textarea[name=contact_message]').val();
                if(contactName.length != 0 && contactEmail.length != 0 && contactSubject.length != 0 && contactMessage.length != 0){
                    var mydata = $("form").serialize();
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: baseURL + "/ajax/contact",
                        data: mydata,
                        success: function(data) {
                            if( data["type"] == "error" ){
                                $("#contact-alert-message").html(data["msg"]);
                                $("#contact-alert-message").removeClass("alert-msg-success");
                                $("#contact-alert-message").addClass("alert-msg-failure");
                                $("#contact-alert-message").show();
                            } else {
                                $("#contact-alert-message").html(data["msg"]);
                                $("#contact-alert-message").addClass("alert-msg-success");
                                $("#contact-alert-message").removeClass("alert-msg-failure");
                                $("input[name=contact_name]").val("");
                                $("input[name=contact_email]").val("");
                                $("input[name=contact_subject]").val("");
                                $("textarea[name=contact_message]").val("");
                                $("#contact-alert-message").show();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            //alert(textStatus);
                        }
                    });
                    return false;
                }
                else {
                    if(contactName.length == 0){
                        $('#contact_name_help').css({'color': '#F00'}).html('Name required');
                    }
                    else{
                        $('#contact_name_help').css({'color': '#737373'}).html('');
                    }
                    if(contactEmail.length == 0){
                        $('#contact_email_help').css({'color': '#F00'}).html('Email required');
                    }
                    else{
                        $('#contact_email_help').css({'color': '#737373'}).html('');
                    }
                    if(contactSubject.length == 0){
                        $('#contact_subject_help').css({'color': '#F00'}).html('Subject required');
                    }
                    else{
                        $('#contact_subject_help').css({'color': '#737373'}).html('');
                    }
                    if(contactMessage.length == 0){
                        $('#contact_message_help').css({'color': '#F00'}).html('Message required');
                    }
                    else{
                        $('#contact_message_help').css({'color': '#737373'}).html('');
                    }
                    $('#contact-alert-message').html('');
                }

            });/* Quick Contact Form /- */

        })(jQuery);
    </script>
@endsection