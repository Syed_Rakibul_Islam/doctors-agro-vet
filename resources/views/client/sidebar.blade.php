@forelse($clients as $client)
    <div class="panel panel-default map-sidebar-content" data-id="{{ $client->id }}">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 text-center">
                    <img class="img-circle img-thumbnail img-responsive" src="{{ !empty($client->image) && file_exists('images/upload/client/' . $client->image) ? url('images/upload/client/' . $client->image) : url('images/client.png') }}" />
                </div>
                <div class="col-xs-8 text-center">
                    <p> <strong>{{ config('app.locale') == "bn" && !empty($client->name_b) ? str_limit(strip_tags($client->name_b), 50) : str_limit(strip_tags($client->name . 'jhdkjsjkdfs sddff sd sdff'), 13) }}</strong></p>
                    <p> <strong>{{ isset($client->district->id) && !empty($client->district->name) ? $client->district->name : '' }}</strong></p>
                </div>
            </div>
        </div>
    </div>
    
@empty
    <h1>No client found</h1>
@endforelse