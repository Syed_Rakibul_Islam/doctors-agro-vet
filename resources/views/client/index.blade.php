@extends('layouts.app')

@section('pageTitle', __('navigation.clients'))

@section('style')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('js/scrollbar/jquery.mCustomScrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/map.css') }}" />
@endsection

@section('content')
    <main class="site-main page-spacing">
        <!-- Columns Section -->
        <div class="columns-section container-fluid">
            <div class="row">


                <div class="col-md-10 no-padding pull-right col-sm-pull-left">
                    <div class="map-wrapper">
                        <div class="map" id="map-homepage"></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="padding-15"></div>
                    <!--end map-wrapper-->
                    <div class="results-wrapper">
                        <div class="form search-form inputs-underline">
                            <form id="map-search" class="contactus-form map-search-form">
                                <div class="form-group">
                                    <input type="text" name="client_name" class="form-control" placeholder="Client name" />
                                </div>
                                <div class="form-group">
                                    <select name="client_district" class="form-control">
                                        <option value="" selected>Select district</option>
                                        @foreach($districts as $district)
                                            <option value="{{ $district->id }}" >{{ ucfirst($district->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Submit" id="serach-button" title="Send" name="post">
                                </div>
                            </form>
                            <!--end form-hero-->
                        </div>
                        <div class="results">
                            <div class="tse-scrollable">
                                <div class="tse-content">
                                    <div class="section-title">
                                        <h2>Search Results <span class="results-number"></span></h2>
                                    </div>

                                    <!--end section-title-->
                                    <div id="content-result-loading" class="text-center">
                                        <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                    <div id="results-content" style="display: none"></div>
                                    <!--end results-content-->
                                </div>
                                <!--end tse-content-->
                            </div>
                            <!--end tse-scrollable-->
                        </div>
                        <!--end results-->
                    </div>
                    <!--end results-wrapper-->
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="well text-center"><strong>Districts</strong></div>
                <hr/>
                <div class="col-md-3 col-sm-6">
                @foreach($districts as $district)
                    @if(in_array($loop->iteration, [18, 35, 52]))
                        </div>
                        <div class="col-md-3 col-sm-6">
                    @endif
                    <ul class="list-group">
                        <li class="list-group-item"><a  href="{{ url('clients/' . strtolower($district->name)) }}"><i class="fa fa-angle-double-right"></i> {{ $district->name }}</a><span class="badge">{{ count($district->clients) > 0 ? count($district->clients) : '' }}</span></li>
                    </ul>
                @endforeach
                </div>
            </div>
        </div><!-- Columns Section /- -->
    </main>

    <!-- Modal -->
    <div class="modal fade" id="modal-client" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="service-table">
                        <div class="service-box">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/richmarker-compiled.js') }}"></script>
    <script src="{{ asset('js/markerclusterer_packed.js') }}"></script>
    <script src="{{ asset('js/infobox.js') }}"></script>
    <script src="{{ asset('js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <script src="{{ asset('js/maps.js') }}"></script>

    <script>
        //map height
        var windowHeight = window.innerHeight;
        var mainHeaderHeight = document.getElementById('main-menu-area').clientHeight;
        var mapHeight = windowHeight - mainHeaderHeight;
        document.getElementById('map-homepage').style.height = mapHeight + 'px';
        var resultContentOffset = document.getElementById('content-result-loading').offsetTop;
        var scrollerBarSize = windowHeight - resultContentOffset;


        var optimizedDatabaseLoading = 0;
        var _latitude = 23.6850;
        var _longitude = 90.3563;
        var element = "map-homepage";
        var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
        var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
        var showMarkerLabels = false; // next to every marker will be a bubble with title
        var mapDefaultZoom = 8; // default zoom
        heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);

    </script>
@endsection
