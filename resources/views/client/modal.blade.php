@if(!empty($client))
<h3>{{ __('client') }} | {{ config('app.locale') == "bn" && !empty($client->name_b) ? $client->name_b : ucfirst($client->name) }}</h3>
<div class="service-content">
    <img class="img-circle img-thumbnail img-responsive" src="{{ !empty($client->image) && file_exists('images/upload/client/' . $client->image) ? url('images/upload/client/' . $client->image) : url('images/client.png') }}" style="max-height: 400px" />
    <ul>
        <li>Email: {{ $client->email }}</li>
        <li>Contact: {{ $client->contact }}</li>
        <li>Address: {{ $client->location }}</li>
        <li>District: {{ isset($client->district->id) ? $client->district->name : '' }}</li>
        <li>About: {{ $client->about }}</li>
    </ul>
</div>
@else
    <h4 class="text-danger">{{ __('client') }} | Not Found</h4>
@endif