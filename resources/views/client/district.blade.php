@extends('layouts.app')

@section('pageTitle', __('navigation.clients') . ' | ' . __('navigation.district'))

@section('style')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('js/scrollbar/jquery.mCustomScrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/map.css') }}" />
@endsection

@section('content')
    <main class="site-main page-spacing">
        <!-- Columns Section -->
        <div class="columns-section container-fluid">
            <div class="row">


                <div class="col-md-12 no-padding pull-right col-sm-pull-left">
                    <div class="map-wrapper">
                        <div class="map" id="map-homepage"></div>
                    </div>
                </div>

            </div>
        </div><!-- Columns Section /- -->
    </main>

    <!-- Modal -->
    <div class="modal fade" id="modal-client" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="service-table">
                        <div class="service-box">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/richmarker-compiled.js') }}"></script>
    <script src="{{ asset('js/markerclusterer_packed.js') }}"></script>
    <script src="{{ asset('js/infobox.js') }}"></script>
    <script src="{{ asset('js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <script src="{{ asset('js/district.maps.js') }}"></script>

    <script>
        //map height
        var windowHeight = window.innerHeight;
        var mainHeaderHeight = document.getElementById('main-menu-area').clientHeight;
        var mapHeight = windowHeight - mainHeaderHeight;
        document.getElementById('map-homepage').style.height = mapHeight + 'px';


        var distrcitID = "{{ $district->id }}";
        var optimizedDatabaseLoading = 0;
        var _latitude = 23.6850;
        var _longitude = 90.3563;
        var element = "map-homepage";
        var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
        var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
        var showMarkerLabels = false; // next to every marker will be a bubble with title
        var mapDefaultZoom = 8; // default zoom
        heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom, distrcitID);

    </script>
@endsection
