@extends('layouts.app')

@section('pageTitle', __('sign_in'))

@section('content')

    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-100"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form id="login-form" class="login-form" method="post" action="{{ route('login') }}">
                                <h1 class="text-center">{{ __('navigation.sign_in') }}</h1>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('auth.email') }} / {{ __('auth.username') }} *" required />
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input type="password" name="password" class="form-control" placeholder="{{ __('auth.password') }} *" required />
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" value="{{ __('auth.submit') }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="pull-right">{{ __('msg.no_acc') }} <a href="{{ route('register') }}">{{ __('navigation.sign_up') }}</a></p>
                                        <p><a href="{{ route('password.request') }}">{{ __('navigation.forgotten_acc') }}? </a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-100"></div>
        </div><!-- Contact Forms Section /- -->
    </main>

@endsection
