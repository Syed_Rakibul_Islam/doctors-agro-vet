@extends('layouts.app')

@section('pageTitle', __('navigation.reset_password'))

@section('content')
    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-100"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form id="login-form" class="login-form" method="post" action="{{ route('password.email') }}">
                                <h1 class="text-center">{{ __('navigation.reset_password') }}</h1>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('auth.email') }} *" required />
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" value="{{ __('auth.submit') }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="pull-right">{{ __('msg.is_acc') }} <a href="{{ route('login') }}">{{ __('navigation.sign_in') }}</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-100"></div>
        </div><!-- Contact Forms Section /- -->
    </main>
@endsection
