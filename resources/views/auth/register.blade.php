@extends('layouts.app')

@section('pageTitle', __('navigation.sign_up'))

@section('content')

    <main class="site-main page-spacing">
        <!-- Contact Forms Sections -->
        <div class="contact-forms-section">
            <div class="padding-50"></div>
            <div class="container">
                <!-- Contact Forms Block -->
                <div class="contact-forms-block layout2">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form id="login-form" class="login-form" method="post" action="{{ route('register') }}">
                                <h1 class="text-center">{{ __('navigation.sign_up') }}</h1>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="{{ __('auth.name') }} *" required />
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                            <input type="text" name="username" class="form-control" value="{{ old('username') }}" placeholder="{{ __('auth.username') }} *" required />
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ __('auth.email') }} *" required />
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
                                            <input type="text" name="contact" class="form-control" value="{{ old('contact') }}" placeholder="{{ __('auth.contact_no') }} " />
                                            @if ($errors->has('contact'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contact') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                            <textarea rows="2" name="address" class="form-control" value="{{ old('address') }}"  placeholder="{{ __('auth.address') }}"></textarea>
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input type="password" name="password" class="form-control" placeholder="{{ __('auth.password') }} " />
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="{{ __('auth.password_confirmation') }} *" required />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" value="{{ __('auth.submit') }}" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="pull-right">{{ __('msg.is_acc') }} <a href="{{ route('login') }}">{{ __('navigation.sign_in') }}</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- Contact Forms Block /- -->
            </div>
            <div class="padding-50"></div>
        </div><!-- Contact Forms Section /- -->
    </main>

@endsection
