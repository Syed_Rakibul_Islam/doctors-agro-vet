@extends('layouts.app')

@section('pageTitle', __('navigation.home'))

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/navigation.css') }}">
@endsection

@section('content')
    <main>
        <!-- Slider Section -->
        <div id="home1-slider" class="slider-section container-fluid no-padding ">
            <!-- START REVOLUTION SLIDER 5.0 -->
            <div class="rev_slider_wrapper">
                <div id="slider1" class="rev_slider" data-version="5.0">
                    <ul>
                        @forelse($slides as $slide)
                            <li data-transition="fade" data-slotamount="1"  data-easein="default" data-easeout="default" data-masterspeed="1500">
                                <!-- MAIN IMAGE -->
                                <img src="{{ !empty($slide->image) && file_exists('images/upload/slide/' . $slide->image) ? url('images/upload/slide/' . $slide->image) : 'http://placehold.it/1920x1025' }}" alt="Slide {{ $loop->iteration }}"  width="1920" height="1025">

                                @if(!empty($slide->text1))
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" id="slide-1-layer-1"
                                         data-x="['left','left','left','left']" data-hoffset="['330','330','330','330']"
                                         data-y="['top','top','top','top']"  data-voffset="['240','240','240','240']"
                                         data-fontsize="['30','30','30','30']"
                                         data-lineheight="['110','110','110','110']"
                                         data-width="none"
                                         data-height="none"
                                         data-whitespace="nowrap"
                                         data-transform_idle="o:1;"
                                         data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                         data-start="1000"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         data-elementdelay="0.05"
                                         style="z-index: 5; color:#727272; font-weight:400;  font-style:italic; font-family: 'Open Sans', sans-serif;">{{ (config('app.locale') == "bn") && !empty($slide->text1_b) ? $slide->text1_b : $slide->text1 }}
                                    </div>
                                @endif

                                @if(!empty($slide->text2))
                                    <!-- LAYER NR. 2 -->
                                    <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
                                         id="slide-1-layer-2"
                                         data-x="['left','left','left','left']" data-hoffset="['330','330','330','330']"
                                         data-y="['top','top','top','top']" data-voffset="['340','340','340','340']"
                                         data-fontsize="['100','100','100','100']"
                                         data-lineheight="['110','110','110','110']"
                                         data-width="auto"
                                         data-height="none"
                                         data-whitespace="noraml"
                                         data-transform_idle="o:1;"
                                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                         data-start="1500"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         style="z-index: 6; color:#212121; font-family: 'Montserrat', sans-serif; font-weight:600; text-transform:capitalize; letter-spacing:-10px;">{{ (config('app.locale') == "bn") && !empty($slide->text2_b) ? $slide->text2_b : $slide->text2 }}
                                    </div>
                                @endif

                                @if(!empty($slide->text3))
                                    <!-- LAYER NR. 3 -->
                                    <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
                                         id="slide-1-layer-3"
                                         data-x="['left','left','left','left']" data-hoffset="['330','330','330','330']"
                                         data-y="['top','top','top','top']" data-voffset="['500','500','500','500']"
                                         data-fontsize="['16','16','16','16']"
                                         data-lineheight="['32.87','32.87','32.87','32.87']"
                                         data-width="auto"
                                         data-height="none"
                                         data-whitespace="noraml"
                                         data-transform_idle="o:1;"
                                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                         data-start="1500"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         style="z-index: 6; font-family: 'Montserrat', sans-serif; color:#fff; text-transform:uppercase; background-color:#02bcef; padding: 12px 30px; letter-spacing:0px;">{{ (config('app.locale') == "bn") && !empty($slide->text3_b) ? $slide->text3_b : $slide->text3 }}
                                    </div>
                                @endif
                            </li>
                        @empty
                            <li data-transition="fade">
                                <!-- MAIN IMAGE -->
                                <img src="http://placehold.it/1920x1025" alt="home1"  width="1920" height="1025">

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-3-layer-1"
                                     data-x="['left','left','left','left']" data-hoffset="['330','330','330','330']"
                                     data-y="['top','top','top','top']"  data-voffset="['240','240','240','240']"
                                     data-fontsize="['30','30','30','30']"
                                     data-lineheight="['110','110','110','110']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;"
                                     data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="chars"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     data-elementdelay="0.05"
                                     style="z-index: 5; color:#727272; font-weight:400;  font-style:italic; font-family: 'Open Sans', sans-serif;">Aliquam
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
                                     id="slide-3-layer-2"
                                     data-x="['left','left','left','left']" data-hoffset="['330','330','330','330']"
                                     data-y="['top','top','top','top']" data-voffset="['340','340','340','340']"
                                     data-fontsize="['100','100','100','100']"
                                     data-lineheight="['110','110','110','110']"
                                     data-width="auto"
                                     data-height="none"
                                     data-whitespace="noraml"
                                     data-transform_idle="o:1;"
                                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1500"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 6; color:#212121; font-family: 'Montserrat', sans-serif; font-weight:600; text-transform:capitalize; letter-spacing:-10px;">Aliquam <br>Consectetur
                                </div>
                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
                                     id="slide-3-layer-3"
                                     data-x="['left','left','left','left']" data-hoffset="['330','330','330','330']"
                                     data-y="['top','top','top','top']" data-voffset="['595','595','595','595']"
                                     data-fontsize="['16','16','16','16']"
                                     data-lineheight="['32.87','32.87','32.87','32.87']"
                                     data-width="auto"
                                     data-height="none"
                                     data-whitespace="noraml"
                                     data-transform_idle="o:1;"
                                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1500"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 6; font-family: 'Montserrat', sans-serif; color:#fff; text-transform:uppercase; background-color:#02bcef; padding: 12px 30px; letter-spacing:0px;">read more
                                </div>
                            </li>
                        @endforelse
                    </ul>
                </div><!-- END REVOLUTION SLIDER -->
            </div><!-- END OF SLIDER WRAPPER -->
        </div><!-- Slider Section -->

        <!-- Full Chart -->
        <div class="chart-section full-chart gray-bg container-fluid no-padding">
            <div class="padding-100"></div>
            <!-- Container -->
            <div class="container">
                <!-- Section Header -->
                <div class="section-header">
                    <h3>Best Products</h3>
                </div><!-- Section Header /- -->
            </div><!-- Container /- -->
        </div><!-- Full Chart /- -->

        <!-- Interractive Banner -->
        <div class="interractive-banner interractive-banner-1 container-fluid no-padding">
            @foreach($bestProducts as $bestProduct)
                <a href="{{ isset($bestProduct->product->name) ? url('products/' . $bestProduct->product->name) : '#' }}">
                    <div class="col-md-3 col-sm-6 col-xs-6 no-padding">
                        <div class="interractive-box interractive-box-1 text-center">
                            <img src="{{ !empty($bestProduct->product->image) && file_exists('images/upload/product/' . $bestProduct->product->image) ? asset('images/upload/product/' . $bestProduct->product->image) : asset('images/product.png') }}" alt="interractive" width="300" />
                            <div class="interractive-back-content interractive-back-content-1">
                                <h4>
                                    @if(config('app.locale') == "bn" && !empty($bestProduct->product->title_b))
                                        {{ \Illuminate\Support\Str::words($bestProduct->product->title_b, 4,' ...') }}
                                    @elseif(!empty($bestProduct->product->title))
                                        {{ \Illuminate\Support\Str::words($bestProduct->product->title, 4,' ...') }}
                                    @endif
                                </h4>
                                <p>{{ config('app.locale') == "bn" && !empty($bestProduct->description_b) ? \Illuminate\Support\Str::words($bestProduct->description_b, 60,' ...') : \Illuminate\Support\Str::words($bestProduct->description, 60,' ...') }}</p>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div><!-- Interractive Banner / -->


        <!-- Full Chart -->
        <div class="chart-section full-chart gray-bg container-fluid no-padding">
            <div class="padding-100"></div>
            <!-- Container -->
            <div class="container">
                <!-- Section Header -->
                <div class="section-header">
                    <h3>Global Partners</h3>
                </div><!-- Section Header /- -->
            </div><!-- Container /- -->
        </div><!-- Full Chart /- -->
        <!-- Clients Section Bg -->
        <div class="partners-section container-fluid no-padding ">
            <div class="padding-50"></div>
            <div class="partners-carousel">
                @foreach($partners as $partner)
                <div class="col-md-12 item">
                    <a href="{{ !empty($partner->url) ? $partner->url : '#' }}" {{ !empty($partner->url) ? 'target="_blank"' : '' }} title="{{ $partner->name }}">
                        <img src="{{ !empty($partner->image) && file_exists('images/upload/partner/' . $partner->image) ? asset('images/upload/partner/' . $partner->image) : asset('images/partner.png') }}" alt="{{ $partner->name }}"/>
                        <h4 class="text-center" >{{ $partner->name }}</h4>
                        <h4 class="text-center" >{{ !empty($partner->country->name) ? $partner->country->name : '' }}</h4>
                    </a>
                </div>
                @endforeach
            </div>
            <div class="padding-50"></div>
        </div><!-- Clients Section Bg /- -->

    </main>
@endsection

@section('script')
    <!-- RS5.0 Core JS Files -->
    <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script>
    <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script>
    <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>

@endsection
