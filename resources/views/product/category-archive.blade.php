@extends('layouts.app')

@php
    if(!empty($category)){
        $categoryName = config('app.locale') == "bn" && !empty($category->name_b) ? $category->name_b : $category->name;
    }
    else{
        $categoryName = __('msg.undefined');
    }
@endphp

@section('pageTitle', __('navigation.products') . ' | ' . __('navigation.category') . ' | ' . $categoryName)

@section('content')
    <main class="site-main page-spacing">
        <!-- Shop Section -->
        <div id="product-section" class="woocommerce product-section-no-sidebar container-fluid no-padding">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li> <a href="{{ url('products') }}">{{ __('navigation.products') }}</a></li>
                        <li><a href="{{ url('products/category') }}">{{ __('navigation.category') }}</a></li>
                        <li class="active">{{ $categoryName }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            @include('product.common.products-content')
        </div><!-- Shop Section /- -->
    </main>
@endsection

@section('script')
    <script>
        $(function() {
            $('#orderby').change(function(){
                $('.woocommerce-ordering').submit();
            });
        });
    </script>
@endsection