<!-- Container -->
<div class="container">
    <div class="row">
        <!-- Content Area -->
        <div class="content-area col-md-12 col-sm-12 col-xs-12">
            @php
                $totalShowProducts = (config('app.locale') == "bn") ? \App\Functions\NumberConversion::en2bn($products->count()) : $products->count();
                $totalProduct = (config('app.locale') == "bn") ? \App\Functions\NumberConversion::en2bn($productCount) : $productCount;
            @endphp
            <p class="woocommerce-result-count">{{ __('msg.result_msg', ['show' => $totalShowProducts, 'total' => $totalProduct]) }}</p>
            <form method="get" class="woocommerce-ordering">
                <select class="orderby" id="orderby" name="orderby" >
                    <option value="desc" {{ request()->has('orderby') && request()->orderby == 'desc' ? 'selected' : '' }}>{{ __('msg.newest_first') }}</option>
                    <option value="asc" {{ request()->has('orderby') && request()->orderby == 'asc' ? 'selected' : '' }}>{{ __('msg.oldest_first') }}</option>
                </select>
            </form>
            <ul class="products row" style="width: 100%">
                @forelse($products as $product)
                    @php
                        $productTitle = config('app.locale') == "bn" && !empty($product->title_b) ? $product->title_b : $product->title;
                    @endphp
                    <li class="product">
                        <a href="{{ url('products/' . $product->name) }}" title="{{ $productTitle }}">
                            <div class="product-img-box">
                                <img alt="shop" src="{{ !empty($product->image) && file_exists('images/upload/product/' . $product->image) ? url('images/upload/product/' . $product->image) : url('images/product.png') }}" style="width: 271px; height: 271px" />
                                @if($product->status == 'new')
                                    <span class="onsale">{{ __('msg.new') }}</span>
                                @elseif($product->status == 'out')
                                    <span class="sold">{{ __('msg.out') }}</span>
                                @endif
                            </div>
                            <div class="detail-box">
                                <h3>{{ $productTitle }}</h3>
                            </div>
                        </a>
                        <a class="button product_type_simple add_to_cart_button" href="{{ url('products/' . $product->name) }}">{{ (config('app.locale') == "bn") ? \App\Functions\NumberConversion::en2bn($product->price) : $product->price }} &#2547;</a>
                    </li>
                @empty
                    <h1>{{ __('msg.not_found', ['name' => __('navigation.product')]) }}</h1>
                @endforelse
            </ul>
            <div class="clearfix"></div>
            <div class="pagination-wrapper text-center"> {!! $products->appends(Request::only('orderby'))->render() !!} </div>
        </div><!-- Content Area /- -->
    </div>
</div><!-- Container /- -->
<div class="padding-50"></div>