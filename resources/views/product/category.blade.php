@extends('layouts.app')

@section('pageTitle', __('navigation.products') . ' | ' . __('navigation.category'))

@section('content')
    <main class="site-main page-spacing">
        <!-- Shop Section -->
        <div id="product-section" class="woocommerce product-section-no-sidebar container-fluid no-padding">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li> <a href="{{ url('products') }}">{{ __('navigation.products') }}</a></li>
                        <li class="active">{{ __('navigation.category') }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <!-- Content Area -->
                    <div class="content-area col-md-12 col-sm-12 col-xs-12">
                        <p class="woocommerce-result-count">Showing {{ $categories->count() }} </p>

                        <div class="clearfix"></div>
                        <ul class="list-group">
                            @foreach($categories as $category)
                                @if(count($category->products) > 0)
                                    @if(config('app.locale') == "bn" && !empty($category->name_b))
                                        <li class="list-group-item"><a title="{{ $category->name_b }} পণ্য" href="{{ url("products/category/" . strtolower($category->name)) }}">{{ $category->name_b }} পণ্য</a> <span class="badge">{{ \App\Functions\NumberConversion::en2bn(count($category->products)) }}</span></li>
                                    @else
                                        <li class="list-group-item"><a title="{{ ucfirst($category->name) }} Product" href="{{ url("products/category/" . strtolower($category->name)) }}">{{ ucfirst($category->name) }} Product</a> <span class="badge">{{ count($category->products) }}</span></li>
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </div><!-- Content Area /- -->
                </div>
            </div><!-- Container /- -->
            <div class="padding-50"></div>
        </div><!-- Shop Section /- -->
    </main>
@endsection

@section('script')
    <script>
        $(function() {
            $('#orderby').change(function(){
                $('.woocommerce-ordering').submit();
            });
        });
    </script>
@endsection