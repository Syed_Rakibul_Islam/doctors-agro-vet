@extends('layouts.app')
@php
    $productTitle = config('app.locale') == "bn" && !empty($product->title_b) ? $product->title_b : $product->title;
@endphp

@section('pageTitle', __('navigation.products') . ' | ' . $productTitle)

@section('content')

    <main>
        <div class="container blog blogpost blogpost2">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('products') }}">{{ __('navigation.products') }}</a></li>
                        <li class="active">{{ $productTitle }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <div class="row">
                <!-- Content Area -->
                <div class="col-md-12 content-area">
                    <article class="type-post image-post">
                        <div class="entry-cover text-center">
                            <a href="#" title="Cover"><img src="{{ !empty($product->image) && file_exists('images/upload/product/' . $product->image) ? url('images/upload/product/' . $product->image) : url('images/product.png') }}" width="271" height="271"/></a>
                        </div>
                        <div class="entrycontent-block">
                            <div class="post-tag text-center">
                                @foreach($product->categories as $productCategory)

                                    @php
                                        $categoryName = config('app.locale') == "bn" && !empty($productCategory->name_b) ? $productCategory->name_b : $productCategory->name;
                                    @endphp
                                    <a href="{{ url('products/category/' . strtolower($productCategory->name) ) }}" title="{{ $categoryName }}">{{ $categoryName }}</a>

                                @endforeach
                            </div>
                            <div class="entry-title">
                                <h3>{{ $productTitle }}</h3>
                            </div>
                            <div class="entry-content">
                                @if(config('app.locale') == "bn" && !empty($product->description_b))
                                    {!! $product->description_b !!}
                                @else
                                    {!! $product->description !!}
                                @endif
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <div class="padding-100"></div>
    </main>
@endsection