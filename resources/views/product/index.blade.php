@extends('layouts.app')

@section('pageTitle', __('navigation.products'))

@section('content')
    <main class="site-main page-spacing">
        <!-- Shop Section -->
        <div id="product-section" class="container woocommerce product-section-no-sidebar container-fluid no-padding">
            <div class="col-md-12">
                <!-- Breadcrumb Area -->
                <div class="page-breadcrumb container-fluid no-padding">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li class="active">{{ __('navigation.products') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            @include('product.common.products-content')
        </div><!-- Shop Section /- -->
    </main>
@endsection

@section('script')
    <script>
        $(function() {
            $('#orderby').change(function(){
                $('.woocommerce-ordering').submit();
            });
        });
    </script>
@endsection