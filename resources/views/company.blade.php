@extends('layouts.app')

@section('pageTitle', __('navigation.our_company'))

@section('content')
    <main class="site-main page-spacing">
        <div class="padding-100"></div>
        <!-- Columns Section -->
        <div class="columns-section container-fluid no-padding">
            <!-- Container -->
            <div class="container">
                <div class="columns-heading">
                    <h3>about Dr.Agro</h3>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="columns-content">
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <div class="columns-content">
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                        </div>
                    </div>
                </div>
            </div><!-- Container /- -->
        </div><!-- Columns Section /- -->
        <div class="padding-100"></div>
        <!-- Why Choose -->
        <div class="whychoose-us container-fluid no-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="whychoose-box">
                            <div class="whychoose-content">
                                <h4>Who we are</h4>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                                <a href="#">read more</a>
                            </div>
                            <div class="shape"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="whychoose-box">
                            <div class="whychoose-content">
                                <h4>Our Mission</h4>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                                <a href="#">read more</a>
                            </div>
                            <div class="shape"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="whychoose-box">
                            <div class="whychoose-content">
                                <h4>Our Commitment To You</h4>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                                <a href="#">read more</a>
                            </div>
                            <div class="shape"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Why Choose /- -->
        <div class="padding-100"></div>

        <!-- Tab Section 1 -->
        <div class="tab-section tab-section-1 container-fluid no-padding">
            <div class="padding-50"></div>
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <div class="main-tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#first1" aria-controls="first1" role="tab" data-toggle="tab">Who we are</a></li>
                            <li role="presentation"><a href="#second1" aria-controls="second1" role="tab" data-toggle="tab">Mission</a></li>
                            <li role="presentation"><a href="#third1" aria-controls="third1" role="tab" data-toggle="tab">Vision</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="first1">
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn't listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="second1">
                                <p>2.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn't listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="third1">
                                <p>3.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn't listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Container /- -->
            <div class="padding-50"></div>
        </div><!-- Tab Section 1 -->
        <div class="padding-100"></div>
        <!-- Counter Section -->
        <div id="counter_section-3" class="counter-section counter-circle container-fluid no-padding">
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-xs-6 col-xs-12">
                        <div class="counter-box">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <h3><span class="count" id="statistics_3_count-1" data-statistics_percent="48">&nbsp;</span></h3>
                            <p>Products</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6 col-xs-12">
                        <div class="counter-box">
                            <i class="fa fa-moon-o" aria-hidden="true"></i>
                            <h3><span class="count" id="statistics_3_count-2" data-statistics_percent="12">&nbsp;</span></h3>
                            <p>Employees</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6 col-xs-12">
                        <div class="counter-box">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <h3><span class="count" id="statistics_3_count-3" data-statistics_percent="17">&nbsp;</span></h3>
                            <p>Awards</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6 col-xs-12">
                        <div class="counter-box">
                            <i class="fa fa-lemon-o" aria-hidden="true"></i>
                            <h3><span class="count" id="statistics_3_count-4" data-statistics_percent="189">&nbsp;</span></h3>
                            <p>Clients</p>
                        </div>
                    </div>
                </div>
            </div><!-- Container /- -->
        </div><!-- Counter Section /- -->
        <div class="padding-100"></div>
    </main>
@endsection
