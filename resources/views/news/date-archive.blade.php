@extends('layouts.app')

@php
    $segments = \Illuminate\Support\Facades\Request::segments();
    if(isset($segments[5])){
        $segment3Value = config('app.locale') == "bn" ? \App\Functions\NumberConversion::en2bn($segments[3]) : $segments[3];
        $segment4Value = __('date.number-month.' . $segments[4]);
        $segment5Value = config('app.locale') == "bn" ? \App\Functions\NumberConversion::en2bn($segments[5]) : $segments[5];
        $titleValue = $segment3Value . ' | ' . $segment4Value . ' | ' . $segment5Value;
    }
    elseif(isset($segments[4])){
        $segment3Value = config('app.locale') == "bn" ? \App\Functions\NumberConversion::en2bn($segments[3]) : $segments[3];
        $segment4Value = __('date.number-month.' . $segments[4]);
        $segment5Value = '';
        $titleValue = $segment3Value . ' | ' . $segment4Value;
    }
    else{
        $segment3Value = config('app.locale') == "bn" ? \App\Functions\NumberConversion::en2bn($segments[3]) : $segments[3];
        $segment4Value = '';
        $segment5Value = '';
        $titleValue = $segment3Value;
    }
@endphp

@section('pageTitle', __('navigation.news') . ' | ' . __('navigation.date') . ' | ' . $titleValue)

@section('content')
    <main>
        <div class="container blog blogpost">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('news') }}">{{ __('navigation.news') }}</a></li>
                        <li><a href="{{ url('news/date') }}">{{ __('navigation.date') }}</a></li>
                        @if(isset($segments[5]))
                            <li><a href="{{ url('news/date/' . $segments[3]) }}">{{ $segment3Value }}</a></li>
                            <li><a href="{{ url('news/date/' . $segments[3] . '/' . $segments[4]) }}">{{ $segment4Value }}</a></li>
                            <li class="active">{{ $segment4Value }}</li>
                        @elseif(isset($segments[4]))
                            <li><a href="{{ url('news/date/' . $segments[3]) }}">{{ $segment3Value }}</a></li>
                            <li class="active">{{ $segment4Value }}</li>
                        @else
                            <li class="active">{{ $segment3Value }}</li>
                        @endif
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <hr/>
            <div class="row">
                <!-- Content Area -->
            @include('news.common.post')
            <!-- Content Area /- -->

                <!-- Widget Area -->
            @include('news.common.right-side-bar')
            <!-- Widget Area /- -->

            </div>
            <div class="padding-100"></div>
        </div>
    </main>
@endsection

@section('script')
    @include('news.common.content-share-js')
@endsection
