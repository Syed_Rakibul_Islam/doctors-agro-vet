@extends('layouts.app')

@php
    $postTitle = config('app.locale') == "bn" && !empty($post->title_b) ? $post->title_b : $post->title;
    $hasImage = !empty($post->image) && file_exists('images/upload/news/' . $post->image) ? true : false;
    if(config('app.locale') == "bn"){
        $bnDate = new \EasyBanglaDate\Types\BnDateTime($post->created_at, new DateTimeZone('Asia/Dhaka'));
        $formatDateTime =  $bnDate->getDateTime()->format('F d, Y h:i A'). PHP_EOL;
    }
    else{
        $formatDateTime = \Carbon\Carbon::parse($post->created_at)->timezone('Asia/Dhaka')->format('F d, Y h:i A');
    }
@endphp

@section('pageTitle', __('navigation.news') . ' | ' . $postTitle)

@section('content')

    <main>
        <div class="container blog blogpost blogpost2">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('news') }}">{{ __('navigation.news') }}</a></li>
                        <li class="active">{{ $postTitle }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <hr/>
            <div class="row">
                <!-- Content Area -->
                <div class="col-md-9 col-sm-9 content-area">
                    <article class="type-post {{ $hasImage ? 'image-post' : 'simple-post audio-post' }}">
                        <div class="entry-cover">
                            @if($hasImage)
                                <a href="#" title="Cover"><img src="{{ asset('images/upload/news/' . $post->image) }}" alt="blogpost1" width="872" height="498"/></a>
                                <div class="entry-meta">
                                    <div class="post-share"><a href="#" data-toggle="modal" data-target="#post-share-{{ $post->id }}" title="share"><i class="fa fa-share-alt" aria-hidden="true"></i></a></div>
                                    <div class="postby">
                                        <a href="#" title="{{ config('app.locale') == "bn" && !empty($post->title_b) ? $post->title_b : $post->title }}">
                                            <img src="{{ isset($post->author->name) && !empty($post->author->image) ? asset('images/user/' . $post->author->image) : asset('images/av.png') }}" alt="author" width="72" height="72"/>
                                            <span>{{ isset($post->author->name) ? $post->author->name : 'Baned' }}</span>
                                        </a>
                                    </div>
                                    <ul>
                                        <li><a href="#" title="Post Date"><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $formatDateTime }}</a></li>
                                        <li>
                                            @if(\Auth::check())
                                                <button type="button" title="Likes" class="btn btn-link default-link-button post-like-button" data-post-id="{{ $post->id }}" data-base-url="{{ url('/') }}" ><i class="fa fa-heart" aria-hidden="true" style="{{ !empty($post->total_like) ? 'color: #ff0000' : '' }}"></i><span>{{ $post->total_like }}</span></button>
                                            @else
                                                <a href="{{ url('login') }}" title="Likes"><i class="fa fa-heart" aria-hidden="true"></i><span>{{ $post->total_like }}</span></a>
                                            @endif
                                        </li>
                                        <li><a href="{{ url('/news/' . $post->name) }}" title="Comment"><i class="fa fa-comment" aria-hidden="true" style="color: #02bcef"></i>{{ $post->total_comment }}</a></li>
                                    </ul>
                                </div>
                            @else
                                <div class="entry-meta">
                                    <div class="post-share"><a href="#" title="share" data-toggle="modal" data-target="#post-share-{{ $post->id }}"><i class="fa fa-share-alt" aria-hidden="true"></i></a></div>
                                    <div class="postby">
                                        <a href="#" title="{{ isset($post->author->name) ? $post->author->name : 'Baned' }}">
                                            <img src="{{ isset($post->author->name) && !empty($post->author->image) ? asset('images/user/' . $post->author->image) : asset('images/av.png') }}" alt="author" width="72" height="72"/>
                                            <span>{{ isset($post->author->name) ? $post->author->name : 'Baned' }}</span>
                                        </a>
                                    </div>
                                    <ul>
                                        <li><a href="#" title="Post Date"><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $formatDateTime }}</a></li>
                                        <li>
                                            @if(\Auth::check())
                                                <button type="button" title="Likes" class="btn btn-link default-link-button post-like-button" data-post-id="{{ $post->id }}" data-base-url="{{ url('/') }}" ><i class="fa fa-heart" aria-hidden="true" style="{{ !empty($post->total_like) ? 'color: #ff0000' : '' }}"></i><span>{{ $post->total_like }}</span></button>
                                            @else
                                                <a href="{{ url('login') }}" title="Likes"><i class="fa fa-heart" aria-hidden="true"></i><span>{{ $post->total_like }}</span></a>
                                            @endif
                                        </li>
                                        <li><a href="{{ url('/news/' . $post->name) }}" title="Comment"><i class="fa fa-comment" aria-hidden="true" style="color:#02bcef"></i>{{ $post->total_comment }}</a></li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="entrycontent-block">
                            <div class="post-tag">
                            @foreach($post->categories as $postCategory)
                                @php
                                    $categoryName = config('app.locale') == "bn" && !empty($postCategory->name_b) ? $postCategory->name_b : ucfirst($postCategory->name);
                                @endphp
                                <a href="{{ url('news/category/' . strtolower($postCategory->name) ) }}" title="{{ $categoryName }}">{{ $categoryName }}</a>
                            @endforeach
                            </div>
                            <div class="entry-title">
                                <h3>{{ $postTitle }}</h3>
                            </div>
                            <div class="entry-content">
                                @if(config('app.locale') == "bn" && !empty($post->description_b))
                                    {!! $post->description_b !!}
                                @else
                                    {!!$post->description !!}
                                @endif
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Widget Area -->
                @include('news.common.right-side-bar')
                <!-- Widget Area /- -->
            </div>
        </div>
        <!-- Container -->
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <hr />
                    <h3 class="text-center">{{ __('msg.comments') }}</h3>
                    <hr />
                    <div class="comment-section">
                        <ul class="media-list">
                            @if($post->total_comment > 0)
                                @foreach($post->comments as $comment)
                                    @if(!empty($comment->approve) || (\Auth::check() && \Auth::user()->id == $comment->user_id))
                                        @php
                                            if(!empty($comment->user_name)){
                                                $userName = $comment->user_name;
                                            }
                                            else{
                                                $userName = isset($comment->user->name) ? $comment->user->name : __('msg.anonymous');
                                            }

                                        @endphp
                                        <li class="media" style="{{ empty($comment->approve) ? 'opacity: 0.3; filter: alpha(opacity=30);' : '' }}">
                                            <div class="media-left">
                                                <a href="#" title="{{ $userName }}"><img src="{{ isset($comment->user->id) && !empty($comment->user->image) && file_exists('images/upload/user/' . $comment->user->image) ? asset('images/upload/user/' . $comment->user->image) : asset('images/av.png')  }}" alt="{{ $userName }}" width="102" height="102"/></a>
                                            </div>
                                            <div class="media-body">
                                                <div class="media-content">
                                                    <h4 class="media-heading">{{ $userName }},<span>
                                                            @if(config('app.locale') == "bn")
                                                                @php
                                                                    $bnDate = new \EasyBanglaDate\Types\BnDateTime($comment->created_at, new DateTimeZone('Asia/Dhaka'));
                                                                    echo $bnDate->getDateTime()->format('F d, Y h:i A'). PHP_EOL;
                                                                @endphp
                                                            @else
                                                                {{ \Carbon\Carbon::parse($comment->created_at)->timezone('Asia/Dhaka')->format('F d, Y h:i A') }}
                                                            @endif
                                                        </span></h4>
                                                    <p>{{ $comment->description }}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        <div class="comment-form">
                            <h3>{{ __('msg.add_comment') }}</h3>
                            <form class="row" name="comment-form" method="post" data-post-id="{{ $post->id }}">
                                @if(!\Auth::check())
                                    <div class="form-group col-md-12">
                                        <input type="text" placeholder="{{ __('auth.name') }} *" name="user_name" class="form-control" maxlength="100" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="email" placeholder="{{ __('auth.email') }} *" name="user_email" class="form-control" maxlength="100" required>
                                    </div>
                                @endif
                                <div class="form-group col-md-12">
                                    <textarea placeholder="{{ __('auth.comment') }} *" name="description" rows="8" class="form-control" maxlength="100000" required></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="submit" title="Submit" value="Submit" name="submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Container /- -->
        <div class="padding-100"></div>
    </main>
    <!-- Modal -->
    <div class="modal fade" id="post-share-{{ $post->id }}" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-brand">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Share</h4>
                </div>
                <div class="modal-body">
                    <aside class="ftr-widget ftr_about_widget text-center">
                        <ul>
                            <li><a class="facebook customer share" href="https://www.facebook.com/sharer.php?u={{ url('/news/' . $post->name) }}" title="Facebook share" target="_blank" ><i class="fa fa-facebook fa-2x"></i></a></li>
                            <li><a class="twitter customer share" href="https://twitter.com/share?url={{ url('/news/' . $post->name) }};text={{ $post->title }};hashtags={{ config('app.name') }}" title="Twitter share" target="_blank" ><i class="fa fa-twitter fa-2x"></i></a></li>
                            <li><a class="google_plus customer share" href="https://plus.google.com/share?url={{ url('/news/' . $post->name) }}" title="Google Plus Share" target="_blank" ><i class="fa fa-google-plus fa-2x"></i></a></li>
                            <li><a class="linkedin customer share" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url('/news/' . $post->name) }}" title="linkedin Share" target="_blank" ><i class="fa fa-linkedin fa-2x"></i></a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Form Validation JavaScript -->
    <script src="{{ asset('admin-resources/vendors/bower_components/bootstrap-validator/dist/validator.min.js') }}"></script>

    <script>
        var baseURL = "{{ url('/') }}";
        var appLocal = "{{ config('app.locale') }}";
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('form[name=comment-form]').submit(function(e){
                e.preventDefault();
                var name = $('input[name=user_name]') ? $('input[name=user_name]').val() : '';
                var email = $('input[name=user_email]') ? $('input[name=user_email]').val() : '';
                var description = $('textarea[name=description]').val();
                $.ajax({
                    type: 'POST',
                    url: baseURL + '/ajax/post-comment',
                    dataType: "json",
                    data: {post_id : $(this).data('post-id'), user_name : name, user_email : email, description : description, localization : appLocal},
                    success: function (result) {
                        var css = (result.approve) ? '' : 'style="opacity: 0.3; filter: alpha(opacity=30);"' ;
                        var data = '' +
                            '<li class="media" ' + css + '>\n' +
                            '   <div class="media-left">\n' +
                            '      <a href="#" title="' + result.user_name + '"><img src="' + result.image + '" alt="' + result.user_name + '" width="102" height="102"/></a>\n' +
                            '   </div>\n' +
                            '   <div class="media-body">\n' +
                            '      <div class="media-content">\n' +
                            '         <h4 class="media-heading">' + result.user_name + ',<span>' + result.date + ' </span></h4>\n' +
                            '         <p>' + result.description + '</p>\n' +
                            '     </div>\n' +
                            '   </div>\n' +
                            '</li>';

                        $('.media-list').append(data).animate({ width: 'show' });

                        $('input[name=user_name]').val('');
                        $('input[name=user_email]').val('');
                        $('textarea[name=description]').val('');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Status: " + textStatus, "error");
                        console.log("Error: " + errorThrown, "error");
                    }
                });
            });
        });
    </script>

    @include('news.common.content-share-js')

@endsection
