@extends('layouts.app')

@section('pageTitle', __('navigation.products') . ' | ' . __('navigation.category'))

@section('content')
    <main>

        <div class="container blog blogpost">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li> <a href="{{ url('news') }}">{{ __('navigation.news') }}</a></li>
                        <li class="active">{{ __('navigation.category') }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <hr/>
            <div class="row">
            <!-- Content Area -->
            <div class="col-md-9 col-sm-9 content-area">
                <br/>
                <ul class="list-group">
                @foreach($sharedCategories as $category)
                    @if(count($category->posts) > 0)
                        @if(config('app.locale') == "bn" && !empty($category->name_b))
                            <li class="list-group-item"><a title="{{ $category->name_b }}" href="{{ url("news/category/" . strtolower($category->name)) }}">{{ $category->name_b }}</a> <span class="badge">{{ \App\Functions\NumberConversion::en2bn(count($category->posts)) }}</span></li>
                        @else
                            <li class="list-group-item"><a title="{{ $category->name }}" href="{{ url("news/category/" . strtolower($category->name)) }}">{{ $category->name }}</a> <span class="badge">{{ count($category->posts) }}</span></li>
                        @endif
                    @endif
                @endforeach
                </ul>
            </div>
            <!-- Content Area /- -->

                <!-- Widget Area -->
            @include('news.common.right-side-bar')
            <!-- Widget Area /- -->

            </div>
            <div class="padding-100"></div>
        </div>
    </main>
@endsection
