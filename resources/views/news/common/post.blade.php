<div class="col-md-9 col-sm-9 content-area">
    @forelse($posts as $post)
        @php
            $hasImage = !empty($post->image) && file_exists('images/upload/news/' . $post->image) ? true : false;
            if(config('app.locale') == "bn"){
                $bnDate = new \EasyBanglaDate\Types\BnDateTime($post->created_at, new DateTimeZone('Asia/Dhaka'));
                $formatDateTime =  $bnDate->getDateTime()->format('F d, Y h:i A'). PHP_EOL;
            }
            else{
                $formatDateTime = \Carbon\Carbon::parse($post->created_at)->timezone('Asia/Dhaka')->format('F d, Y h:i A');
            }
        @endphp
        <article class="type-post {{ $hasImage ? 'image-post' : 'simple-post audio-post' }}">
            <div class="entry-cover">
                @if($hasImage)
                    <img src="{{ asset('images/upload/news/' . $post->image) }}" alt="blog1" width="1170" height="646"/>
                    <div class="entry-meta">
                        <div class="post-share"><a href="#" data-toggle="modal" data-target="#post-share-{{ $post->id }}" title="share"><i class="fa fa-share-alt" aria-hidden="true"></i></a></div>
                        <div class="postby">
                            <a href="#" title="{{ isset($post->author->name) ? $post->author->name : 'Baned' }}">
                                <img src="{{ isset($post->author->name) && !empty($post->author->image) ? asset('images/user/' . $post->author->image) : asset('images/av.png') }}" alt="author" width="72" height="72"/>
                                <span>{{ isset($post->author->name) ? $post->author->name : 'Baned' }}</span>
                            </a>
                        </div>
                        <ul>
                            <li><a href="#" title="Post Date"> <i class="fa fa-clock-o" aria-hidden="true"></i>{{ $formatDateTime }}</a></li>
                            <li>
                                @if(\Auth::check())
                                    <button type="button" title="Likes" class="btn btn-link default-link-button post-like-button" data-post-id="{{ $post->id }}" data-base-url="{{ url('/') }}" ><i class="fa fa-heart" aria-hidden="true" style="{{ !empty($post->total_like) ? 'color: #ff0000' : '' }}"></i><span>{{ $post->total_like }}</span></button>
                                @else
                                    <a href="{{ url('login') }}" title="Likes"><i class="fa fa-heart" aria-hidden="true"></i><span>{{ $post->total_like }}</span></a>
                                @endif

                            </li>
                            <li><a href="{{ url('/news/' . $post->name) }}" title="Comment"><i class="fa fa-comment" aria-hidden="true" style="color:#02bcef"></i>{{ $post->total_comment }}</a></li>
                        </ul>
                    </div>
                @else
                    <div class="entry-meta">
                        <div class="post-share"><a href="#" title="share" data-toggle="modal" data-target="#post-share-{{ $post->id }}"><i class="fa fa-share-alt" aria-hidden="true"></i></a></div>
                        <div class="postby">
                            <a href="#" title="{{ isset($post->author->name) ? $post->author->name : 'Baned' }}">
                                <img src="{{ isset($post->author->name) && !empty($post->author->image) ? asset('images/user/' . $post->author->image) : asset('images/av.png') }}" alt="author" width="72" height="72"/>
                                <span>{{ isset($post->author->name) ? $post->author->name : 'Baned' }}</span>
                            </a>
                        </div>
                        <ul>
                            <li><a href="#" title="Post Date"><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $formatDateTime }}</a></li>
                            <li>
                                @if(\Auth::check())
                                    <button type="button" title="Likes" class="btn btn-link default-link-button post-like-button" data-post-id="{{ $post->id }}" data-base-url="{{ url('/') }}" ><i class="fa fa-heart" aria-hidden="true" style="{{ !empty($post->total_like) ? 'color: #ff0000' : '' }}"></i><span>{{ $post->total_like }}</span></button>
                                @else
                                    <a href="{{ url('login') }}" title="Likes"><i class="fa fa-heart" aria-hidden="true"></i><span>{{ $post->total_like }}</span></a>
                                @endif
                            </li>
                            <li><a href="{{ url('/news/' . $post->name) }}" title="Comment"><i class="fa fa-comment" aria-hidden="true" style="color:#02bcef"></i>{{ $post->total_comment }}</a></li>
                        </ul>
                    </div>
                @endif
            </div>
            <div class="entrycontent-block">
                <div class="post-tag">
                    @foreach($post->categories as $postCategory)
                        @php
                            $categoryName = config('app.locale') == "bn" && !empty($postCategory->name_b) ? $postCategory->name_b : ucfirst($postCategory->name);
                        @endphp
                        <a href="{{ url('news/category/' . strtolower($postCategory->name) ) }}" title="{{ $categoryName }}">{{ $categoryName }}</a>
                    @endforeach
                </div>
                <div class="entry-title">
                    @if(config('app.locale') == "bn" && !empty($post->title_b))
                        <h3><a href="{{ url('/news/' . $post->name) }}" title="{{ $post->title_b }}">{{ $post->title_b }}</a></h3>
                    @else
                        <h3><a href="{{ url('/news/' . $post->name) }}" title="{{ $post->title }}">{{ $post->title }}</a></h3>
                    @endif
                </div>
                <div class="entry-content">
                    @if(config('app.locale') == "bn" && !empty($post->description_b))
                        {!! \Illuminate\Support\Str::words($post->description_b, 150,'....') !!}
                    @else
                        {!! \Illuminate\Support\Str::words($post->description, 150,'....') !!}
                    @endif
                </div>
            </div>
        </article>
        <div class="padding-50"></div>
        <div class="pagination-wrapper"> {!! $posts->links() !!} </div>

        <!-- Modal -->
        <div class="modal fade" id="post-share-{{ $post->id }}" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-brand">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Share</h4>
                    </div>
                    <div class="modal-body">
                        <aside class="ftr-widget ftr_about_widget text-center">
                            <ul>
                                <li><a class="facebook customer share" href="https://www.facebook.com/sharer.php?u={{ url('/news/' . $post->name) }}" title="Facebook share" target="_blank" ><i class="fa fa-facebook fa-2x"></i></a></li>
                                <li><a class="twitter customer share" href="https://twitter.com/share?url={{ url('/news/' . $post->name) }};text={{ $post->title }};hashtags={{ config('app.name') }}" title="Twitter share" target="_blank" ><i class="fa fa-twitter fa-2x"></i></a></li>
                                <li><a class="google_plus customer share" href="https://plus.google.com/share?url={{ url('/news/' . $post->name) }}" title="Google Plus Share" target="_blank" ><i class="fa fa-google-plus fa-2x"></i></a></li>
                                <li><a class="linkedin customer share" href="https://www.linkedin.com/shareArticle?mini=true&url={{ url('/news/' . $post->name) }}" title="linkedin Share" target="_blank" ><i class="fa fa-linkedin fa-2x"></i></a></li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <h1>{{ __('msg.not_found', ['name' => __('navigation.news')]) }}</h1>
    @endforelse

</div>