<div class="col-md-3 col-sm-3 widget-area">
    <aside class="widget widget_categories">
        <h3 class="widget-title">{{ __('navigation.categories') }}</h3>
        <ul>
        @foreach($sharedCategories as $category)
            @if(count($category->posts) > 0)
                @if(config('app.locale') == "bn" && !empty($category->name_b))
                    <li><a title="{{ $category->name_b }}" href="{{ url("news/category/" . strtolower($category->name)) }}">{{ $category->name_b }}<span>{{ \App\Functions\NumberConversion::en2bn(count($category->posts)) }}</span></a></li>
                @else
                    <li><a title="{{ $category->name }}" href="{{ url("news/category/" . strtolower($category->name)) }}">{{ $category->name }}<span>{{ count($category->posts) }}</span></a></li>
                @endif
            @endif
        @endforeach
        </ul>
    </aside>
    <aside class="widget widget_categories archives">
        <h3 class="widget-title">{{ __('date.months') }}</h3>
        <ul>
            @for($i = 0; $i > -6; $i--)
                @php
                    if(config('app.locale') == "bn"){
	                    $bnDate = new \EasyBanglaDate\Types\BnDateTime(\Carbon\Carbon::now()->addMonths($i), new DateTimeZone('Asia/Dhaka'));
                        $dateTimeZone = $bnDate->getDateTime()->format('F Y'). PHP_EOL;
                    }
                    else{
                        $dateTimeZone = \Carbon\Carbon::now()->addMonths($i)->timezone('Asia/Dhaka')->format('F Y');
                    }
                @endphp
            <li>
                <a title="{{ $dateTimeZone }}" href="{{ url('news/date/' . \Carbon\Carbon::now()->addMonths($i)->timezone('Asia/Dhaka')->format('Y') . '/' . \Carbon\Carbon::now()->addMonths($i)->timezone('Asia/Dhaka')->format('m') ) }}">
                    {{ $dateTimeZone }}
                </a>
            </li>
            @endfor
        </ul>
    </aside>
    <aside class="widget widget_recent">
        <h3 class="widget-title">{{ __('msg.latest_post') }}</h3>
        @foreach($sharedPosts as $post)
        <div class="recent-content">
            @if(!empty($post->image) && file_exists('images/upload/news/' . $post->image))
                <a href="{{ url('news/' . $post->name) }}"><img src="{{ asset('images/upload/news/' . $post->image) }}" alt="RecentPost" width="55" height="55"/></a>
            @endif
            <h3>
                @if(config('app.locale') == "bn" && !empty($post->title_b))
                    <a href="{{ url('news/' . $post->name) }}" title="{{ $post->title_b }}"> {!! \Illuminate\Support\Str::words($post->title_b, 2,' ...') !!}</a>
                @else
                    <a href="{{ url('news/' . $post->name) }}" title="{{ $post->title }}"> {!! \Illuminate\Support\Str::words($post->title, 2,' ...') !!}</a>
                @endif
            </h3>
            <span>
                @if(config('app.locale') == "bn")
                    @php
                        $bnDate = new \EasyBanglaDate\Types\BnDateTime($post->created_at, new DateTimeZone('Asia/Dhaka'));
                        echo $bnDate->getDateTime()->format('F d, Y'). PHP_EOL;
                    @endphp
                @else
                    {{ \Carbon\Carbon::parse($post->created_at)->timezone('Asia/Dhaka')->format('F d, Y') }}
                @endif
            </span>
        </div>
        @endforeach
    </aside>
</div>