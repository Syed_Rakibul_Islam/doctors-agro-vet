@extends('layouts.app')


@section('pageTitle', __('navigation.news') . ' | ' . __('navigation.date'))

@section('content')
    <main>

        <div class="container blog blogpost">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li> <a href="{{ url('news') }}">{{ __('navigation.news') }}</a></li>
                        <li class="active">{{ __('navigation.date') }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <hr/>
            <div class="row">
                <!-- Content Area -->
                <div class="col-md-9 col-sm-9 content-area">
                    <br/>
                    <ul class="list-group">
                        @foreach($posts as $post)
                            @php
                                $dateTime = \Carbon\Carbon::parse($post->day)->timezone('Asia/Dhaka');
                            @endphp
                            <li class="list-group-item">
                                <a title="October 2015" href="{{ url('news/date/' . $dateTime->format('Y') . '/' . $dateTime->format('m') . '/' . $dateTime->format('d') ) }}">
                                    @if(config('app.locale') == "bn")
                                        @php
                                            $bnDate = new \EasyBanglaDate\Types\BnDateTime($dateTime);
                                            echo $bnDate->getDateTime()->format('F d, Y'). PHP_EOL;
                                        @endphp
                                    @else
                                        {{ $dateTime->format('F d, Y') }}
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- Content Area /- -->

                <!-- Widget Area -->
            @include('news.common.right-side-bar')
            <!-- Widget Area /- -->

            </div>
            <div class="padding-100"></div>
        </div>
    </main>
@endsection
