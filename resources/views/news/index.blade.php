@extends('layouts.app')

@section('pageTitle', __('navigation.news'))

@section('content')
    <main>

        <div class="container blog blogpost">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="active">{{ __('navigation.news') }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <hr/>
            <div class="row">
                <!-- Content Area -->
                @include('news.common.post')
                <!-- Content Area /- -->

                <!-- Widget Area -->
                @include('news.common.right-side-bar')
                <!-- Widget Area /- -->

            </div>
            <div class="padding-100"></div>
        </div>
    </main>
@endsection

@section('script')
    @include('news.common.content-share-js')
@endsection
