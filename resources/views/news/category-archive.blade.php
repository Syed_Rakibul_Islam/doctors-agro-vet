@extends('layouts.app')

@php
    if(!empty($category)){
        $categoryName = config('app.locale') == "bn" && !empty($category->name_b) ? $category->name_b : $category->name;
    }
    else{
        $categoryName = __('msg.undefined');
    }
@endphp

@section('pageTitle', __('navigation.products') . ' | ' . __('navigation.category') . ' | ' . $categoryName)

@section('content')
    <main>
        <div class="container blog blogpost">
            <!-- Breadcrumb Area -->
            <div class="page-breadcrumb container-fluid no-padding">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('news') }}">{{ __('navigation.news') }}</a></li>
                        <li><a href="{{ url('news/category') }}">{{ __('navigation.category') }}</a></li>
                        <li class="active">{{ $categoryName }}</li>
                    </ol>
                </div>
            </div>
            <!-- Breadcrumb Area /- -->
            <hr/>
            <div class="row">
                <!-- Content Area -->
            @include('news.common.post')
            <!-- Content Area /- -->

                <!-- Widget Area -->
            @include('news.common.right-side-bar')
            <!-- Widget Area /- -->

            </div>
            <div class="padding-100"></div>
        </div>
    </main>
@endsection

@section('script')
    @include('news.common.content-share-js')
@endsection
