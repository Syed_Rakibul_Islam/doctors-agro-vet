@extends('layouts.app')

@section('pageTitle', __('msg.error_404'))

@section('content')
    <main>

        <div class="info-boxes container-fluid no-padding">
            <div class="row">

                <div class="no-padding">
                    <div class="info-boxes-content yellow-bg text-center">

                        <div class="padding-100"></div>

                        <h1 class="text-white">{{ __('msg.oops') }}!</h1>
                        <h1 class="text-white">{{ __('msg.error_404') }}</h1>
                        <h1 class="text-white">{{ __('msg.pnf') }}</h1>
                        <div>
                            <a href="{{ route('home') }}"><i class="fa fa-home no-padding-left-i" aria-hidden="true"></i> {{ __('navigation.home') }}</a>
                            <a href="{{ route('contact') }}" class="text-black-i border-black-i btn-hover-white-i">{{ __('navigation.contact') }} <i class="fa fa-envelope no-padding-left-i" aria-hidden="true"></i></a>
                        </div>

                        <div class="padding-100"></div>
                    </div>
                </div>

            </div>
        </div>
    </main>
@endsection
