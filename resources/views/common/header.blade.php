@php
    $productCategories = \App\Category::where('type', 'product')->select( 'id', 'name', 'name_b')->get();

    // Change version
    $path = Request::path();
    $segment1 = Request::segment(1);

    if($segment1 == 'en') $path = preg_replace('/' . preg_quote('en', '/') . '/', 'bn', $path, 1);
    else $path = preg_replace('/' . preg_quote('bn', '/') . '/', 'en', $path, 1);

    $queryParameters = !empty(Request::getQueryString()) ? '?' . Request::getQueryString() : '';
@endphp
<div id="main-menu-area">
    <a id="top"></a>
    <!-- Burger Menu -->
    <div class="burger-menu-block">
        <span><i class="icon_close"></i></span>
        <div class="burger-menu-content">
            <h3>{{ __('navigation.account') }}</h3>
            <ul>
                @if(Auth::check())
                    <li><a href="{{ route('profile') }}" title="{{ __('navigation.my_profile') }}">{{ __('navigation.my_profile') }}</a></li>
                    <li><a href="{{ route('change_password') }}" title="{{ __('navigation.change_password') }}">{{ __('navigation.change_password') }}</a></li>
                    <li><a href="{{ route('logout') }}" title="{{ __('navigation.sign_out') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('navigation.sign_out') }}</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @else
                    <li><a href="{{ route('register') }}" title="{{ __('navigation.sign_up') }}">{{ __('navigation.sign_up') }}</a></li>
                    <li><a href="{{ route('login') }}" title="{{ __('navigation.sign_in') }}">{{ __('navigation.sign_in') }}</a></li>
                @endif
            </ul>
        </div>
    </div><!-- Burger Menu /- -->
    <div class="row no-left-margin no-right-margin top-header-section">
        <div class="col-xs-3 no-left-padding">
            <div class="buttons-section container-fluid no-padding">
                <div class="buttons-bg version-button">
                    <a href="{{ url('/' . $path ) . $queryParameters }}" class="btn btn-default btn-xs" >{{ config('app.locale') == "en" ? 'বাংলা সংস্করণ' : 'English Version' }}</a>
                </div>
            </div>
        </div>
        <div class="col-xs-9 no-right-padding">
            <!-- Social Section -->
            <div class="social-section container-fluid no-padding pull-right">
                <ul class="socials social-icons">
                    <li><a href="#" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div><!-- Social Section /- -->
            <div class="clearfix"></div>
        </div>
    </div>
    <hr class="top-header-hr"/>
    <header id="header" class="header-section header-section8 text-color-black responsive-bg container-fluid no-padding">
        <!-- Logo Block -->
        <div class="logo-block">
            <a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}"></a>
        </div><!-- Logo Block -->

        <!-- nav -->
        <nav class="navbar navbar-default ow-navigation no-padding-right">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}"></a>
            </div>
            <!-- Menu Icon -->
            <div class="menu-icon">
                <div class="search">
                    <a href="#" id="search" title="Search"><img src="{{ asset('images/search-ic-black.png') }}" alt="Search" /></a>
                </div>
                <div class="burger-menu">
                    @if(Auth::guest())
                        <a href="#" title="menu"><i class="fa fa-user fa-2x text-black" aria-hidden="true"></i></a>
                    @else
                        <a href="#" title="{{ Auth::user()->name }}"><img src="{{ !empty(Auth::user()->image) && file_exists('images/upload/user/' . Auth::user()->image) ? asset('images/upload/user/' . Auth::user()->image) : asset('images/user.png') }}" alt="{{ Auth::user()->name }}" style="height: 24px;" /></a>
                    @endif
                </div>
            </div>
            <!-- Menu Icon /- -->
            <div class="navbar-collapse collapse navbar-right" id="navbar">
                <ul class="nav navbar-nav">
                    <li class="{{ request()->is('*/home') ? 'active' : '' }}" ><a href="{{ route('home') }}" title="Home">{{ __('navigation.home') }}</a></li>
                    <li class="{{ request()->is('*/company') ? 'active' : '' }}"><a href="{{ url('company') }}" title="Our Company">{{ __('navigation.our_company') }}</a></li>
                    <li class="dropdown {{ request()->is('*/products*') ? 'active' : '' }}">
                        <a href="{{ url('products') }}" title="Products" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{{ __('navigation.products') }}</a>
                        <i class="ddl-switch fa fa-angle-down"></i>
                        <ul class="dropdown-menu">
                            @foreach($productCategories as $product_category)
                            <li><a href="{{ url('products/category/' . strtolower($product_category->name)) }}">{{ config('app.locale') == "bn" && !empty($product_category->name_b) ? $product_category->name_b . ' পণ্য' : ucfirst($product_category->name) . ' Product' }}</a></li>
                            @endforeach
                            <li><a href="{{ url('products') }}"> {{ __('navigation.all_products') }}</a></li>
                        </ul>
                    </li>
                    {{--<li class="{{ request()->is('*/clients*') ? 'active' : '' }}"><a href="{{ url('clients') }}" title="Our Company">{{ __('navigation.clients') }}</a></li>--}}
                    <li class="{{ request()->is('*/partners*') ? 'active' : '' }}"><a href="{{ url('partners') }}" title="Partners">{{ __('navigation.global_partners') }}</a></li>
                    <li class="{{ request()->is('*/news*') ? 'active' : '' }}" ><a href="{{ route('news') }}" title="Partners">{{ __('navigation.news') }}</a></li>
                    <li class="{{ request()->is('*/contact*') ? 'active' : '' }}" ><a href="{{ url('contact') }}" title="Partners">{{ __('navigation.contact') }}</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </nav><!-- nav /- -->
        <!-- Search Box -->
        <div class="search-box">
            <span><i class="icon_close"></i></span>
            <form>
                <input type="text" class="form-control" placeholder="Enter a keyword and press enter..." />
            </form>
        </div><!-- Search Box /- -->
    </header>
</div>