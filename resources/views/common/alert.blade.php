@if (Session::has('flash_message') || Session::has('warning_msg'))
<div class="alert-section alert-section-1 container-fluid no-padding">
    @if (Session::has('flash_message'))
        <div role="alert" class="alert alert-success alert-dismissable no-margin">
            <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('flash_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
            </button>
        </div>
    @endif
    @if (Session::has('warning_msg'))
        <div role="alert" class="alert alert-warning alert-dismissable no-margin">
            <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('warning_msg') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
            </button>
        </div>
    @endif
</div>
@endif
