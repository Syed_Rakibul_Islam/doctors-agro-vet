@php
    $posts = \App\Post::orderBy('id', 'desc')->limit(3)->get();
@endphp
<div class="footer-main footer-section3 footer-no-bg gray-bg container-fluid no-padding">
    <!-- Container -->
    <div class="container">
        <div class="padding-60"></div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <aside class="ftr-widget ftr_about_widget">
                    <a href="{{ url('/') }}" title="Logo"><img src="{{ asset('favicons/favicon-96x96.png') }}" alt="Logo" width="76"  /></a>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences.</p>
                    <ul>
                        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" title="Twiiter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </aside>
            </div>
            <div class="col-md-4 col-sm-6">
                <aside class="ftr-widget ftr_contact_widget">
                    <h3 class="widget-title">{{ __('auth.address') }}</h3>
                    <p><i class="fa fa-map-marker"></i>Nurjehan Tower (5th Floor), 2, Link Road, Banglamotor, Dhaka-1200, Bangladesh </p>
                    <p><i class="fa fa-phone"></i> <a href="tel:+88029673737" title="+8802-9673737">+880-29673737</a></p>
                    <p><i class="fa fa-envelope-o"></i> <a href="mailto:info@doctorsagrovetltd.com" title="info@doctorsagrovetltd.com">info@doctorsagrovetltd.com</a></p>
                </aside>
                <div id="map-canvas-contact" class="map-canvas footer-map" style="height: 110px" data-lat="23.745540" data-lng="90.394377" data-string="Doctor's Agro-vet" data-zoom="16"></div>
            </div>
            <div class="col-md-4 col-sm-6">
                <aside class="ftr-widget ftr_latest_post">
                    <h3 class="widget-title">{{ __('msg.latest_news') }}</h3>
                    @foreach($posts as $post)
                    <div class="latest-box">
                        <a href="#"><img src="{{ !empty($post->image) && file_exists('images/upload/news/' . $post->image) ? asset('images/upload/news/' . $post->image) : asset('images/post.png') }}" alt="Latest Post" width="83" height="84" /></a>
                        <h6><a href="#">{{ config('app.locale') == "bn" && !empty($post->title_b) ? \Illuminate\Support\Str::words($post->title_b, 10,' ...') : \Illuminate\Support\Str::words($post->title, 10,' ...') }}</a></h6>
                        <span>
                            @if(config('app.locale') == "bn")
                                @php
                                    $bnDate = new \EasyBanglaDate\Types\BnDateTime($post->created_at, new DateTimeZone('Asia/Dhaka'));
                                    echo $bnDate->getDateTime()->format('F d, Y'). PHP_EOL;
                                @endphp
                            @else
                                {{ \Carbon\Carbon::parse($post->created_at)->timezone('Asia/Dhaka')->format('F d, Y') }}
                            @endif
                        </span>
                    </div>
                    @endforeach
                </aside>
            </div>
        </div>
        <div class="copyright container-fluid">
            <p>&copy; Copyright {{ date("Y") }} <a href="https://www.trustdigitalmedia.com/" target="_blank">Trust Digital Media</a>. All rights reserved.</p>
        </div><!-- Bottom Footer /- -->
    </div><!-- Container /- -->
</div>