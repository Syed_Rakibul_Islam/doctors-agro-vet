let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/assets/js/functions.js', 'resources/assets/js/custom.js'], 'public/js')
   .css([
       'resources/assets/css/plugins.css',
       'resources/assets/css/navigation-menu.css',
       'resources/assets/css/fonts.css',
       'resources/assets/css/style.css',
       'resources/assets/css/footer.css',
       'resources/assets/css/shortcode.css',
       'resources/assets/css/woocommerce.css',
       'resources/assets/css/custom.css'
   ], 'public/css')
    .version();
